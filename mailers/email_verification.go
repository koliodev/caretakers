package mailers

import (
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/envy"
	"github.com/kolioDev/care/models"
)

func SendEmailVerifications(u models.User, e models.EmailVerification) error {
	m := mail.NewMessage()

	// fill in with your stuff:
	m.Subject = "Потвърждение на Е-mail адрес"
	m.From = envy.Get("SMTP_FROM", "Garfild <noreply@garfild.me>")
	m.To = []string{u.Email.String}
	err := m.AddBody(r.HTML("email_verification.html"), map[string]interface{}{
		"user":  u,
		"token": e.Token,
	})
	if err != nil {
		return err
	}
	m.Headers["Content-Type"] = "text/html"
	return smtp.Send(m)
}
