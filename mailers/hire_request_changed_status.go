package mailers

import (
	"errors"
	"fmt"
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/envy"
	"github.com/kolioDev/care/models"
	"strings"
)

func SendHireRequestChangedStatus(u models.User, h models.HireRequest) error {
	m := mail.NewMessage()

	m.Subject = "Приета поръчка"
	if h.Status == "declined" {
		m.Subject = "Отказана поръчка"
	} else if h.Status == "pending" {
		return errors.New("cannot compose email for pending hire request")
	}
	m.From = envy.Get("SMTP_FROM", "Garfild <noreply@garfild.me>")
	m.To = []string{u.Email.String}

	url := fmt.Sprintf("%s%s", envy.Get("FRONTEND_URL", "127.0.0.1:8080"), envy.Get("FRONTEND_HIRE_REQUEST_URL", "/request/:hire_request"))
	url = strings.Replace(url, ":hire_request", h.ID.String(), 1)

	err := m.AddBody(r.HTML("hire_request_changed_status.html"), map[string]interface{}{
		"caretaker":  h.Caretaker,
		"status":     h.Status,
		"requestUrl": url,
	})
	if err != nil {
		return err
	}

	m.Headers["Content-Type"] = "text/html"
	return smtp.Send(m)
}
