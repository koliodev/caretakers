package mailers

import (
	"testing"

	"github.com/stanislas-m/mocksmtp"
	"github.com/stretchr/testify/require"
)

func Test_SendBlankMail(t *testing.T) {
	r := require.New(t)
	ms, v := smtp.(*mocksmtp.MockSMTP)
	r.True(v)
	// Clear SMTP queue after test
	defer ms.Clear()
	err := SendBlankEmails("pe6o@gmail.com")
	r.NoError(err)
	// Test email
	r.Equal(1, ms.Count())
	m, err := ms.LastMessage()
	r.NoError(err)
	r.Equal("Garfild <noreply@garfild.me>", m.From)
	r.Equal("Blank test E-mail", m.Subject)
	r.Equal(1, len(m.To))
	r.Equal("pe6o@gmail.com", m.To[0])
	r.Contains(m.Bodies[0].Content, "#blank email !DO NOT DELETE THIS LINE")
}
