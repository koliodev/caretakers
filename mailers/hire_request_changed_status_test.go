package mailers

import (
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioDev/care/models"
	"testing"

	"github.com/stanislas-m/mocksmtp"
	"github.com/stretchr/testify/require"
)

func Test_SendHireRequestChangedStatus(t *testing.T) {
	u := models.User{Email: nulls.NewString("go6o@gmail.com")}
	h := models.HireRequest{
		ID:     uuid.FromStringOrNil("22c36ad1-aa99-448c-9fa5-a49f7a7886f6"),
		Status: "declined",
		Caretaker: models.User{
			FirstName:  "Валенсия",
			SecondName: "Барселонова",
			Caretaker: &models.Caretaker{
				Picture: "base64Valensia",
			},
		},
	}

	r := require.New(t)
	ms, v := smtp.(*mocksmtp.MockSMTP)
	r.True(v)
	// Clear SMTP queue after test
	defer ms.Clear()
	err := SendHireRequestChangedStatus(u, h)
	r.NoError(err)
	// Test email
	r.Equal(1, ms.Count())
	m, err := ms.LastMessage()
	r.NoError(err)
	r.Equal("Garfild <noreply@garfild.me>", m.From)
	//Отказана поръчка
	r.Equal("Отказана поръчка", m.Subject)
	r.Equal(1, len(m.To))
	r.Equal("go6o@gmail.com", m.To[0])
	r.Contains(m.Bodies[0].Content, "Валенсия Барселонова")
	r.Contains(m.Bodies[0].Content, "отказа Вашата поръчка")
	r.Contains(m.Bodies[0].Content, "base64Valensia")
	r.Contains(m.Bodies[0].Content, "127.0.0.1:8080/request/22c36ad1-aa99-448c-9fa5-a49f7a7886f6")

	//Приета поръчка
	ms.Clear()
	h.Status = "accepted"
	err = SendHireRequestChangedStatus(u, h)
	r.NoError(err)
	m, err = ms.LastMessage()
	r.NoError(err)

	r.Equal("Приета поръчка", m.Subject)
	r.Equal(1, len(m.To))
	r.Equal("go6o@gmail.com", m.To[0])
	r.Contains(m.Bodies[0].Content, "прие Вашата поръчка")

}
