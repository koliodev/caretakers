package mailers

import (
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/buffalo/render"
	"github.com/gobuffalo/envy"
)

func SendBlankEmails(to string) error {
	m := mail.NewMessage()

	// fill in with your stuff:
	m.Subject = "Blank test E-mail"
	m.From = envy.Get("SMTP_FROM", "Garfild <noreply@garfild.me>")
	m.To = []string{to}

	err := m.AddBody(r.HTML("blank_email.html"), render.Data{})
	if err != nil {
		return err
	}
	m.Headers["Content-Type"] = "text/html"
	return smtp.Send(m)
}
