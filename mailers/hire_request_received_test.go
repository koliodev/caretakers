package mailers

import (
	"testing"

	"github.com/gobuffalo/nulls"
	"github.com/kolioDev/care/models"
	"github.com/stanislas-m/mocksmtp"
	"github.com/stretchr/testify/require"
)

func Test_SendHireRequestReceived(t *testing.T) {
	u := models.User{
		Email: nulls.NewString("go6o@gmail.com"),
	}
	h := models.HireRequest{
		User: models.User{
			FirstName:  "иван",
			SecondName: "иванов",
		},
		Caretaker: models.User{},
		Individual: models.Individual{
			Name: "гошко",
		},
	}

	r := require.New(t)
	ms, v := smtp.(*mocksmtp.MockSMTP)
	r.True(v)
	// Clear SMTP queue after test
	defer ms.Clear()
	err := SendHireRequestReceived(u, h)
	r.NoError(err)
	// Test email
	r.Equal(1, ms.Count())
	m, err := ms.LastMessage()
	r.NoError(err)
	r.Equal("Garfild <noreply@garfild.me>", m.From)
	r.Equal("Получихте нова поръчка", m.Subject)
	r.Equal(1, len(m.To))
	r.Equal("go6o@gmail.com", m.To[0])
	r.Contains(m.Bodies[0].Content, "Иван Иванов")
	r.Contains(m.Bodies[0].Content, "лице Гошко")

}
