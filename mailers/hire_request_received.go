package mailers

import (
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/envy"
	"github.com/kolioDev/care/models"
)

func SendHireRequestReceived(u models.User, h models.HireRequest) error {
	m := mail.NewMessage()

	// fill in with your stuff:
	m.Subject = "Получихте нова поръчка"
	m.From = envy.Get("SMTP_FROM", "Garfild <noreply@garfild.me>")
	m.To = []string{u.Email.String}
	err := m.AddBody(r.HTML("hire_request_received.html"), map[string]interface{}{
		"individual": h.Individual,
		"sender":     h.User,
	})
	if err != nil {
		return err
	}

	m.Headers["Content-Type"] = "text/html"
	return smtp.Send(m)
}
