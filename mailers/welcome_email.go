package mailers

import (
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/uuid"
	"github.com/kolioDev/care/models"
)

func SendWelcomeEmails(u models.User) error {
	m := mail.NewMessage()

	m.Subject = "Добре дошли в Garfild"
	m.From = envy.Get("SMTP_FROM", "Garfild <noreply@garfild.me>")
	m.To = []string{u.Email.String}
	err := m.AddBody(r.HTML("welcome_email.html"), map[string]interface{}{
		"isCaretaker": u.CaretakerID.UUID != uuid.Nil,
	})
	if err != nil {
		return err
	}

	m.Headers["Content-Type"] = "text/html"
	return smtp.Send(m)
}
