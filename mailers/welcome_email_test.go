package mailers

import (
	"testing"

	"github.com/gobuffalo/uuid"

	"github.com/gobuffalo/nulls"
	"github.com/kolioDev/care/models"
	"github.com/stanislas-m/mocksmtp"
	"github.com/stretchr/testify/require"
)

func Test_SendRegistrationMail(t *testing.T) {
	r := require.New(t)
	ms, v := smtp.(*mocksmtp.MockSMTP)
	r.True(v)
	// Clear SMTP queue after test
	defer ms.Clear()
	err := SendWelcomeEmails(models.User{
		Email:       nulls.NewString("go6o@gmail.com"),
		CaretakerID: nulls.NewUUID(uuid.FromStringOrNil("45d48529-02ba-4f2d-bc6b-7cdc3ad214a5")),
	})
	r.NoError(err)
	// Test email
	r.Equal(1, ms.Count())
	m, err := ms.LastMessage()
	r.NoError(err)
	r.Equal("Garfild <noreply@garfild.me>", m.From)
	r.Equal("Добре дошли в Garfild", m.Subject)
	r.Equal(1, len(m.To))
	r.Equal("go6o@gmail.com", m.To[0])
	r.Contains(m.Bodies[0].Content, "Добре дошли в Garfild")
	r.Contains(m.Bodies[0].Content, "се нуждаят от Вашата грижа") //ако е caretaker
	r.NotContains(m.Bodies[0].Content, "гледачи за Вашите близки")

	ms.Clear()
	err = SendWelcomeEmails(models.User{
		Email:       nulls.NewString("go6o@gmail.com"),
		CaretakerID: nulls.NewUUID(uuid.Nil),
	})
	r.NoError(err)
	m, err = ms.LastMessage()
	r.NoError(err)

	r.NotContains(m.Bodies[0].Content, "се нуждаят от Вашата грижа")
	r.Contains(m.Bodies[0].Content, "гледачи за Вашите близки") //ако е смъртен потребител

}
