package mailers

import (
	"fmt"
	"log"
	"testing"

	"github.com/gobuffalo/nulls"
	"github.com/kolioDev/care/models"

	"github.com/stanislas-m/mocksmtp"
	"github.com/stretchr/testify/require"
)

func Test_EmailVerification(t *testing.T) {
	r := require.New(t)
	ms, v := smtp.(*mocksmtp.MockSMTP)
	r.True(v)

	u := &models.User{
		Password:             "qwerty",
		Email:                nulls.NewString("pe6o@gmail.com"),
		PasswordConfirmation: "qwerty",
		FirstName:            "стоян",
		SecondName:           "стоянов",
		Location:             models.Location{Lat: 13.33, Long: 14.44},
	}
	verrs, err := u.Create(models.DB)
	r.NoError(err)
	r.False(verrs.HasAny())
	e := models.EmailVerification{}
	verrs, err = e.Create(models.DB, *u)
	r.NoError(err)
	r.False(verrs.HasAny())

	// Clear SMTP queue after test
	defer ms.Clear()
	err = SendEmailVerifications(*u, e)
	r.NoError(err)
	// Test email
	r.Equal(1, ms.Count())
	m, err := ms.LastMessage()
	r.NoError(err)
	r.Equal("Garfild <noreply@garfild.me>", m.From)
	r.Equal("Потвърждение на Е-mail адрес", m.Subject)
	r.Equal(1, len(m.To))
	r.Equal("pe6o@gmail.com", m.To[0])
	log.Println("#email token", e.Token)
	r.Contains(m.Bodies[0].Content, fmt.Sprintf("http://127.0.0.1:3000/verify/email/%s", e.Token))
	r.Contains(m.Bodies[0].Content, "Стоян")
	r.Contains(m.Bodies[0].Content, "Стоянов")
}
