# Welcome to caretakers web app!

### Как тръгвата нещата ?
*изисквания - docker и docker-compose и  нищо друго ;)

      docker-compose up -d
      
      docker exec -t care_buffalo_web buffalo db migrate up
      
 *при грешки
   
       //при грешка с базата
       docker exec -t care_buffalo_web buffalo db  reset
       
       //при грешка с buffalo 
       docker-compose build   
    
### Kак работи 
* request-и към 
        
	    127.0.0.1:3000 

#API
List of endpoints on 127.0.0.1:3000/ и 127.0.0.1:3000/json

# Buffalo tasks
   ### Notify
   #####изпраща и запазва в базата известие към определен потребител
   
   ##### usage :
    docker exec -t care_buffalo_web buffalo task notify [userID uuid] [type string] [message string] [metaID uuid !optional] 
    
* **userID** - uuid-то на потребиеля        
* **type** - вида известие 
    * **messages.unread** - пример: `имате 4 непрочени съобщения` _!не изисква `metaID`_
    * **request.sent** - пример: `Успешно изпратена заявка към Гошо за лице Пешо` _!`metaID`= Id-то на заявката_
    * **request.received** - пример: `Гошо ви изпрати заявка за лице Пешо`  _!`metaID`= Id-то на заявката_
    * **request.accepted** - пример: `Гошо одобри заявката за лице Пешо` _!`metaID`= Id-то на заявката_
    * **request.declined** - пример: `Гошо отхвърли заявката за лице Пешо`  _!`metaID`= Id-то на заявката_
    * **message.received** - пример: `Гошо ви изпрати съобщение`  _!`metaID`= Id-то на chat стаята_
* **message** - текста на известието - пример: `имате 4 непрочени съобщения`
* **metaID** - ако вида съобщение изисква, връзката с друг компонент от приложението посредство уникалното му ID; 
Пример при заявка "Гошо ви изпрати заявка" `metaID` трябва да e id-то на заявката (виж **type**)

 ##### example :
    
    docker exec -t care_buffalo_web buffalo task notify 4d38e2ba-419a-4d98-929a-c88c67f4e97e messages.unread "Имате 101 непрочетени съобщения човееек"
    docker exec -t care_buffalo_web buffalo task notify 4d38e2ba-419a-4d98-929a-c88c67f4e97e request.sent "Успешно изпратена заявка към Гошо за лице Пешо" c39366f6-2593-489f-b407-038813454081
        
  ### Email:send-test
   ##### изпраща тестов email на потребител с даден email-адрес
   
   ##### usage :
    docker exec -t care_buffalo_web buffalo task email:send-test  [userEmail string]
    
* **userEmail** - userEmail някакъв, валиден Е-mail адрес


 ##### example :
    
    docker exec -t care_buffalo_web buffalo task  email:send-test go6o@pe6o.com
    
 ### Users:remove_unverified
   ##### Изтрива от базата потребителите с непотвърден E-mail
   
   ##### usage :
    docker exec -t care_buffalo_web buffalo task users:remove_unverified  [createdBefore? duration]
    
* **createdBefore** - duration Aко е зададено ще изтрие всички потребители създадени пред този период. 
Примерно при 2h ще изтрие всички непотвърдени user-и които са създадени преди повече от 2 часа.
Валидни duration-и - "ns", "us", "ms", "s", "m", "h"


 ##### example :
    
    docker exec -t care_buffalo_web buffalo task  remove_unverified
    docker exec -t care_buffalo_web buffalo task  remove_unverified 3.5h
    
    
### Users:verify
   ##### Потвърждава email-а на даден user
   
   ##### usage :
    docker exec -t care_buffalo_web buffalo task users:verify  [email string]
    
* **email** - E-mail-ът на потребителя 


 ##### example :
    
    docker exec -t care_buffalo_web buffalo task users:verify  go6o@pe6o.com