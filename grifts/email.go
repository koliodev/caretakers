package grifts

import (
	"github.com/kolioDev/care/mailers"
	"github.com/pkg/errors"

	. "github.com/markbates/grift/grift"
)

var _ = Namespace("email", func() {

	Desc("send-test", "Sends test email to a user \n [toEmail string]")
	Add("send-test", func(c *Context) error {

		if len(c.Args) < 1 {
			return errors.New("not enough arguments")
		}

		err := mailers.SendBlankEmails(c.Args[0])
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})

})
