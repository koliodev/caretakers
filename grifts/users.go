package grifts

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/gobuffalo/envy"
	"github.com/pusher/chatkit-server-go"
	"log"
	"time"

	"github.com/kolioDev/care/models"
	. "github.com/markbates/grift/grift"
	"github.com/pkg/errors"
)

var _ = Namespace("users", func() {

	//### ---- USERS:REMOVE UNVERIFIED
	Desc("remove_unverified", "Removes the users with unverified email address [createdBefore? duration]")
	Add("remove_unverified", func(c *Context) error {
		if len(c.Args) > 1 {
			return errors.New("too many arguments")
		}

		createdBefore := time.Now()
		if len(c.Args) == 1 {
			d, err := time.ParseDuration(c.Args[0])
			if err != nil {
				return errors.New("invalid param ")
			}
			createdBefore = time.Now().Add(-d)
		}

		us := models.Users{}
		models.DB.Where("verified=?", false).Where("updated_at<=?", createdBefore).All(&us)
		count := len(us)
		err := models.DB.Destroy(&us)
		if err != nil {
			return errors.WithStack(err)
		}

		log.Printf("successfully deleted %d users from the DB", count)
		return nil
	})

	//### ---- USERS:VERIFY
	Desc("verify", "Verifies user's email address [email string]")
	Add("verify", func(c *Context) error {
		if len(c.Args) != 1 {
			return errors.New("expected one argument")
		}

		u := &models.User{}
		err := models.DB.Where("email=?", c.Args[0]).First(u)
		if err != nil {
			if errors.Cause(err) == sql.ErrNoRows {
				return errors.New("cannot find such user")
			}
			return errors.WithStack(err)
		}

		if err := u.Verify(models.DB); err != nil {
			return errors.WithStack(err)
		}

		//Създава pusher chatkit user
			chatter, err  :=  chatkit.NewClient(
				envy.Get("PUSHER_CHATKIT_INSTANCE", "v1:us1:43ebc889-dded-4a0b-b3fc-24762ebe0065"),
				envy.Get("PUSHER_CHATKIT_KEY", "f7e63760-1a6e-4304-9595-47ceaa5fc159:/LAXZaOtsSO0AF9usGsv0GoRawDN4xj5d6V3RPMMmO0="),
			)
			if err !=nil{
				return errors.WithStack(err)
			}
			if err := chatter.CreateUser(context.Background(), chatkit.CreateUserOptions{
				ID:   u.ID.String(),
				Name: fmt.Sprintf("%s %s", u.FirstName, u.SecondName),
			});err!=nil{
				return errors.WithStack(err)
			}




		log.Printf("User with email %s successfully verified", u.Email.String)

		return nil
	})

})
