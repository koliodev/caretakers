package grifts

import (
	"github.com/gobuffalo/buffalo"
	"github.com/kolioDev/care/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
