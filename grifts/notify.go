package grifts

import (
	"fmt"

	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/uuid"
	"github.com/kolioDev/care/models"
	. "github.com/markbates/grift/grift"
	"github.com/pkg/errors"
	"github.com/pusher/pusher-http-go"
)

var _ = Desc("notify", "Sends and save to the DB notification to a user \n [userID uuid] [type string] [message string] [metaID uuid]")
var _ = Add("notify", func(c *Context) error {

	if len(c.Args) < 3 {
		return errors.New("not enough arguments")
	}

	uID := uuid.FromStringOrNil(c.Args[0])
	nType := c.Args[1]
	mess := c.Args[2]

	metaID := uuid.Nil
	if len(c.Args) > 3 {
		metaID = uuid.FromStringOrNil(c.Args[3])
	}

	if err := models.DB.Find(&models.User{}, uID); err != nil {
		return errors.New("cannot find user with id " + c.Args[0])
	}

	n := models.Notification{
		UserID:  uID,
		Type:    nType,
		Message: mess,
		MetaID:  nulls.NewUUID(metaID),
	}

	verrs, err := n.Create(models.DB)
	if err != nil {
		return errors.New(fmt.Sprintf("cannot create notification has err- %v", err.Error()))
	}
	if verrs.HasAny() {
		return errors.New(fmt.Sprintf("cannot create notification has verrs- %v", verrs.Errors))
	}

	//pusher broadcas
	if envy.Get("GO_ENV", "development") != "test" && envy.Get("GO_DISABLE_PUSHER", "false") != "true" {
		client := &pusher.Client{
			AppID:   envy.Get("PUSHER_APPID", "1234"),
			Key:     envy.Get("PUSHER_KEY", "ket"),
			Secret:  envy.Get("PUSHER_SECRET", "pa$$"),
			Cluster: envy.Get("PUSHER_CLUSTER", "eu"),
			Secure:  false,
		}
		err := client.Trigger(fmt.Sprintf("private-%s", uID.String()), "notification-sent", n)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	fmt.Println()
	fmt.Println("Notification sent and saved successfully")

	return nil
})
