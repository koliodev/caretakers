# This is a multi-stage Dockerfile and requires >= Docker 17.05
# https://docs.docker.com/engine/userguide/eng-image/multistage-build/
FROM gobuffalo/buffalo:v0.15.3 as builder

RUN mkdir -p $GOPATH/src/github.com/kolioDev/care
WORKDIR $GOPATH/src/github.com/kolioDev/care

ADD . .
ENV GO111MODULE=on
RUN go mod vendor


ENV DEVELOPMENT_DB_USER=postgres
ENV DEVELOPMENT_DB_PASSWORD=postgres
ENV DEVELOPMENT_DB_HOST=care_postgres_db
ENV ADDR=0.0.0.0
ENV FACEBOOK_KEY=2371958166401398
ENV FACEBOOK_SECRET=6ac57ae531c75f28ac87bf8a43ce3ded
ENV GOOGLE_KEY=679837228391-haia9irtk5vboacl0qq9g0so4i2317ni.apps.googleusercontent.com
ENV GOOGLE_SECRET=26yBZxOPs45CmjuDpfB5jWAb
ENV GOOGLE_CARETAKER_KEY=679837228391-3qeup1gsih6ercb7n0mapqa3sno247ep.apps.googleusercontent.com
ENV GOOGLE_CARETAKER_SECRET=aL60xrZ5f1rUn00Yb_D4hukQ
ENV FRONTEND_URL=http://127.0.0.1:8080
ENV FRONTEND_SIGNUP_URL_USER=/signup/user/:email/:provider/:provider_id
ENV FRONTEND_SIGNUP_URL_CARETAKER=/signup/caretaker/:email/:provider/:provider_id
ENV FRONTEND_LOGIN_URL=/login/:provider/:provider_id
ENV PUSHER_APPID=946863
ENV PUSHER_KEY=fe94235358bac0df8fad
ENV PUSHER_SECRET=dcfccbf393536cfe915a
ENV PUSHER_CLUSTER=eu
ENV PUSHER_CHATKIT_INSTANCE=v1:us1:43ebc889-dded-4a0b-b3fc-24762ebe0065
ENV PUSHER_CHATKIT_KEY=f7e63760-1a6e-4304-9595-47ceaa5fc159:/LAXZaOtsSO0AF9usGsv0GoRawDN4xj5d6V3RPMMmO0=


EXPOSE 3000


CMD buffalo dev

#RUN buffalo db migrate
