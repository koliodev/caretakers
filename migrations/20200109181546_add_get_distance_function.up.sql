CREATE FUNCTION public.get_distance(lat1 double precision, lng1 double precision, lat2 double precision, lng2 double precision) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
DECLARE radlat1 float8;
  DECLARE radlat2 float8;
  DECLARE theta float8;
  DECLARE radtheta float8;
  DECLARE PI float8;
  DECLARE  di float8;
BEGIN
  PI = 3.141592653589793;
  radlat1 = PI*lat1 / 180;
  radlat2 = PI*lat2 / 180;
  theta = lng1 - lng2;
  radtheta = PI*theta/180;
  di = sin(radlat1)*sin(radlat2) + cos(radlat1)*cos(radlat2)*cos(radtheta);
  if (di>1) then
    di = 1;
  end if;

  di = acos(di);
  di = di *180 / PI;
  di = di*60*1.1515;
  RETURN di;
END;
$$;