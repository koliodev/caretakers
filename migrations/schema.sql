--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: get_distance(double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_distance(lat1 double precision, lng1 double precision, lat2 double precision, lng2 double precision) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
DECLARE radlat1 float8;
  DECLARE radlat2 float8;
  DECLARE theta float8;
  DECLARE radtheta float8;
  DECLARE PI float8;
  DECLARE  di float8;
BEGIN
  PI = 3.141592653589793;
  radlat1 = PI*lat1 / 180;
  radlat2 = PI*lat2 / 180;
  theta = lng1 - lng2;
  radtheta = PI*theta/180;
  di = sin(radlat1)*sin(radlat2) + cos(radlat1)*cos(radlat2)*cos(radtheta);
  if (di>1) then
    di = 1;
  end if;

  di = acos(di);
  di = di *180 / PI;
  di = di*60*1.1515;
  RETURN di;
END;
$$;


ALTER FUNCTION public.get_distance(lat1 double precision, lng1 double precision, lat2 double precision, lng2 double precision) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: caretakers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.caretakers (
    id uuid NOT NULL,
    bio text NOT NULL,
    experience text NOT NULL,
    education text NOT NULL,
    skills character varying(255)[] NOT NULL,
    works_with character varying(255)[] NOT NULL,
    date_of_birth timestamp with time zone NOT NULL,
    tip_rate numeric NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    picture text NOT NULL
);


ALTER TABLE public.caretakers OWNER TO postgres;

--
-- Name: email_verifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.email_verifications (
    id uuid NOT NULL,
    user_id uuid NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.email_verifications OWNER TO postgres;

--
-- Name: hire_requests; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hire_requests (
    id uuid NOT NULL,
    caretaker_id uuid NOT NULL,
    individual_id uuid NOT NULL,
    user_id uuid NOT NULL,
    status character varying(255) NOT NULL,
    message character varying(524) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    weekly boolean NOT NULL,
    datetime jsonb NOT NULL,
    price numeric NOT NULL,
    room_id uuid NOT NULL
);


ALTER TABLE public.hire_requests OWNER TO postgres;

--
-- Name: individuals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.individuals (
    id uuid NOT NULL,
    user_id uuid NOT NULL,
    location jsonb NOT NULL,
    city character varying(255),
    name character varying(255) NOT NULL,
    date_of_birth timestamp with time zone NOT NULL,
    type character varying(255) NOT NULL,
    special_needs character varying(500) NOT NULL,
    about character varying(500) NOT NULL,
    picture text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.individuals OWNER TO postgres;

--
-- Name: messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messages (
    id uuid NOT NULL,
    hire_request uuid NOT NULL,
    sender uuid NOT NULL,
    delivered boolean NOT NULL,
    read boolean NOT NULL,
    message character varying(1024) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    receiver uuid NOT NULL,
    pusher_id integer NOT NULL
);


ALTER TABLE public.messages OWNER TO postgres;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notifications (
    id uuid NOT NULL,
    user_id uuid NOT NULL,
    message character varying(256) NOT NULL,
    type character varying(100) NOT NULL,
    read boolean NOT NULL,
    meta_id uuid,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.notifications OWNER TO postgres;

--
-- Name: schema_migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migration (
    version character varying(14) NOT NULL
);


ALTER TABLE public.schema_migration OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id uuid NOT NULL,
    email character varying(255),
    password character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    family_name character varying(255) NOT NULL,
    location jsonb NOT NULL,
    city character varying(255),
    caretaker uuid,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    provider character varying(255),
    provider_id character varying(255),
    verified boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: caretakers caretakers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.caretakers
    ADD CONSTRAINT caretakers_pkey PRIMARY KEY (id);


--
-- Name: email_verifications email_verifications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_verifications
    ADD CONSTRAINT email_verifications_pkey PRIMARY KEY (id);


--
-- Name: hire_requests hire_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hire_requests
    ADD CONSTRAINT hire_requests_pkey PRIMARY KEY (id);


--
-- Name: individuals individuals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.individuals
    ADD CONSTRAINT individuals_pkey PRIMARY KEY (id);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: schema_migration_version_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX schema_migration_version_idx ON public.schema_migration USING btree (version);


--
-- PostgreSQL database dump complete
--

