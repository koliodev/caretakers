package models

/**--------------------------------------------------------

				Caretaker.go

Модел за нещата свързани с гледачите
(гледаческата част от потребителя)

Методи:
	FindIndividuals() ->    намира подходящи лица
	GetCalendarInfo() ->    връща информация, кога
		     с какво е зает гледача (като поръчки)
	CheckAvailable() -> проверява, дали има приети
		                    поръчки за дадена дата

---------------------------------------------------------**/

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"github.com/gobuffalo/validate/validators"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
)

type Caretaker struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Bio        string    `json:"bio" db:"bio"`
	Experience string    `json:"experience" db:"experience"`
	Edu        string    `json:"education" db:"education"`
	Birth      time.Time `json:"birth_date" db:"date_of_birth"`
	HourRate   float32   `json:"hour_rate" db:"tip_rate"`

	Skills        []string `json:"skills" db:"-"`
	WorksWith     []string `json:"works_with" db:"-"`
	SkillsJson    string   `json:"-" db:"skills"`
	WorksWithJson string   `json:"-" db:"works_with"`

	Picture string `json:"picture" db:"picture"` //base64

	Rating int `json:"rating" db:"-"` //TODO::implement (отделна таблициа, чрез, която да се изчислява) (след успешно наемане да има функция rateCaretaker() )
}

// String is not required by pop and may be deleted
func (c Caretaker) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)

}

// Caratekers is not required by pop and may be deleted
type Caretakers []Caretaker

type WorkingTimeWithHR struct {
	WorkingTime
	HireRequestID      uuid.UUID `json:"hire_request_id"`
	HireRequestVerbose string    `json:"hire_request_verbose"`
}

// String is not required by pop and may be deleted
func (c Caretakers) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (c *Caretaker) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(

		&validators.StringIsPresent{Name: "bio", Field: c.Bio, Message: "Биографията е задължителна"},
		&validators.StringLengthInRange{Name: "bio", Field: c.Bio, Min: 10, Max: 1000, Message: "Биографията трябва да е между 10 и 1000 символа"},
		&validators.StringIsPresent{Name: "picture", Field: c.Picture, Message: "Снимката е задължителна"},

		&validators.StringLengthInRange{Name: "experience", Field: c.Experience, Min: 0, Max: 500, Message: "Предишният опит не трябва да надвишава 500 символа"},
		&validators.StringLengthInRange{Name: "education", Field: c.Edu, Min: 0, Max: 500, Message: "Образованието не трябва да надвишава 500 символа"},
		&validators.IntIsGreaterThan{Name: "skills", Field: len(c.Skills), Compared: 2, Message: "Необходими са поне 3 умения"},
		&validators.IntIsGreaterThan{Name: "works_with", Field: len(c.WorksWith), Compared: 0, Message: "Изберете поне 1 група"},
		&validators.TimeIsPresent{Name: "birth_date", Field: c.Birth, Message: "Датата на раждане е задължителна"},
		&validators.TimeIsBeforeTime{FirstName: "birth_date", FirstTime: c.Birth, SecondTime: time.Now().Add(-time.Hour * 24 * 356 * 14), Message: "Трябва да сте поне на 14 години"},
		&validators.TimeIsBeforeTime{FirstName: "birth_date", FirstTime: time.Date(1900, 1, 1, 1, 1, 1, 1, time.Local), SecondTime: c.Birth, Message: "Твърде ранна рожденна дата"},
		&validators.FuncValidator{Name: "hour_rate", Message: "Невалидна надница%s", Fn: func() bool {
			if c.HourRate < 0.01 || c.HourRate > 999 {
				return false
			}
			return true
		}},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (c *Caretaker) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (c *Caretaker) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

//AfterFind вика се след "намиране" на записан от базата
func (c *Caretaker) AfterFind(tx *pop.Connection) error {

	//{text, text} -> ["text", "text"]
	sanitize := func(s string) string {
		s = strings.Replace(s, "{", "[\"", 1)
		s = strings.Replace(s, "}", "\"]", 1)
		s = strings.Replace(s, ",", "\",\"", -1)
		s = strings.Replace(s, "\"\"", "\"", -1)
		return s
	}

	//{text, text} -> ["text", "text"]
	wwj := sanitize(c.WorksWithJson)
	sj := sanitize(c.SkillsJson)

	if err := json.Unmarshal([]byte(wwj), &c.WorksWith); err != nil {
		return errors.WithStack(err)
	}

	if err := json.Unmarshal([]byte(sj), &c.Skills); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//Create wrapper около логиката за създаване
func (c *Caretaker) Create(tx *pop.Connection) (*validate.Errors, error) {
	c.normalizeInput()
	return tx.ValidateAndCreate(c)
}

//Update wrapper около логиката за редакция
func (c *Caretaker) Update(tx *pop.Connection) (*validate.Errors, error) {
	c.normalizeInput()
	skipCols := []string{"created_at"}
	if c.Picture == "" {
		skipCols = append(skipCols, "picture")
	}
	return tx.ValidateAndUpdate(c, "created_at")
}

//FindIndividuals намира подходящи лица за въпросния гледач
// на база разтояние от него и вид на лицето
func (c *Caretaker) FindIndividuals(tx *pop.Connection, dist float32) (Individuals, error) {
	is := Individuals{}

	u := User{} //потребителя, към когото е link-нат caretaker-a
	if err := tx.Where("caretaker=?", c.ID).First(&u); err != nil {
		return nil, errors.WithStack(err)
	}

	q := tx.
		Where("public.get_distance(?, ?, (location ->>'latitude')::double precision,  (location->>'longitude')::double precision ) < ?",
			u.Location.Lat, u.Location.Long, dist).
		Where("type IN ( '" + strings.Join(c.WorksWith, "','") + "' ) ")

	err := q.All(&is)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return is, nil
}

//GetCalendarInfo връща масив с поетите ангацименти oт гледача
func (c *Caretaker) GetCalendarInfo(tx *pop.Connection) (map[interface{}][]WorkingTimeWithHR, error) {
	hrs := HireRequests{}
	if err := tx.Where("caretaker_id=?", c.ID).Where("status=?", "accepted").All(&hrs); err != nil {
		return nil, errors.WithStack(err)
	}

	hours := map[interface{}][]WorkingTimeWithHR{}

	for _, h := range hrs {
		for k, dt := range h.DateTimes {
			hours[k] = append(hours[k], WorkingTimeWithHR{
				dt, h.ID, h.Individual.Name,
			})
		}
	}
	return hours, nil
}

//CheckAvailable проверява, дали потребителя има вече приети заяки за някой ден
func (c *Caretaker) CheckAvailable(tx *pop.Connection, day interface{}, times WorkingTime) (bool, error) {
	weekDaysMap := map[time.Weekday]int{
		time.Monday: 0, time.Tuesday: 1,
		time.Wednesday: 2, time.Thursday: 3,
		time.Friday: 4, time.Saturday: 5, time.Sunday: 6,
	}

	//Проверява дали даден времеви период се засича с друг
	timeTouches := func(dt1, dt2 WorkingTime) bool {
		if dt1.Start > dt2.Start && dt1.Start < dt2.End {
			return true
		}
		if dt1.End > dt2.Start && dt1.End < dt2.End {
			return true
		}
		return false
	}

	//проверява, дали 2 дати съвпадат
	datesMatch := func(da1, da2 time.Time) bool {
		y1, m1, d1 := da1.Date()
		y2, m2, d2 := da2.Date()

		return y1 == y2 && m1 == m2 && d1 == d2
	}

	//Връща номера на деня от седмицата за дадена дата
	dateGetWeekDay := func(d time.Time) int {
		return weekDaysMap[d.Weekday()]
	}

	dtms, err := c.GetCalendarInfo(tx)
	if err != nil {
		return false, errors.WithStack(err)
	}

	//Aко денят е дата
	if dayDate, ok := day.(time.Time); ok {
		weekDay := dateGetWeekDay(dayDate) //week day
		for k, dt := range dtms {

			wd, isWeekDay := k.(int) //week day
			dd, _ := k.(time.Time)   //date

			//Aко деня, който върти е ден от седмицата и е същия ден от седмицата като запитваната дата
			// ИЛИ е дата и тя е същата
			if (isWeekDay && wd == weekDay) || (!isWeekDay && datesMatch(dd, dayDate)) {
				for _, d := range dt {
					if timeTouches(times, d.WorkingTime) {
						return false, nil
					}
				}
			}
		}
	}
	//Aко денят е ден от седмицата
	if weekDay, ok := day.(int); ok {
		for k, dt := range dtms {
			wd, isWeekDay := k.(int) //week day
			dd, _ := k.(time.Time)   //date

			//Aко деня, който върти е ден от седмицата и е същия ден от седмицата като запитвания
			// ИЛИ е дата и тази дата е същия ден от седмицата
			if (isWeekDay && wd == weekDay) || (!isWeekDay && dateGetWeekDay(dd) == weekDay) {
				for _, d := range dt {
					if timeTouches(times, d.WorkingTime) {
						return false, nil
					}
				}
			}
		}
	}

	return true, nil
}

//normalizeInput - нормализира въведените данни - име, фамилия email, location
func (c *Caretaker) normalizeInput() {
	//Премахва симовлите  [ { ] } от даден string,защото ще правят проблеми после
	var removeSpecial = func(array []string) []string {
		var A []string
		for i, w := range array {
			A = append(A, strings.Replace(w, "{", "", -1))
			A[i] = strings.Replace(A[i], "{", "", -1)
			A[i] = strings.Replace(A[i], "]", "", -1)
			A[i] = strings.Replace(A[i], "]", "", -1)
		}
		return A
	}

	ww, _ := json.Marshal(removeSpecial(c.WorksWith))
	c.WorksWithJson = strings.Replace(string(ww), "[", "{", 1)
	c.WorksWithJson = strings.Replace(string(c.WorksWithJson), "]", "}", 1)

	s, _ := json.Marshal(removeSpecial(c.Skills))
	c.SkillsJson = strings.Replace(string(s), "[", "{", 1)
	c.SkillsJson = strings.Replace(string(c.SkillsJson), "]", "}", 1)
}
