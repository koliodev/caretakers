package models

/**--------------------------------------------------------

				User.go

Модел за нещата свързани с потребителите

Методи:
	AddIndividual() -> Добавя (запазва в базата) лице
	RequestHire()   -> Изпраща заявка към гледач
							(запазва я в базата)


---------------------------------------------------------**/

import (
	"encoding/json"
	"math"
	"strings"
	"time"

	"github.com/gobuffalo/validate/validators"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/imdario/mergo"
)

type User struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Email      nulls.String `json:"email" db:"email"`
	FirstName  string       `json:"first_name" db:"name"`
	SecondName string       `json:"second_name" db:"family_name"`

	Provider   nulls.String `json:"provider" db:"provider"`
	ProviderID nulls.String `json:"provider_id" db:"provider_id"`

	Location     Location     `json:"location" db:"-"`
	LocationJSON string       `json:"-" db:"location"`
	City         nulls.String `json:"city" db:"city"`

	CaretakerID nulls.UUID `json:"-" db:"caretaker"`
	Caretaker   *Caretaker `json:"caretaker"  db:"-"`

	PasswordHash         string `json:"-" db:"password"`
	Password             string `json:"password,omitempty" db:"-"`
	PasswordConfirmation string `json:"password_confirmation,omitempty" db:"-" `

	Verified bool `json:"-" db:"verified"`

	Individuals Individuals `json:"individuals"  db:"-"`
}

type Location struct {
	Lat  float64 `json:"latitude"`
	Long float64 `json:"longitude"`
}

// String is not required by pop and may be deleted
func (u User) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Users is not required by pop and may be deleted
type Users []User

// String is not required by pop and may be deleted
func (u Users) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (u *User) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error

	return validate.Validate(&validators.EmailIsPresent{Name: "email", Field: u.Email.String, Message: "Невалиден е-mail адрес"},
		&validators.StringLengthInRange{Name: "email", Field: u.Email.String, Min: 4, Max: 150, Message: "E-mail адресът трябва да е между 4 и 150 символа"},

		&validators.StringLengthInRange{Name: "city", Field: u.City.String, Min: 0, Max: 250, Message: "Името на населеното място трябва да не надвишава 250 символа"},

		&validators.StringIsPresent{Name: "first_name", Field: u.FirstName, Message: "Името е задължително"},
		&validators.StringIsPresent{Name: "second_name", Field: u.SecondName, Message: "Фамилията е задължителна"},
		&validators.StringLengthInRange{Name: "first_name", Field: u.FirstName, Max: 50, Min: 2, Message: "Името трябва да е между 2 и 50 символа"},
		&validators.StringLengthInRange{Name: "second_name", Field: u.SecondName, Max: 50, Min: 2, Message: "Фамилията трябва да е между 2 и 50 символа"},
		StringIsAlpha{Name: "first_name", Field: u.FirstName, Message: "Името може да садържа само букви"},
		StringIsAlpha{Name: "second_name", Field: u.SecondName, Message: "Фамилията може да садържа само букви"},

		&validators.IntIsLessThan{Name: "location", Field: int(math.Round(u.Location.Lat)), Compared: 90, Message: "Невалидно местоположение"},
		&validators.IntIsLessThan{Name: "location", Field: int(math.Round(u.Location.Long)), Compared: 180, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(u.Location.Lat)), Compared: -90, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(u.Location.Long)), Compared: -180, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(math.Abs(u.Location.Lat))), Compared: 0, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(math.Abs(u.Location.Long))), Compared: 0, Message: "Невалидно местоположение"},

		&validators.FuncValidator{
			Field:   nulls.String(u.Email).String,
			Name:    "Email",
			Message: "Вече съществува потребител с e-mail адрес %s",
			Fn: func() bool {
				var b bool
				q := tx.Where("email = ?", u.Email)
				if u.ID != uuid.Nil {
					q = q.Where("id != ?", u.ID)
				}
				b, err = q.Exists(u)
				if err != nil {
					return false
				}
				return !b
			},
		},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (u *User) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	var vals []validate.Validator
	if !u.Provider.Valid {
		vals = append(vals,
			&validators.StringIsPresent{Name: "password", Field: u.Password, Message: "Паролата е задължителна"},
			&validators.StringIsPresent{Name: "password_confirmation", Field: u.Password, Message: "Потвърждението на паролата е задължително"},
			&validators.StringLengthInRange{Name: "password", Field: u.Password, Min: 6, Max: 100, Message: "Паролата трябва да е между 6 и 100 символа"},
			&validators.StringsMatch{Name: "password_confirmation", Field: u.Password, Field2: u.PasswordConfirmation, Message: "Паролите не съвпадат"},
		)
	} else {
		vals = append(vals,
			&validators.StringIsPresent{Name: "provider_id", Field: u.ProviderID.String, Message: "Provider ID-то е задължително"},
			&validators.RegexMatch{Name: "provider", Field: u.Provider.String, Expr: "(google_user|google_caretaker|facebook_user|facebook_caretaker)", Message: "Невалиден OAuth provider"},
		)
	}
	return validate.Validate(vals...), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method 	is not required and may be deleted.
func (u *User) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	var vals []validate.Validator
	//потребителя Преди промените
	var oldU User
	if err := tx.Find(&oldU, u.ID); err != nil {
		return validate.NewErrors(), errors.WithStack(err)
	}

	//Ако потребиеля си променя паролата
	if u.PasswordConfirmation != "" || u.Password != "" {
		vals = append(vals,
			&validators.StringIsPresent{Name: "password", Field: u.Password, Message: "Паролата е задължителна"},
			&validators.StringIsPresent{Name: "password_confirmation", Field: u.Password, Message: "Потвърждението на паролата е задължително"},
			&validators.StringLengthInRange{Name: "password", Field: u.Password, Min: 6, Max: 100, Message: "Паролата трябва да е между 6 и 100 символа"},
			&validators.StringsMatch{Name: "password_confirmation", Field: u.Password, Field2: u.PasswordConfirmation, Message: "Паролите не съвпадат"},
		)
	}

	//проверка да не се променят provider нещата
	vals = append(vals,
		&validators.StringsMatch{Name: "provider_id", Field: u.ProviderID.String, Field2: oldU.ProviderID.String, Message: "Не може да променяте oauth provider id-то"},
		&validators.StringsMatch{Name: "provider", Field: u.Provider.String, Field2: oldU.Provider.String, Message: "Не може да променяте oauth provider-а"},
		&validators.StringsMatch{Name: "caretaker", Field: u.CaretakerID.UUID.String(), Field2: oldU.CaretakerID.UUID.String(), Message: "Не може да променяте caretaker-id то"},
	)

	return validate.Validate(vals...), nil
}

//AfterFind вика се след "намиране" на записан от базата
func (u *User) AfterFind(tx *pop.Connection) error {

	//Попълва структурата с местополжението
	if err := json.Unmarshal([]byte(u.LocationJSON), &u.Location); err != nil {
		return errors.WithStack(err)
	}

	//Намира допълнителните данни, ако е caretaker
	if u.CaretakerID.Valid {
		u.Caretaker = &Caretaker{}
		if err := tx.Find(u.Caretaker, &u.CaretakerID); err != nil {
			return errors.WithStack(err)
		}
	} else {
		u.Caretaker = nil
	}

	//Намира добавените от този потребителя лица
	if err := tx.Where("user_id=?", u.ID).All(&u.Individuals); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

// Create wrapper около логиката за създаване на потребител
// Пуска валидации
// прави хаш на паролата
// запазва правилно местоположението
// Create(tx, true) - създава потребител който е caretaker
func (u *User) Create(tx *pop.Connection, args ...interface{}) (*validate.Errors, error) {
	isCaretaker := len(args) > 0 && args[0].(bool)

	u.Verified = false
	u.normalizeInput()

	//хешира паролата
	ph, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return validate.NewErrors(), errors.WithStack(err)
	}
	u.PasswordHash = string(ph)

	//ако потребителят е caretaker - добавя грешките
	verrs := validate.NewErrors()
	if isCaretaker {
		if careVerrs, _ := u.Caretaker.Validate(tx); careVerrs.HasAny() {
			for k, e := range careVerrs.Errors {
				for _, s := range e {
					verrs.Add("caretaker."+k, s)
				}
			}
		}
	}

	//грешки при валидацията на потребителя
	uVerrs, _ := u.Validate(tx)
	uVerrsCreate, _ := u.ValidateCreate(tx)
	mergo.Merge(&uVerrs.Errors, uVerrsCreate.Errors) //merge-ва грешките от Validate и ValidateCreate
	for k, e := range uVerrs.Errors {
		for _, s := range e {
			verrs.Add(k, s)
		}
	}

	//ако няма грешки нито в User нито в Caretaker
	if !verrs.HasAny() {
		if isCaretaker {
			u.Caretaker.Create(tx)
			u.CaretakerID = nulls.NewUUID(u.Caretaker.ID)
		}
		tx.Create(u)
		e := EmailVerification{}
		return e.Create(tx, *u)
	}

	return verrs, nil
}

// Update wrapper  около логиката за промяна  на потребител
func (u *User) Update(tx *pop.Connection) (*validate.Errors, error) {
	u.normalizeInput()
	oldU := &User{}
	tx.Find(oldU, u.ID)
	u.CaretakerID = oldU.CaretakerID
	u.ProviderID = oldU.ProviderID
	u.Provider = oldU.Provider

	skipCols := []string{"provider_id", "provider", "caretaker", "created_at", "verified", "email"}

	//хешира паролата, ако има зададена такава
	if u.Password != "" {
		ph, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		if err != nil {
			return validate.NewErrors(), errors.WithStack(err)
		}
		u.PasswordHash = string(ph)
	} else {
		skipCols = append(skipCols, "password")
	}

	return tx.ValidateAndUpdate(u, skipCols...)
}

func (u *User) AddIndividual(tx *pop.Connection, i *Individual) (*validate.Errors, error) {
	i.UserID = u.ID
	return i.Create(tx)
}

//RequestHire прави заявка за наемане на caretaker
func (u *User) RequestHire(tx *pop.Connection, msg string, weekly bool, dateTimes map[interface{}]WorkingTime, c Caretaker, i Individual) (*HireRequest, *validate.Errors, error) {

	verrs := validate.NewErrors()

	//проверка, дали вече има същия pending

	if b, _ := tx.Where("caretaker_id=?", c.ID).Where("individual_id=?", i.ID).Where("status=?", "pending").Exists(&HireRequest{}); b {
		verrs.Add("duplicate", "request already exists")
		return nil, verrs, nil
	}

	if u.CaretakerID.UUID == c.ID {
		verrs.Add("caretaker", "invalid caretaker")
		return nil, verrs, nil
	}

	//Проверява, дали има предишен request между същите хора, за да зададе roomID-то
	sameConvH := &HireRequest{} //same conversation hire request
	tx.Where("user_id=?", u.ID).Where("caretaker_id=?", c.ID).First(sameConvH)

	roomID := uuid.Nil
	if sameConvH.ID != uuid.Nil {
		roomID = sameConvH.ID
	}

	h := &HireRequest{
		UserID:       u.ID,
		CaretakerID:  c.ID,
		IndividualID: i.ID,
		RoomID:       roomID,
		Status:       "pending",
		Message:      msg,
		Weekly:       weekly,
		DateTimes:    dateTimes,
	}

	h.User = *u

	verr, err := tx.ValidateAndCreate(h)

	return h, verr, err

}

//Потвърждава някой потребител (вика се когато се установи, че email-ът наистина принадлежи на човека)
func (u *User) Verify(tx *pop.Connection) error {
	u.Verified = true
	err := tx.Update(u,
		"created_at", "email", "name",
		"family_name", "provider", "provider_id",
		"location", "city", "caretaker", "password")
	if err != nil {
		return errors.WithStack(err)
	}

	if err := tx.RawQuery("DELETE FROM email_verifications WHERE user_id=?;", u.ID).Exec(); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

//BeforeDestroy вика се преди изтриване на потреибителя от базата
//Изтрива и записа в таблицата с caretaker-ите (ако има такъв)
func (u *User) BeforeDestroy(tx *pop.Connection) error {
	if err := tx.Find(u, u.ID); err != nil {
		return errors.WithStack(err)
	}

	//Изтрива email потвържденията
	evs := &EmailVerifications{}
	tx.Where("user_id=?", u.ID).All(evs)
	tx.Destroy(evs)

	//Изтрива caretaker info-то (ако има такова)
	if u.CaretakerID.Valid {
		if err := tx.Find(u.Caretaker, u.CaretakerID.UUID); err != nil {
			return errors.WithStack(err)
		}
		return tx.Destroy(u.Caretaker)
	}

	return nil
}

//normalizeInput - нормализира въведените данни - име, фамилия email, location
func (u *User) normalizeInput() {
	// прави email-a lower case
	u.Email = nulls.NewString(strings.ToLower(nulls.String(u.Email).String))

	//json stringify-ва местоположението
	lb, _ := json.Marshal(u.Location)
	u.LocationJSON = string(lb)

	//прави имената all lower case
	u.SecondName = strings.ToLower(u.SecondName)
	u.FirstName = strings.ToLower(u.FirstName)
}
