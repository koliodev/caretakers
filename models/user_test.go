package models

import (
	"fmt"
	"log"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gofrs/uuid"
)

func (ms *ModelSuite) Test_User_Create() {
	u := &User{
		Password:             "qwerty",
		Email:                nulls.NewString("test@gmail.com"),
		PasswordConfirmation: "qwerty",
		FirstName:            "Стоян",
		SecondName:           "Стоянов",
		Location:             Location{Lat: 13.33, Long: 14.44},
	}

	//Creates string with fixed length
	str := func(l int, append string) (s string) {
		for i := 0; i < l; i++ {
			s = s + append
		}
		return
	}

	ms.DBDelta(1, "users", func() {
		ms.DBDelta(1, "email_verifications", func() {
			verrs, err := u.Create(ms.DB)
			ms.NoError(err)
			ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))
			newU := &User{}
			ms.NoError(ms.DB.Find(newU, u.ID))
			ms.Equal(false, newU.Verified)
		})
	})

	//Email
	u.ID = uuid.Nil
	verrs, err := u.Create(ms.DB)
	ms.True(verrs.HasAny(), fmt.Sprintf("Should have verrs, but this happens %v", verrs.Errors))
	ms.Contains(verrs.Get("email"), "Вече съществува потребител с e-mail адрес test@gmail.com")

	u.Email = nulls.NewString("invalid.com")
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("email"), "Невалиден е-mail адрес")

	u.Email = nulls.NewString("i")
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("email"), "E-mail адресът трябва да е между 4 и 150 символа")

	u.Email = nulls.NewString(str(151, "a"))
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("email"), "E-mail адресът трябва да е между 4 и 150 символа")

	//Име
	u.Email = nulls.NewString("valid@user.email")
	u.FirstName = ""
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("first_name"), "Името е задължително")

	u.FirstName = "н"
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("first_name"), "Името трябва да е между 2 и 50 символа")

	u.FirstName = str(51, "a")
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("first_name"), "Името трябва да е между 2 и 50 символа")

	u.FirstName = "Иван56"
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("first_name"), "Името може да садържа само букви")
	u.FirstName = "Мария-Антоанета"

	//Фамилия
	u.SecondName = ""
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("second_name"), "Фамилията е задължителна")

	u.SecondName = "н"
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("second_name"), "Фамилията трябва да е между 2 и 50 символа")

	u.SecondName = str(51, "a")
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("second_name"), "Фамилията трябва да е между 2 и 50 символа")

	u.SecondName = "Иван56"
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("second_name"), "Фамилията може да садържа само букви")
	u.SecondName = "Стефанова"

	//Град
	u.City = nulls.NewString(str(251, "a"))
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("city"), "Името на населеното място трябва да не надвишава 250 символа")
	u.City = nulls.NewString("Dobrich 9300")

	//Местоположение
	//дължина
	u.Location.Long = 180.1
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("location"), "Невалидно местоположение")

	u.Location.Long = -180.1
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("location"), "Невалидно местоположение")

	u.Location.Long = 0
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("location"), "Невалидно местоположение")
	u.Location.Long = 101.123

	//ширина
	u.Location.Lat = 90.1
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("location"), "Невалидно местоположение")

	u.Location.Lat = -93.1
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("location"), "Невалидно местоположение")

	u.Location.Lat = 0
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("location"), "Невалидно местоположение")
	u.Location.Lat = 32.34

	//Парола
	u.Password = ""
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("password"), "Паролата е задължителна")

	u.Password = "i"
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("password"), "Паролата трябва да е между 6 и 100 символа")

	u.Password = str(101, "a")
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("password"), "Паролата трябва да е между 6 и 100 символа")

	u.Password = "password"
	u.PasswordConfirmation = ""
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("password_confirmation"), "Паролите не съвпадат")
	u.PasswordConfirmation = "password"

	//Проверки за oauth provider-a
	u.Password = ""
	u.PasswordConfirmation = ""
	u.Provider = nulls.NewString("ivan")
	u.ProviderID = nulls.NewString("")
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("provider"), "Невалиден OAuth provider")
	ms.Contains(verrs.Get("provider_id"), "Provider ID-то е задължително")

	u.Provider = nulls.NewString("google_user")
	u.ProviderID = nulls.NewString("123456789")

	ms.DBDelta(1, "users", func() {
		verrs, err := u.Create(ms.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v when saving user %v", verrs.Errors, u))
	})

	//#### -------- АКО Е CARETAKER --------
	u.ID = uuid.Nil
	u.Email = nulls.NewString("newStillValid@emai.com")

	u.Caretaker = &Caretaker{
		Bio:        "Алеко Иваницов Константинов, познат под псевдонима Щастливеца, е български писател, адвокат, общественик и основател на организираното туристическо движение в България.",
		Experience: "Работа с деца, коне и кучета",
		Edu:        "",
		Skills:     []string{"плетене", "заваряне", "струговане"},
		WorksWith:  []string{"деца", "кучета"},
		HourRate:   10.50,
		Birth:      time.Date(2001, 1, 19, 0, 0, 0, 0, time.UTC),
		Picture:    "base64",
	}

	ms.DBDelta(1, "users", func() {
		ms.DBDelta(1, "caretakers", func() {
			verrs, err := u.Create(ms.DB, true)
			ms.NoError(err)
			ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))
		})
	})

	//няма нужда да проверявам всички полета на  u.Caretaker (проверени във caretaker_test.go)
	u.Caretaker.Bio = ""
	verrs, err = u.Create(ms.DB, true)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("caretaker.bio"), "Биографията трябва да е между 10 и 1000 символа")

	//проверка дали merge-ва грешките
	u.Email = nulls.NewString("invalid")
	verrs, err = u.Create(ms.DB, true)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("caretaker.bio"), "Биографията трябва да е между 10 и 1000 символа")
	ms.Contains(verrs.Get("email"), "Невалиден е-mail адрес")

}

func (ms *ModelSuite) Test_User_Update() {
	u := &User{
		Password:             "qwerty",
		Email:                nulls.NewString("test@gmail.com"),
		PasswordConfirmation: "qwerty",
		FirstName:            "Стоян",
		SecondName:           "Стоянов",
		Location:             Location{Lat: 13.33, Long: 14.44},
	}
	updatedU := &User{}

	//Запаза го в базата
	ms.DBDelta(1, "users", func() {
		verrs, err := u.Create(ms.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))
	})

	u.Password = ""
	u.PasswordConfirmation = ""
	u.PasswordHash = ""

	//всичко както си е
	verrs, err := u.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))

	//#### --------- Промяна на email  ---------  ####
	//####  ЗА СЕГА НЕ СЕ ПОДДЪРЖА, ЗАРАДИ НУЖНАТА ВАЛИДАЦИЯ НА Email-a
	////невалидно
	//u.Email = nulls.NewString("go6o")
	//verrs, err = u.Update(ms.DB)
	//ms.NoError(err)
	//ms.True(verrs.HasAny())
	//ms.Contains(verrs.Get("email"), "Невалиден е-mail адрес")
	////валидно
	//u.Email = nulls.NewString("completelyValid@yahoo.com")
	//verrs, err = u.Update(ms.DB)
	//ms.NoError(err)
	//ms.False(verrs.HasAny())
	//ms.NoError(ms.DB.Find(updatedU, u.ID))
	//ms.Equal("completelyvalid@yahoo.com", updatedU.Email.String)

	//#### --------- Промяна на име  ---------  ####
	u.FirstName = "g"
	verrs, err = u.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("first_name"), "Името трябва да е между 2 и 50 символа")
	//валидно
	u.FirstName = "Герасим"
	verrs, err = u.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NoError(ms.DB.Find(updatedU, u.ID))
	ms.Equal("герасим", updatedU.FirstName)

	//#### --------- Промяна на местоположение  ---------  ####
	u.Location.Long = 180.1
	verrs, err = u.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("location"), "Невалидно местоположение")
	//валидно
	u.Location.Long = 45.23
	verrs, err = u.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NoError(ms.DB.Find(updatedU, u.ID))
	ms.Equal(45.23, updatedU.Location.Long)

	//#### Промяна на парола
	u.Password = "123"
	u.PasswordConfirmation = "321"
	verrs, err = u.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("password_confirmation"), "Паролите не съвпадат")
	ms.Contains(verrs.Get("password"), "Паролата трябва да е между 6 и 100 символа")

	//валидно
	u.Password = "topSecretPass"
	u.PasswordConfirmation = "topSecretPass"
	verrs, err = u.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NoError(ms.DB.Find(updatedU, u.ID))
	ms.NotEqual(u.PasswordHash, updatedU.Location.Long)

	//#### Промяна на provider id (не може) + Промяна на provider  (не може) + Промяна на caretaker (не може)
	//не връща грешка, просто не пипа тези полета
	oldU := u
	u.ProviderID = nulls.NewString("go6o")
	u.Provider = nulls.NewString("google")
	verrs, err = u.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))
	ms.NoError(ms.DB.Find(updatedU, u.ID))
	ms.Equal(oldU.ProviderID, updatedU.ProviderID)
	ms.Equal(oldU.Provider, updatedU.Provider)
	ms.Equal(oldU.CaretakerID, updatedU.CaretakerID)

}

func (ms *ModelSuite) Test_User_AfterFind() {
	u := &User{
		Password:             "qwerty",
		Email:                nulls.NewString("Test_User_AfterFind@gmail.com"),
		PasswordConfirmation: "qwerty",
		FirstName:            "Стоян",
		SecondName:           "Стоянов",
		Location:             Location{Lat: 13.33, Long: 14.44},
		Caretaker: &Caretaker{
			Bio:        "Алеко Иваницов Константинов, познат под псевдонима Щастливеца, е български писател, адвокат, общественик и основател на организираното туристическо движение в България.",
			Experience: "Работа с деца, коне и кучета",
			Edu:        "",
			Skills:     []string{"плетене", "заваряне", "струговане"},
			WorksWith:  []string{"деца", "кучета"},
			HourRate:   10.50,
			Birth:      time.Date(2001, 1, 19, 0, 0, 0, 0, time.UTC),
			Picture:    "base65",
		},
		Individuals: Individuals{
			{
				Name:  "Гошко",
				Birth: time.Date(2011, 1, 19, 0, 0, 0, 0, time.Local),
				Type:  "дете",
				Needs: "",
				About: "Много готино игриво, трябва му бавачка",
				Location: Location{
					Lat:  12.33,
					Long: 123.33,
				},
				City: nulls.NewString("Доперич"),
			},
			{
				Name:  "Иван",
				Birth: time.Date(1980, 1, 19, 0, 0, 0, 0, time.Local),
				Type:  "старец",
				Needs: "",
				About: "Възрастен и бавен",
				Location: Location{
					Lat:  12.33,
					Long: 123.33,
				},
				City: nulls.NewString("Силистра"),
			},
		},
	}

	var fu User // found user

	verrs, err := u.Create(ms.DB, true)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))

	for _, i := range u.Individuals {
		i.UserID = u.ID
		verrs, err := i.Create(ms.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))
	}

	ms.NoError(ms.DB.Find(&fu, u.ID))

	ms.Equal(u.FirstName, fu.FirstName)
	ms.Equal(u.SecondName, fu.SecondName)
	ms.Equal(u.Location.Lat, fu.Location.Lat)
	ms.Equal(u.Location.Long, fu.Location.Long)
	ms.Equal(u.Caretaker.Bio, fu.Caretaker.Bio)
	ms.Equal(u.Caretaker.Experience, fu.Caretaker.Experience)
	ms.Equal(u.Caretaker.Edu, fu.Caretaker.Edu)
	ms.Equal(u.Caretaker.Skills, fu.Caretaker.Skills)
	ms.Equal(u.Caretaker.WorksWith, fu.Caretaker.WorksWith)
	ms.Equal(u.Caretaker.HourRate, fu.Caretaker.HourRate)
	ms.Equal(len(u.Individuals), len(fu.Individuals))
	ms.Equal(u.Individuals[0].Location.Long, fu.Individuals[0].Location.Long)
	ms.Equal(u.Individuals[0].Location.Lat, fu.Individuals[0].Location.Lat)
	ms.Equal(u.Individuals[0].Name, fu.Individuals[0].Name)

}

func (ms *ModelSuite) Test_User_BeforeDestroy() {
	ms.LoadFixture("a lot of caretakers")
	u := User{}

	ms.NoError(ms.DB.First(&u))
	ms.NotEqual(uuid.Nil, u.CaretakerID.UUID)

	cID := u.CaretakerID.UUID
	ms.DBDelta(-1, "users", func() {
		ms.DBDelta(-1, "caretakers", func() {
			ms.NoError(ms.DB.Destroy(&u))
		})
	})

	exists, _ := ms.DB.Where("id=?", cID).Exists(&Caretaker{})
	ms.False(exists)

	//Aко потреибтелят не е caretaker
	ms.LoadFixture("login user")
	ms.NoError(ms.DB.Where("email=?", "login@user.email").First(&u))
	ms.DBDelta(-1, "users", func() {
		ms.DBDelta(0, "caretakers", func() {
			ms.NoError(ms.DB.Destroy(&u))
		})
	})

	//Aко потреибтелят име изпратен Е-mail с верификация
	ms.LoadFixture("a lot of email verifications")
	ms.NoError(ms.DB.Where("email=?", "fixtureEV1@user.email").First(&u))
	uID := u.ID
	ms.DBDelta(-1, "users", func() {
		ms.DBDelta(0, "caretakers", func() {
			ms.DBDelta(-1, "email_verifications", func() {
				ms.NoError(ms.DB.Destroy(&u))
			})
		})
	})
	exists, _ = ms.DB.Where("user_id=?", uID).Exists(&EmailVerification{})
	ms.False(exists)
}

func (ms *ModelSuite) Test_User_AddIndividual() {
	ms.LoadFixture("a lot of users")
	ms.LoadFixture("a lot of individuals")

	var u User
	ms.NoError(ms.DB.Where("email=?", "fixture1@user.email").First(&u))

	var i Individual
	ms.NoError(ms.DB.Where("name=?", "fixtureИван").First(&i))
	i.ID = uuid.Nil

	verrs, err := u.AddIndividual(ms.DB, &i)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs, but this happens %v", verrs.Errors))

	ms.NotEqual(uuid.Nil, i.ID)

}

func (ms *ModelSuite) Test_User_RequestHire() {
	ms.LoadFixture("a lot of users")
	ms.LoadFixture("a lot of individuals")
	ms.LoadFixture("a lot of caretakers")

	var u User
	var i Individual
	var i2 Individual
	var c Caretaker

	ms.NoError(ms.DB.Where("email=?", "fixture1@user.email").First(&u))
	ms.NoError(ms.DB.Where("name=?", "fixtureИван").First(&i))
	ms.NoError(ms.DB.Where("name=?", "fixtureГошо").First(&i2))
	ms.NoError(ms.DB.Where("bio=?", "Биографията на гледач-потребител 2").First(&c))

	dts := map[interface{}]WorkingTime{
		1: {12.34, 21.22},
	}
	//Невалидно съобщение
	_, verrs, e := u.RequestHire(ms.DB, "", true, dts, c, i)
	ms.NoError(e)
	ms.Contains(fmt.Sprint(verrs.Get("message")[0]), "Съобщението трябва да е")

	//ако всичко е ok
	h := HireRequest{}
	ms.DBDelta(1, "hire_requests", func() {
		_, verrs, err := u.RequestHire(ms.DB, "Моля да гледаш Иван", true, dts, c, i)
		ms.NoError(err)
		ms.False(verrs.HasAny())
		err = ms.DB.Where("user_id=?", u.ID).
			Where("individual_id=?", i.ID).
			Where("caretaker_id=?", c.ID).
			Where("status=?", "pending").First(&h)

		ms.NoError(err)
		ms.Equal(h.ID, h.RoomID)
		ms.NotEqual(h.ID, uuid.Nil)
	})

	//Втори, идентичен request
	_, verrs, e = u.RequestHire(ms.DB, "Моля да гледаш Иван", true, dts, c, i)
	ms.NoError(e)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("duplicate")[0], "already exists")

	i.ID = uuid.FromStringOrNil("23fd7e6a-1baa-4b2e-b9da-72c28b2524fd")

	//Втори request към същия caretaker, но за различно лице
	ms.DBDelta(1, "hire_requests", func() {
		_, verrs, err := u.RequestHire(ms.DB, "Моля да гледаш Иван", true, dts, c, i2)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs but got %v", verrs.Errors))
		h2 := HireRequest{}
		err = ms.DB.Where("user_id=?", u.ID).
			Where("individual_id=?", i2.ID).
			Where("caretaker_id=?", c.ID).
			Where("status=?", "pending").First(&h2)

		ms.NoError(err)
		ms.Equal(h.RoomID, h2.RoomID)
		ms.NotEqual(h2.RoomID, uuid.Nil)
		ms.NotEqual(h2.ID, uuid.Nil)
	})

	//ако user-a и caretaker-а съвпадат
	u.CaretakerID = nulls.NewUUID(c.ID)
	_, verrs, e = u.RequestHire(ms.DB, "Моля да гледаш Иван", true, dts, c, i)
	ms.NoError(e)
	ms.True(verrs.HasAny())
	ms.Contains(fmt.Sprint(verrs.Get("caretaker")), "invalid caretaker")
	u.CaretakerID = nulls.NewUUID(uuid.FromStringOrNil("24fd9e6a-1baa-4b2e-b9da-72c28b2524fd"))

	//ако има невалидни id-ta
	u.ID = uuid.Nil
	_, verrs, e = u.RequestHire(ms.DB, "Моля да гледаш Иван", true, dts, c, i)
	ms.NoError(e)
	ms.True(verrs.HasAny())
	log.Println(verrs.Errors)
	ms.Contains(fmt.Sprint(verrs.Get("user_id")), "user_id can not be blank")

}

func (ms *ModelSuite) Test_User_Verify() {
	ms.LoadFixture("a lot of email verifications")
	u := &User{}
	ms.NoError(ms.DB.Where("verified=?", false).First(u))

	ms.Equal(false, u.Verified)
	ms.DBDelta(-1, "email_verifications", func() {
		ms.NoError(u.Verify(ms.DB))
	})

	ms.Equal(true, u.Verified)
}
