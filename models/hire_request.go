package models

/**--------------------------------------------------------

				HireRequest.go

Модел за нещата свързани с поръчките
(заявките към гледач)

Методи:
	ChangeStatus() -> приема/отказва поръчка

---------------------------------------------------------**/

import (
	"encoding/json"
	"math"
	"reflect"
	"time"

	"github.com/gobuffalo/validate/validators"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
)

type HireRequest struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	RoomID uuid.UUID `json:"room_id" db:"room_id"`

	UserID       uuid.UUID `json:"-" db:"user_id"`
	CaretakerID  uuid.UUID `json:"-" db:"caretaker_id"`
	IndividualID uuid.UUID `json:"-" db:"individual_id"`

	Status  string `json:"status" db:"status"`
	Message string `json:"message" db:"message"`

	//TODO::имплементация
	//ArchivedForUser bool
	//ArchivedForCaretaker bool

	Weekly       bool                        `json:"weekly" db:"weekly"`
	Price        float32                     `json:"price" db:"price"`
	DateTimes    map[interface{}]WorkingTime `json:"-" db:"-"`
	DateTimeJSON string                      `json:"datetime" db:"datetime"`

	User       User       `json:"user" db:"-"`
	Individual Individual `json:"individual" db:"-"`
	Caretaker  User       `json:"caretaker" db:"-"`

	HasMessages bool `json:"has_messages" db:"-"`
}

type WorkingTime struct {
	Start float32 `json:"start"` //час, нещо (12:30 става 12.5)
	End   float32 `json:"end"`
}

// String is not required by pop and may be deleted
func (h HireRequest) String() string {
	jh, _ := json.Marshal(h)
	return string(jh)
}

// HireRequests is not required by pop and may be deleted
type HireRequests []HireRequest

// String is not required by pop and may be deleted
func (h HireRequests) String() string {
	jh, _ := json.Marshal(h)
	return string(jh)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (h *HireRequest) Validate(tx *pop.Connection) (*validate.Errors, error) {

	vals := []validate.Validator{
		&validators.UUIDIsPresent{Name: "caretaker_id", Field: h.CaretakerID},
		&validators.UUIDIsPresent{Name: "user_id", Field: h.UserID},
		&validators.UUIDIsPresent{Name: "individual_id", Field: h.IndividualID},
		&validators.RegexMatch{Name: "status", Field: h.Status, Expr: "(pending|accepted|declined)", Message: "Невалиден статус"},
		&validators.StringLengthInRange{Name: "message", Field: h.Message, Min: 10, Max: 520, Message: "Съобщението трябва да е между 10 и 520 символа"},
		&validators.IntIsGreaterThan{Name: "datetimes", Field: len(h.DateTimes)},
	}

	for k, t := range h.DateTimes {

		//Проверява, дали ключовете са правилния тип
		vals = append(vals, &validators.FuncValidator{Name: "datetimes", Fn: func() bool {
			var ok bool
			if h.Weekly {
				_, ok = k.(int)
			} else {
				_, ok = k.(time.Time)
			}
			return ok
		}})

		if reflect.TypeOf(k).Kind() == reflect.Int {
			vals = append(vals, &validators.IntIsGreaterThan{Name: "datetimes", Field: k.(int), Compared: -1})
			vals = append(vals, &validators.IntIsLessThan{Name: "datetimes", Field: k.(int), Compared: 7})
		}

		//Дали часовете са валидни
		vals = append(vals, &validators.IntIsLessThan{Name: "datetimes", Field: int(t.Start * 100), Compared: 2400})
		vals = append(vals, &validators.IntIsLessThan{Name: "datetimes", Field: int(t.End * 100), Compared: 2400})
		vals = append(vals, &validators.IntIsGreaterThan{Name: "datetimes", Field: int(t.Start * 100), Compared: 0})
		vals = append(vals, &validators.IntIsGreaterThan{Name: "datetimes", Field: int(t.End * 100), Compared: 0})

		//Дали крайния час е по-голям от началния
		vals = append(vals, &validators.IntIsGreaterThan{Name: "datetimes", Field: int(t.End * 100), Compared: int(t.Start * 100)})

	}

	return validate.Validate(vals...), nil

}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (h *HireRequest) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (h *HireRequest) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	//Прави сигорно непроменянето на  id парамерите
	oldHr := &HireRequest{}
	if err := tx.Find(oldHr, h.ID); err != nil {
		return validate.NewErrors(), errors.WithStack(err)
	}

	return validate.Validate(
		&validators.StringsMatch{Name: "caretaker_id", Field: h.CaretakerID.String(), Field2: oldHr.CaretakerID.String()},
		&validators.StringsMatch{Name: "user_id", Field: h.UserID.String(), Field2: oldHr.UserID.String()},
		&validators.StringsMatch{Name: "individual_id", Field: h.IndividualID.String(), Field2: oldHr.IndividualID.String()},
		&validators.FuncValidator{Name: "status", Field: oldHr.Status, Message: "Статусът не може да бъде променен от %s", Fn: func() bool {
			if oldHr.Status != "pending" && h.Status != oldHr.Status {
				return false
			}
			return true
		}},
	), nil
}

//Wrapper около create логиката
func (h *HireRequest) Create(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndCreate(h)
}

//Wrapper около update логиката
func (h *HireRequest) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(h)
}

//AfterSave задва roomID-то, ако не е вече зададено
func (h *HireRequest) AfterSave(tx *pop.Connection) error {
	if h.RoomID != uuid.Nil {
		return nil
	}
	h.RoomID = h.ID
	return tx.Save(h)
}

//AfterFind попълва User, Caretaker и Individual полетата
func (h *HireRequest) AfterFind(tx *pop.Connection) error {
	if h.ID == uuid.Nil {
		return nil
	}

	if err := tx.Find(&h.User, h.UserID); err != nil {
		return errors.WithStack(err)
	}

	if err := tx.Where("caretaker=?", h.CaretakerID).First(&h.Caretaker); err != nil {
		return errors.WithStack(err)
	}

	if err := tx.Find(&h.Individual, h.IndividualID); err != nil {
		return errors.WithStack(err)
	}

	h.DateTimes = map[interface{}]WorkingTime{}
	if h.Weekly {
		var dt map[int]WorkingTime
		if err := json.Unmarshal([]byte(h.DateTimeJSON), &dt); err != nil {
			return errors.WithStack(err)
		}
		for k, v := range dt {
			h.DateTimes[k] = v
		}
	} else {
		var dt map[time.Time]WorkingTime
		if err := json.Unmarshal([]byte(h.DateTimeJSON), &dt); err != nil {
			return errors.WithStack(err)
		}
		for k, v := range dt {
			h.DateTimes[k] = v
		}
	}

	h.HasMessages, _ = tx.Where("hire_request=?", h.RoomID).Exists(&Messages{})

	return nil
}

//Get връща всички request-и за даден потребител
func (h *HireRequests) Get(tx *pop.Connection, userID uuid.UUID, status string) error {
	q := tx.RawQuery("")

	if status != "all" {
		q = tx.Where("status=?", status)
	}

	q = q.Where("user_id=? OR caretaker_id=?", userID, userID).Order("created_at DESC")

	if err := q.All(h); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//ChangeStatus променя статуса на някой request (от pending на нещо)
func (h *HireRequest) ChangeStatus(tx *pop.Connection, status string) (*validate.Errors, error) {
	h.Status = status

	return tx.ValidateAndUpdate(h)

}

func (h *HireRequest) BeforeSave(tx *pop.Connection) error {

	round := func(wt WorkingTime) (wtr WorkingTime) {
		wtr.End = float32(math.Round(float64(wt.End*100)) / 100)
		wtr.Start = float32(math.Round(float64(wt.Start*100)) / 100)
		return
	}

	//json stringify-ва часовете
	var dateTime interface{}
	if h.Weekly {
		//round-ва часовете до 2 знака след запетаята
		dateTimeRounded := make(map[int]WorkingTime, len(h.DateTimes))
		for k, i := range h.DateTimes {
			dateTimeRounded[k.(int)] = round(i)
		}

		dateTime = dateTimeRounded
	} else {
		//round-ва часовете до 2 знака след запетаята
		dateTimeRounded := make(map[time.Time]WorkingTime, len(h.DateTimes))
		for k, i := range h.DateTimes {
			dateTimeRounded[k.(time.Time)] = round(i)
		}

		dateTime = dateTimeRounded
	}

	dtb, _ := json.Marshal(dateTime)
	h.DateTimeJSON = string(dtb)

	//Изчислява общата цена
	if h.Caretaker.ID == uuid.Nil {
		if err := tx.Where("caretaker=?", h.CaretakerID).First(&h.Caretaker); err != nil {
			return errors.WithStack(err)
		}
	}
	h.Price = 0
	for _, t := range h.DateTimes {
		h.Price += (t.End - t.Start) * h.Caretaker.Caretaker.HourRate
	}
	h.Price = float32(math.Round(float64(h.Price*100)) / 100)

	return nil
}
