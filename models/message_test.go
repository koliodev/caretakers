package models

import (
	"fmt"

	"github.com/gofrs/uuid"
)

func (ms *ModelSuite) Test_Message_New() {
	ms.LoadFixture("hire request")

	u := User{}
	h := HireRequest{}
	ms.NoError(
		ms.DB.Where("email=?", "hireRequest@user.email").First(&u))

	ms.NoError(
		ms.DB.Where("user_id=?", u.ID).First(&h))

	m := Message{}

	verrs, err := m.New(ms.DB, h, "Zdr ko pr", u.ID)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	//Невалидно hire request
	verrs, err = m.New(ms.DB, HireRequest{}, "Zdr ko pr", u.ID)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Невалидно съобщение
	verrs, err = m.New(ms.DB, h, "", u.ID)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Невалиден sender
	verrs, err = m.New(ms.DB, h, "Zdr ko pr", uuid.FromStringOrNil("493b385f-fef2-41e2-9951-453247fcd2bc"))
	ms.NoError(err)
	ms.True(verrs.HasAny())
}

func (ms *ModelSuite) Test_Message_SetDelivered() {
	ms.LoadFixture("a message")

	m := Message{}
	ms.DB.Where("message=?", "a message fixture message text").First(&m)

	verrs, err := m.SetDelivered(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("should not hove verrs, but got %v", verrs.Errors))

	//Проверка, дали наистина го е променило
	ms.NoError(ms.DB.Where("message=?", "a message fixture message text").First(&m))
	ms.Equal(true, m.Delivered)
}

func (ms *ModelSuite) Test_Messages_SetDelivered() {
	ms.LoadFixture("a message")
	ms.LoadFixture("a lot of messages")

	mes := Messages{}
	ms.DB.All(&mes)

	verrs, err := mes.SetDelivered(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("should not hove verrs, but got %v", verrs.Errors))

	//Проверка, дали наистина ги е променило
	ms.DB.All(&mes)
	for _, m := range mes {
		ms.Equal(true, m.Delivered)
	}
}

func (ms *ModelSuite) Test_Messages_SetRead() {
	ms.LoadFixture("a message")
	ms.LoadFixture("a lot of messages")

	mes := Messages{}
	ms.DB.All(&mes)

	verrs, err := mes.SetRead(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("should not hove verrs, but got %v", verrs.Errors))

	//Проверка, дали наистина ги е променило
	ms.DB.All(&mes)
	for _, m := range mes {
		ms.Equal(true, m.Read)
		ms.Equal(true, m.Delivered)
	}

}

func (ms *ModelSuite) Test_Message_SetRead() {
	ms.LoadFixture("a message")

	m := Message{}
	ms.DB.Where("message=?", "a message fixture message text").First(&m)

	verrs, err := m.SetRead(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	//Проверка, дали наистина го е променило
	ms.NoError(ms.DB.Where("message=?", "a message fixture message text").First(&m))
	ms.Equal(true, m.Read)
	ms.Equal(true, m.Delivered)

}

func (ms *ModelSuite) Test_Message_Get() {
	ms.LoadFixture("a message")
	ms.LoadFixture("a lot of messages")

	h := HireRequest{}
	ms.DB.Where("message=?", "Моля ви да се грижите за моето куче hire request messages1").First(&h)
	mes := Messages{}
	err := mes.Get(ms.DB, h.ID, 3, 1)
	ms.NoError(err)
	ms.Equal(3, len(mes))
	for _, m := range mes {
		ms.Equal(h.ID, m.HireRequestID)
	}

}

func (ms *ModelSuite) Test_Message_SetPusherID() {
	ms.LoadFixture("a lot of messages")

	m := Message{}
	ms.NoError(ms.DB.Where("message=?", "lots of messages 0 - a message  fixture message text").First(&m))

	//всичко 6
	verrs, err := m.SetPusherID(ms.DB, 1234)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("Should have no verrs got %v", verrs.Errors))

	//проверява, дали е променило базата
	ms.DB.Where("message=?", "lots of messages 0 - a message  fixture message text").First(&m)
	ms.Equal(uint(1234), m.PusherID)

	//пробва 2 пъти да промени id-то
	verrs, err = m.SetPusherID(ms.DB, 4321)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//проверява, дали е променило базата
	ms.DB.Where("message=?", "lots of messages 0 - a message  fixture message text").First(&m)
	ms.Equal(uint(1234), m.PusherID)

	//Опитва да сложи едно и също id на 2 съобщения
	ms.NoError(ms.DB.Where("message=?", "lots of messages 1 - a message4  fixture message text").Last(&m))
	verrs, err = m.SetPusherID(ms.DB, 1234)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Проверява, дали validate update го спира
	verrs, err = m.ValidateUpdate(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

}
