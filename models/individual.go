/**--------------------------------------------------------

				Individual.go

Модел за нещата свързани с лицата
(деца, възрастни хора, домашни любимци и др)

Методи:
	FindSuitableCaretakers() -> връща подходящите гледачи

---------------------------------------------------------**/

package models

import (
	"encoding/json"
	"fmt"
	"math"
	"strings"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/validate/validators"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
)

type Individual struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	UserID uuid.UUID `json:"-" db:"user_id"`

	Name  string    `json:"name" db:"name"`
	Birth time.Time `json:"birth_date" db:"date_of_birth"`
	Type  string    `json:"type" db:"type"`
	Needs string    `json:"special_needs" db:"special_needs"`
	About string    `json:"about" db:"about"`

	Location     Location     `json:"location" db:"-"`
	LocationJSON string       `json:"-" db:"location"`
	City         nulls.String `json:"city" db:"city"`

	Picture nulls.String `json:"picture" db:"picture"` //base64
}

// String is not required by pop and may be deleted
func (i Individual) String() string {
	ji, _ := json.Marshal(i)
	return string(ji)
}

// Individuals is not required by pop and may be deleted
type Individuals []Individual

// String is not required by pop and may be deleted
func (i Individuals) String() string {
	ji, _ := json.Marshal(i)
	return string(ji)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (i *Individual) Validate(tx *pop.Connection) (*validate.Errors, error) {

	return validate.Validate(

		&validators.UUIDIsPresent{Name: "user", Field: i.UserID},
		&validators.StringLengthInRange{Name: "city", Field: i.City.String, Min: 0, Max: 250, Message: "Името на населеното място трябва да надвишава 250 символа"},
		&validators.IntIsLessThan{Name: "location", Field: int(math.Round(i.Location.Lat)), Compared: 90, Message: "Невалидно местоположение"},
		&validators.IntIsLessThan{Name: "location", Field: int(math.Round(i.Location.Long)), Compared: 180, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(i.Location.Lat)), Compared: -90, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(i.Location.Long)), Compared: -180, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(math.Abs(i.Location.Lat))), Compared: 0, Message: "Невалидно местоположение"},
		&validators.IntIsGreaterThan{Name: "location", Field: int(math.Round(math.Abs(i.Location.Long))), Compared: 0, Message: "Невалидно местоположение"},

		&validators.TimeIsPresent{Name: "birth_date", Field: i.Birth, Message: "Датата на раждане е задължителна"},
		&validators.TimeIsBeforeTime{FirstName: "birth_date", FirstTime: time.Date(1900, 1, 1, 1, 1, 1, 1, time.Local), SecondTime: i.Birth, Message: "Твърде ранна рожденна дата"},

		&validators.StringIsPresent{Name: "name", Field: i.Name, Message: "Името е задължително"},
		StringIsAlpha{Name: "name", Field: i.Name, Message: "Името може да садържа само букви, без интервали или знаци различни от \"-\" "},
		&validators.StringLengthInRange{Name: "name", Field: i.Name, Min: 2, Max: 50, Message: "Името трябва да е между 2 и 50 символа"},

		&validators.StringIsPresent{Name: "about", Field: i.About, Message: "Описанието е задължително"},
		&validators.StringLengthInRange{Name: "about", Field: i.About, Min: 10, Max: 500, Message: "Описанието трябва да е между 10 и 500 символа"},

		&validators.StringLengthInRange{Name: "special_needs", Field: i.Needs, Min: 0, Max: 500, Message: "Специалните нужди не трябва да надвишават 500 символа"},
		&validators.StringIsPresent{Name: "type", Field: i.Type, Message: "Видът на лицето е задължителен"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (i *Individual) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (i *Individual) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	//Прави сигорно непроменянето на  id парамерите
	oldI := &Individual{}
	if err := tx.Find(oldI, i.ID); err != nil {
		return validate.NewErrors(), errors.WithStack(err)
	}

	return validate.Validate(
		&validators.StringsMatch{Name: "user_id", Field: i.UserID.String(), Field2: oldI.UserID.String()},
	), nil
}

//AfterFind вика се след "намиране" на записан от базата
func (i *Individual) AfterFind(tx *pop.Connection) error {
	if err := json.Unmarshal([]byte(i.LocationJSON), &i.Location); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

// Create wrapper около логиката за създаване
func (i *Individual) Create(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndCreate(i)
}

//Update wrapper около логиката за редакция
func (i *Individual) Update(tx *pop.Connection) (*validate.Errors, error) {
	return tx.ValidateAndUpdate(i)
}

//Delete wrapper около логиката за изтриване
func (i *Individual) Delete(tx *pop.Connection) error {
	//TODO::да разкарва всички hire_request-и
	//TODO::тестове
	return tx.Destroy(i)

}

func (i *Individual) BeforeSave(tx *pop.Connection) error {
	//json stringify-ва местоположението
	lb, err := json.Marshal(i.Location)
	if err != nil {
		return errors.WithStack(err)
	}
	i.LocationJSON = string(lb)
	return nil
}

//FindSuitableCaretakers намира подходящи гледачи за даденото лице
func (i *Individual) FindSuitableCaretakers(tx *pop.Connection, dist float32, params ...interface{}) (Users, error) {

	var skills []interface{}
	if len(params) >= 1 && params[0] != nil {
		skills = params[0].([]interface{})
	}

	us := Users{}

	q := tx.Select("users.*").Join("caretakers", "caretakers.id = users.caretaker").
		Where("public.get_distance(?, ?, (users.location ->>'latitude')::double precision,  (users.location->>'longitude')::double precision ) < ?", i.Location.Lat, i.Location.Long, dist).
		Where("array[caretakers.works_with] @> array[?::character varying (255)]", i.Type).
		Where("users.verified=?", true)

	if len(skills) > 0 {
		cvSkills := make([]string, len(skills))
		for k := range skills {
			cvSkills[k] = "?::character varying (255)"
		}
		q = q.Where(fmt.Sprintf("array[caretakers.skills] @> array[%s]", strings.Join(cvSkills, " , ")), skills...)
	}

	err := q.All(&us)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return us, nil

}
