package models

import (
	"fmt"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/uuid"
)

func (ms *ModelSuite) Test_Notification_Create() {
	var u User
	ms.LoadFixture("a lot of users")
	ms.NoError(ms.DB.Where("email=?", "fixture1@user.email").First(&u))

	n := Notification{
		UserID:  u.ID,
		Type:    "request.sent",
		Message: "Успешно пратена заявка до Гошо",
		MetaID:  nulls.NewUUID(uuid.FromStringOrNil("abe6c7db-079f-437c-bfd4-e7dbb75353fb")),
	}

	//Всичко 6
	ms.DBDelta(1, "notifications", func() {
		verrs, err := n.Create(ms.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs but got %v", verrs.Errors))
	})
	n.ID = uuid.Nil

	//Невалидно userID
	n.UserID = uuid.Nil
	verrs, err := n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	n.UserID = u.ID

	//невалиден тип
	n.Type = "go6o"
	verrs, err = n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	n.Type = ""
	verrs, err = n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	n.Type = "request.sent"

	//невалидно съобщение
	n.Message = "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_"
	verrs, err = n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	n.Message = "12"
	verrs, err = n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	n.Message = "123"

	//невалидно metaID
	n.MetaID = nulls.NewUUID(uuid.Nil)
	verrs, err = n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	n.MetaID = nulls.UUID{}
	verrs, err = n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	n.Type = "messages.unread"
	n.MetaID = nulls.NewUUID(uuid.FromStringOrNil("abe6c7db-079f-437c-bfd4-e7dbb75353fb"))
	verrs, err = n.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	n.MetaID = nulls.UUID{}

	//Всичко 6
	ms.DBDelta(1, "notifications", func() {
		verrs, err = n.Create(ms.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs but got %v", verrs.Errors))
	})

}

func (ms *ModelSuite) Test_Notification_ComposeMessage() {
	n := Notification{}

	//Непрочетени съобщения
	n.Type = "messages.unread"
	err := n.ComposeMessage(uint(5))
	ms.NoError(err)
	ms.Equal("Имате 5 непрочетени съобщения", n.Message)

	err = n.ComposeMessage("ivan")
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	err = n.ComposeMessage(uint(5), uint(7))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	//съобщение от ..
	n.Type = "message.received"
	err = n.ComposeMessage("го6о")
	ms.NoError(err)
	ms.Equal("Го6о ви изпрати съобщение", n.Message)

	err = n.ComposeMessage(uint(5))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	err = n.ComposeMessage("го6о", uint(5))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	//Изпратена заявка
	n.Type = "request.sent"
	err = n.ComposeMessage("пе6о", "иванчо")
	ms.NoError(err)
	ms.Equal("Успешно изпратена поръчка към Пе6о за лице Иванчо", n.Message)

	err = n.ComposeMessage(uint(5))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	err = n.ComposeMessage("го6о", uint(5))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	//Получена заявка
	n.Type = "request.received"
	err = n.ComposeMessage("пе6о", "иванчо")
	ms.NoError(err)
	ms.Equal("Пе6о ви изпрати поръчка за лице Иванчо", n.Message)

	err = n.ComposeMessage(uint(5))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	err = n.ComposeMessage("го6о", "ми6о", "ми6о")
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	//Одобрена заявка
	n.Type = "request.accepted"
	err = n.ComposeMessage("пе6о", "иванчо")
	ms.NoError(err)
	ms.Equal("Пе6о прие поръчката за лице Иванчо", n.Message)

	err = n.ComposeMessage(uint(5))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	err = n.ComposeMessage("го6о", "ми6о", "ми6о")
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	//Отхвърлена заявка
	n.Type = "request.declined"
	err = n.ComposeMessage("пе6о", "иванчо")
	ms.NoError(err)
	ms.Equal("Пе6о отказа поръчката за лице Иванчо", n.Message)

	err = n.ComposeMessage(uint(5))
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

	err = n.ComposeMessage(uint(5), "го6о", "ми6о")
	ms.Error(err)
	ms.Equal(err.Error(), "invalid params")

}

func (ms *ModelSuite) Test_Notification_GetForUser() {
	ms.LoadFixture("lots of notifications")

	u := User{}
	ms.NoError(ms.DB.Where("email=?", "notification@user1.email").First(&u))

	ns := Notifications{}
	ms.NoError(ns.GetForUser(ms.DB, u, 0, 0))
	ms.Equal(6, len(ns))

	ns = Notifications{}
	ms.NoError(ns.GetForUser(ms.DB, u, 3, 1))
	ms.Equal(3, len(ns))
	ms.Contains(fmt.Sprintf("%v", ns), "request.received")
	ms.Contains(fmt.Sprintf("%v", ns), "request.accepted")
	ms.Contains(fmt.Sprintf("%v", ns), "request.declined")

	ns = Notifications{}
	ms.NoError(ms.DB.Where("email=?", "notification@user2.email").First(&u))
	ms.NoError(ns.GetForUser(ms.DB, u, 0, 0))
	ms.Equal(3, len(ns))

}

func (ms *ModelSuite) Test_Notification_Remove() {
	ms.LoadFixture("lots of notifications")
	n := Notification{}
	ms.NoError(ms.DB.First(&n))
	ms.NotEqual(uuid.Nil, n.ID)

	ms.DBDelta(-1, "notifications", func() {
		ms.NoError(n.Remove(ms.DB))
	})
}

func (ms *ModelSuite) Test_Notification_SetRead() {
	ms.LoadFixture("lots of notifications")
	n := Notification{}
	ms.NoError(ms.DB.First(&n))
	ms.NotEqual(uuid.Nil, n.ID)

	//Всичко 6
	verrs, err := n.SetRead(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.Equal(true, n.Read)

	n.Read = false
	ms.NoError(ms.DB.Save(&n))
	ms.Equal(false, n.Read)

	//Пробва да промени някой от другите параметри
	n.Message = "Ivan"
	verrs, err = n.SetRead(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Equal(false, n.Read)

}
