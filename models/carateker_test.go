package models

import (
	"fmt"
	"log"
	"time"

	"github.com/gofrs/uuid"
)

func (ms *ModelSuite) Test_Caretaker_Create() {
	c := Caretaker{
		Bio:        "Алеко Иваницов Константинов, познат под псевдонима Щастливеца, е български писател, адвокат, общественик и основател на организираното туристическо движение в България.",
		Experience: "Работа с деца, коне и кучета",
		Edu:        "",
		Skills:     []string{"плетене", "заваряне", "струговане на дърво"},
		WorksWith:  []string{"деца", "кучета", "безработни хора"},
		HourRate:   10.50,
		Birth:      time.Date(2001, 1, 19, 0, 0, 0, 0, time.UTC),
		Picture:    "base64",
	}

	ms.DBDelta(2, "caretakers", func() {
		verrs, err := c.Create(DB)
		ms.NoError(err)
		ms.False(verrs.HasAny())
		c.ID = uuid.Nil

		//Снимка
		c.Picture = ""
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("picture"), "Снимката е задължителна")
		c.Picture = "base65"

		//Биография
		c.Bio = ""
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("bio"), "Биографията е задължителна")

		c.Bio = "био"
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("bio"), "Биографията трябва да е между 10 и 1000 символа")

		c.Bio = str(1001, "б")
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("bio"), "Биографията трябва да е между 10 и 1000 символа")
		c.Bio = "Биография която е за мен"

		//Опит
		c.Experience = str(501, "б")
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("experience"), "Предишният опит не трябва да надвишава 500 символа")
		c.Experience = ""

		//Образование
		c.Edu = str(501, "б")
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("education"), "Образованието не трябва да надвишава 500 символа")
		c.Edu = "основно"

		//Умения
		c.Skills = []string{"умение", "плетене"}
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("skills"), "Необходими са поне 3 умения")
		c.Skills = []string{"умение", "плетене", "варене на ракия]"}

		//работи с
		c.WorksWith = []string{}
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("works_with"), "Изберете поне 1 група")
		c.WorksWith = []string{"възрастни"}

		//Дата на разждане
		c.Birth = time.Time{}
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("birth_date"), "Датата на раждане е задължителна")

		c.Birth = time.Date(2016, 1, 19, 0, 0, 0, 0, time.UTC)
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("birth_date"), "Трябва да сте поне на 14 години")
		c.Birth = time.Date(2003, 1, 19, 0, 0, 0, 0, time.UTC)

		//Надница
		c.HourRate = -12.22
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("hour_rate"), "Невалидна надница")

		c.HourRate = 0
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("hour_rate"), "Невалидна надница")

		c.HourRate = 0.001
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("hour_rate"), "Невалидна надница")

		c.HourRate = 1000.99
		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.True(verrs.HasAny())
		ms.Contains(verrs.Get("hour_rate"), "Невалидна надница")

		c.HourRate = 12.1234567

		verrs, err = c.Create(DB)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("има валидационни грешки %v", verrs.Errors))
	})

}

func (ms *ModelSuite) Test_Caretaker_Update() {
	c := Caretaker{
		Bio:        "Алеко Иваницов Константинов, познат под псевдонима Щастливеца, е български писател, адвокат, общественик и основател на организираното туристическо движение в България.",
		Experience: "Работа с деца, коне и кучета",
		Edu:        "",
		Skills:     []string{"плетене", "заваряне", "струговане на дърво"},
		WorksWith:  []string{"деца", "кучета", "безработни хора"},
		HourRate:   10.50,
		Birth:      time.Date(2001, 1, 19, 0, 0, 0, 0, time.UTC),
		Picture:    "base64",
	}
	verrs, err := c.Create(DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	cNewNotOk := Caretaker{
		ID:         c.ID,
		Bio:        "",
		Experience: str(510, "t"),
		Edu:        str(560, "a"),
		Skills:     []string{"плетене", "заваряне"},
		WorksWith:  []string{},
		HourRate:   -10.50,
		Birth:      time.Date(2051, 1, 19, 0, 0, 0, 0, time.UTC),
		Picture:    "",
	}

	cNewOk := Caretaker{
		ID:         c.ID,
		Bio:        "Обича да работи с деца, да коси трева и има бензин в кръвта",
		Experience: "Балкчал съм къде ли не бате",
		Edu:        "Предучилищна само",
		Skills:     []string{"готвене", "духане(насвещички)", "струговане"},
		WorksWith:  []string{"баби", "дядовци ", "безработни"},
		HourRate:   5.32,
		Birth:      time.Date(2005, 1, 19, 0, 0, 0, 0, time.UTC),
		Picture:    "base69",
	}

	//опит с валидационни грешки
	verrs, err = cNewNotOk.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(verrs.Get("picture"), "Снимката е задължителна")
	ms.Contains(verrs.Get("skills"), "Необходими са поне 3 умения")
	ms.Contains(verrs.Get("birth_date"), "Трябва да сте поне на 14 години")
	ms.Contains(verrs.Get("works_with"), "Изберете поне 1 група")
	ms.Contains(verrs.Get("bio"), "Биографията трябва да е между 10 и 1000 символа")
	ms.Contains(verrs.Get("experience"), "Предишният опит не трябва да надвишава 500 символа")
	ms.Contains(verrs.Get("education"), "Образованието не трябва да надвишава 500 символа")
	ms.Contains(verrs.Get("hour_rate"), "Невалидна надница")

	//опит ако всичко е 6
	verrs, err = cNewOk.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	updatedC := &Caretaker{}
	ms.NoError(ms.DB.Find(updatedC, c.ID))
	ms.Equal(updatedC.ID, cNewOk.ID)
	ms.Equal(updatedC.Bio, cNewOk.Bio)
	ms.Equal(updatedC.Experience, cNewOk.Experience)
	ms.Equal(updatedC.WorksWith, cNewOk.WorksWith)
	ms.Equal(updatedC.Skills, cNewOk.Skills)
	ms.Equal(updatedC.HourRate, cNewOk.HourRate)
	ms.Equal(updatedC.HourRate, cNewOk.HourRate)
	ms.Equal(updatedC.Picture, cNewOk.Picture)

}

func (ms *ModelSuite) Test_Caretaker_FindIndividuals() {
	ms.LoadFixture("a lot of caretakers")
	ms.LoadFixture("a lot of individuals")

	c := Caretaker{}
	ms.NoError(ms.DB.Where("bio=?", "Биографията на гледач-потребител 1").First(&c)) //Тоя е от Добрич

	//район Добрич
	inds, err := c.FindIndividuals(ms.DB, 1)
	ms.NoError(err)
	ms.Equal(1, len(inds))

	//район Добрич+Дончево
	inds, err = c.FindIndividuals(ms.DB, 14)
	ms.NoError(err)
	ms.Equal(2, len(inds))

	//Добавя вид на нещата, които може да гледа + "куче"
	c.WorksWith = append(c.WorksWith, "куче")
	inds, err = c.FindIndividuals(ms.DB, 14)
	ms.NoError(err)
	ms.Equal(3, len(inds))

	//Прави нещата, които може да гледа ["куче", "дете"]
	c.WorksWith = []string{"куче", "дете"}
	inds, err = c.FindIndividuals(ms.DB, 14)
	ms.NoError(err)
	ms.Equal(1, len(inds))

}

func (ms *ModelSuite) Test_Caretaker_GetCalendarInfo() {
	ms.LoadFixture("a lot of hire requests")
	c := Caretaker{}
	ms.NoError(ms.DB.First(&c))

	ms.NoError(ms.DB.RawQuery("UPDATE hire_requests SET caretaker_id=? , status=?", c.ID, "accepted").Exec())

	log.Println(c.ID)

	dtms, err := c.GetCalendarInfo(ms.DB)
	ms.NoError(err)
	ms.Equal(2, len(dtms[1]))
	ms.Equal(2, len(dtms[3]))
	ms.Equal(1, len(dtms[4]))
	ms.Equal(1, len(dtms[5]))
}

func (ms *ModelSuite) Test_Caretaker_CheckAvailable() {
	ms.LoadFixture("a lot of hire requests")
	c := Caretaker{}
	ms.NoError(ms.DB.First(&c))
	ms.NoError(ms.DB.RawQuery("UPDATE hire_requests SET caretaker_id=? , status=?", c.ID, "accepted").Exec())

	//######### дата #######

	//датата е четвъртък (има часове за тогава)
	available, err := c.CheckAvailable(ms.DB, time.Date(2020, 2, 27, 0, 0, 0, 0, time.UTC), WorkingTime{
		2.3, 22.45,
	})
	ms.NoError(err)
	ms.False(available)

	//датата е четвъртък (ама няма часове за тези часове)
	available, err = c.CheckAvailable(ms.DB, time.Date(2020, 2, 27, 0, 0, 0, 0, time.UTC), WorkingTime{
		2.3, 3.4,
	})
	ms.NoError(err)
	ms.True(available)

	//датата е петък (няма часове за тогава)
	available, err = c.CheckAvailable(ms.DB, time.Date(2020, 2, 28, 0, 0, 0, 0, time.UTC), WorkingTime{
		2.3, 22.45,
	})
	ms.NoError(err)
	ms.True(available)

	//Ако датата е за днес (има часове тогава )
	available, err = c.CheckAvailable(ms.DB, time.Now(), WorkingTime{
		2.3, 22.45,
	})
	ms.NoError(err)
	ms.False(available)

	//###### Ден от седмицата  #######
	//понеделник (няма часове за тогава)
	available, err = c.CheckAvailable(ms.DB, 0, WorkingTime{
		2.3, 22.45,
	})
	ms.NoError(err)
	ms.True(available)

	//вторник (няма часове за тогава)
	available, err = c.CheckAvailable(ms.DB, 1, WorkingTime{
		5, 6,
	})
	ms.NoError(err)
	ms.False(available)

	//вторник (ама няма часове за тези часове)
	available, err = c.CheckAvailable(ms.DB, 1, WorkingTime{
		1, 2,
	})
	ms.NoError(err)
	ms.True(available)
}

//Прави string с определена дължина
func str(l int, append string) (s string) {
	for i := 0; i < l; i++ {
		s = s + append
	}
	return
}
