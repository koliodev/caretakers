package models

/**--------------------------------------------------------

				Notification.go

Модел за нещата свързани с известията

Методи:
	SetRead() -> задава статус "прочетено"
	Remove()  -> изтрива известието от базата

---------------------------------------------------------**/

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/validate/validators"
	"github.com/pkg/errors"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

type Notification struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"-" db:"updated_at"`

	UserID  uuid.UUID `json:"-" db:"user_id"`
	Type    string    `json:"type" db:"type"` // дали е зз непрочетени съобщения, получен/изпратен hire_request  одобрен/decline-нат hire_request
	Message string    `json:"message" db:"message"`
	Read    bool      `json:"read" db:"read"`

	MetaID nulls.UUID `json:"meta_id" db:"meta_id"` //Ако notification-a изисква някакво id като мета данни (на hire_request или message)
}

//Типовете, които НЕ изискват някакво id като мета данни в самия notification
var notificationNoIDTypes = []string{
	"messages.unread",
}

//Типовете, които  изискват някакво id като мета данни в самия notification (на hire_request или пък message)
var notificationIDTypes = []string{
	"request.sent", "request.received",
	"request.accepted", "request.declined",
	"message.received",
}

// String is not required by pop and may be deleted
func (n Notification) String() string {
	jn, _ := json.Marshal(n)
	return string(jn)
}

// Notifications is not required by pop and may be deleted
type Notifications []Notification

// String is not required by pop and may be deleted
func (n Notifications) String() string {
	jn, _ := json.Marshal(n)
	return string(jn)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (n *Notification) Validate(tx *pop.Connection) (*validate.Errors, error) {

	//Всички възможни типове
	types := append(notificationNoIDTypes, notificationIDTypes...)

	//Валидаторите
	vals := []validate.Validator{
		&validators.RegexMatch{Name: "type", Field: n.Type, Expr: fmt.Sprintf("(%s)", strings.Join(types, "|")), Message: "Невалиден тип"},
		&validators.StringLengthInRange{Name: "message", Field: n.Message, Min: 3, Max: 255, Message: "Съобщението трябва да е между 2 и 255 симовоа"},
		&validators.UUIDIsPresent{Name: "user_id", Field: n.UserID},
	}

	//Aко типа (не)изисква id
	stringIn := func(s string, ss []string) bool {
		for _, x := range ss {
			if x == s {
				return true
			}
		}
		return false
	}
	if stringIn(n.Type, notificationIDTypes) {
		//Изисква meta_id
		vals = append(vals,
			&validators.UUIDIsPresent{Name: "meta_id", Field: n.MetaID.UUID},
		)
	} else {
		//Изисква да няма meta_id
		vals = append(vals,
			&validators.FuncValidator{Name: "meta_id", Field: "", Fn: func() bool {
				if n.MetaID.Valid && n.MetaID.UUID != uuid.Nil {
					return false
				}
				return true
			}, Message: "Meta ID-то не е позволено %s"},
		)
	}

	//Ако типа изисква ID

	return validate.Validate(
		vals...,
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (n *Notification) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (n *Notification) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	oldN := Notification{}
	if err := tx.Find(&oldN, n.ID); err != nil {
		return validate.NewErrors(), errors.WithStack(err)
	}

	return validate.Validate(
		&validators.StringsMatch{Name: "type", Field: n.Type, Field2: oldN.Type, Message: "Тиша не може да бъде променян"},
		&validators.StringsMatch{Name: "message", Field: n.Message, Field2: oldN.Message, Message: "Съобщението не може да бъде променяно"},
		&validators.StringsMatch{Name: "meta_id", Field: n.MetaID.UUID.String(), Field2: oldN.MetaID.UUID.String(), Message: "Мета id-то не може да бъде променяно"},
	), nil
}

//Wrapper около create логиката
func (n *Notification) Create(tx *pop.Connection) (*validate.Errors, error) {
	n.Read = false
	return tx.ValidateAndCreate(n)
}

//ComposeMessage  съствая смислено съобщение в замисимост от вида на известието
func (n *Notification) ComposeMessage(param ...interface{}) error {
	//Проверка, дали параметрите са подходящи за типа съобщение
	ok := false
	//ако иска точно 1 параметър, string
	if n.Type == "message.received" && len(param) == 1 && reflect.TypeOf(param[0]).String() == "string" {
		param[0] = strings.Title(param[0].(string))
		ok = true

	} else if n.Type == "messages.unread" && len(param) == 1 && reflect.TypeOf(param[0]).String() == "uint" { //ако иска точно 1 параметър, uint
		ok = true
	} else if len(param) == 2 && reflect.TypeOf(param[0]).String() == "string" && reflect.TypeOf(param[1]).String() == "string" { //ако иска точно 2 параметъра, string-ове
		param[0] = strings.Title(param[0].(string))
		param[1] = strings.Title(param[1].(string))
		ok = true
	}

	if !ok {
		return errors.New("invalid params")
	}

	switch n.Type {
	case "messages.unread":
		n.Message = fmt.Sprintf("Имате %d непрочетени съобщения", param[0])
		return nil
	case "request.sent":
		n.Message = fmt.Sprintf("Успешно изпратена поръчка към %s за лице %s", param...)
		return nil
	case "request.received":
		n.Message = fmt.Sprintf("%s ви изпрати поръчка за лице %s", param...)
		return nil
	case "request.accepted":
		n.Message = fmt.Sprintf("%s прие поръчката за лице %s", param...)
		return nil
	case "request.declined":
		n.Message = fmt.Sprintf("%s отказа поръчката за лице %s", param...)
		return nil
	case "message.received":
		n.Message = fmt.Sprintf("%s ви изпрати съобщение", param[0])
		return nil
	}

	return errors.New("unexpected error")
}

//SetRead задава статус "прочетена" на нотификацията
func (n *Notification) SetRead(tx *pop.Connection) (*validate.Errors, error) {
	prevRead := n.Read //previous read status
	n.Read = true
	verrs, err := tx.ValidateAndUpdate(n)
	if err != nil || verrs.HasAny() {
		n.Read = prevRead
		return verrs, err
	}
	return validate.NewErrors(), nil
}

//Remove изтрива известие
func (n *Notification) Remove(tx *pop.Connection) error {
	return tx.Destroy(n)
}

//GetForUser връща всички известия на даден потребител
//поддържа pagination
func (n *Notifications) GetForUser(tx *pop.Connection, u User, lim, offset uint) error {
	q := "SELECT * FROM notifications WHERE user_id=?  ORDER BY created_at DESC"
	params := []interface{}{u.ID}
	if offset > 0 {
		q += " OFFSET ?"
		params = append(params, offset)
	}
	if lim > 0 {
		q += " LIMIT ?"
		params = append(params, lim)
	}

	return tx.RawQuery(q, params...).All(n)

}
