package models

import "github.com/gobuffalo/uuid"

func (ms *ModelSuite) Test_EmailVerification_Create() {
	e := EmailVerification{}
	verrs, err := e.Create(ms.DB, User{})

	ms.NoError(err)
	ms.Contains(verrs.Get("user"), "user can not be blank.")

	ms.DBDelta(1, "email_verifications", func() {
		verrs, err = e.Create(ms.DB, User{ID: uuid.FromStringOrNil("ed9e19ce-1108-4b74-9603-5d643fbc4828")})
		ms.NoError(err)
		ms.False(verrs.HasAny())

		ms.NotEqual("", e.Token)
		ms.Equal("ed9e19ce-1108-4b74-9603-5d643fbc4828", e.UserID.String())

	})
}

//func (ms *ModelSuite) Test_EmailVerification_GetForUser(){
//	ms.LoadFixture("a lot of email verifications")
//
//	e := EmailVerification{}
//	u := &User{}
//	ms.NoError(ms.DB.Where("email=?", "fixtureEV1@user.email").First(u))
//
//	//Aко има такъв потребител
//	err := e.GetForUser(ms.DB, *u)
//	ms.NoError(err)
//	ms.Equal(e.Token, "token123Nomer1")
//	ms.Equal(e.UserID, u.ID)
//
//	//Ако няма
//	err := e.GetForUser(ms.DB, *u)
//	ms.Error(err)
//
//}
