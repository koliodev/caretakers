package models

/**----------------------------------------------------------------------------

				EmailVerification.go

Контролер за нещата свързани с верефикацията на потребителските email-и

Методи-и :
	Create() -> създава връзка в таблицата  userID->token
				Като създава token със зададена дължина

------------------------------------------------------------------------------**/

import (
	"encoding/json"
	"math/rand"
	"time"

	"github.com/gobuffalo/validate/validators"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

type EmailVerification struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	Token  string    `json:"-" db:"token"`
	UserID uuid.UUID `json:"-" db:"user_id"`
}

// String is not required by pop and may be deleted
func (e EmailVerification) String() string {
	je, _ := json.Marshal(e)
	return string(je)
}

// EmailVerifications is not required by pop and may be deleted
type EmailVerifications []EmailVerification

// String is not required by pop and may be deleted
func (e EmailVerifications) String() string {
	je, _ := json.Marshal(e)
	return string(je)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (e *EmailVerification) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.UUIDIsPresent{Name: "user", Field: e.UserID},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (e *EmailVerification) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (e *EmailVerification) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

//Create wrapper около логоката за създаване
func (e *EmailVerification) Create(tx *pop.Connection, user User) (*validate.Errors, error) {

	randString := func(n int) string {
		const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

		b := make([]byte, n)
		for i := range b {
			b[i] = letterBytes[rand.Intn(len(letterBytes))]
		}
		return string(b)
	}

	e.UserID = user.ID
	e.Token = randString(32)
	return tx.ValidateAndCreate(e)
}
