package models

/**--------------------------------------------------------

				Message.go

Модел за нещата свързани със съобщенията

Методи:
	SetRead() 	   -> задава статус "прочетено"
	SetDelivered() -> задава статус "получено"
	SetPusherID()  -> задава PusherID (integer, който отговаря
  		   на id-то на пратеното съобщение в базата на pusher)

---------------------------------------------------------**/

import (
	"encoding/json"

	"github.com/gobuffalo/validate/validators"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"log"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
)

type Message struct {
	ID        uuid.UUID `json:"-" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`

	PusherID uint `json:"id" db:"pusher_id"`

	HireRequest   `json:"-" db:"-"`
	HireRequestID uuid.UUID `json:"hire_request" db:"hire_request"`
	Sender        uuid.UUID `json:"sender" db:"sender"`
	Receiver      uuid.UUID `json:"receiver" db:"receiver"`

	Message   string `json:"message" db:"message"`
	Delivered bool   `json:"delivered" db:"delivered"`
	Read      bool   `json:"read" db:"read"`
}

// String is not required by pop and may be deleted
func (m Message) String() string {
	jm, _ := json.Marshal(m)
	return string(jm)
}

// Messages is not required by pop and may be deleted
type Messages []Message

// String is not required by pop and may be deleted
func (m Messages) String() string {
	jm, _ := json.Marshal(m)
	return string(jm)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (m *Message) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.UUIDIsPresent{Name: "hire_request", Field: m.HireRequestID},
		&validators.UUIDIsPresent{Name: "sender", Field: m.Sender},
		&validators.StringIsPresent{Name: "message", Field: m.Message},
		&validators.StringLengthInRange{Name: "message", Field: m.Message, Min: 1, Max: 1023},
		&validators.FuncValidator{Name: "sender", Message: "Невалиден изпращач %s", Fn: func() bool {
			if m.HireRequest.ID == uuid.Nil {
				tx.Find(&m.HireRequest, m.HireRequestID)
			}

			if m.Sender != m.HireRequest.Caretaker.ID && m.Sender != m.HireRequest.UserID {
				return false
			}
			return true
		}},
		&validators.FuncValidator{Name: "receiver", Message: "Невалиден приемач %s", Fn: func() bool {
			if m.HireRequest.ID == uuid.Nil {
				tx.Find(&m.HireRequest, m.HireRequestID)
			}

			if m.Receiver != m.HireRequest.Caretaker.ID && m.Receiver != m.HireRequest.UserID {
				return false
			}
			if m.Receiver == m.Sender {
				return false
			}
			return true
		}},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (m *Message) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (m *Message) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {

	return validate.Validate(&validators.FuncValidator{Name: "pusher_id", Message: "Не може да промените pusher_id-то %s", Fn: func() bool {
		oldM := &Message{}

		if err := tx.Find(oldM, m.ID); err != nil {
			return false
		}

		//Проверява, дали се променя вече зададено pusher_id
		if oldM.PusherID != m.PusherID && oldM.PusherID != 0 {
			return false
		}
		//Проверява, дали съществува друго съобщение със същото pusher_id
		if oldM.PusherID == 0 && m.PusherID != 0 {
			e, err := tx.Where("pusher_id=?", m.PusherID).Exists(&Message{})
			if e || err != nil {
				return false
			}
		}

		return true
	}}), nil
}

//New - sends new message
func (m *Message) New(tx *pop.Connection, h HireRequest, msg string, sender uuid.UUID) (*validate.Errors, error) {
	m.HireRequestID = h.RoomID
	m.HireRequest = h
	m.Message = msg
	m.Delivered = false
	m.Read = false
	m.Sender = sender

	if h.UserID == sender {
		m.Receiver = h.Caretaker.ID
	} else {
		m.Receiver = h.UserID
	}

	return tx.ValidateAndCreate(m)
}

func (m *Message) SetPusherID(tx *pop.Connection, id uint) (*validate.Errors, error) {
	m.PusherID = id
	return tx.ValidateAndUpdate(m)
}

//SetDelivered - променя статуса на "delivered"
func (m *Message) SetDelivered(tx *pop.Connection) (*validate.Errors, error) {
	m.Delivered = true
	return tx.ValidateAndUpdate(m)
}

//SetDelivered - променя статуса на "delivered" за всички съобщение
func (ms *Messages) SetDelivered(tx *pop.Connection) (*validate.Errors, error) {
	newMS := Messages{}
	for _, m := range *ms {
		m.Delivered = true
		newMS = append(newMS, m)
	}
	*ms = newMS

	return tx.ValidateAndUpdate(ms)
}

//SetRead - променя статуса на "прочетено" и "доставено" за всички съобщение
func (ms *Messages) SetRead(tx *pop.Connection) (*validate.Errors, error) {
	newMS := Messages{}
	for _, m := range *ms {
		m.Read = true
		m.Delivered = true
		newMS = append(newMS, m)
	}
	*ms = newMS

	return tx.ValidateAndUpdate(ms)
}

//SetRead - променя статуса на "прочетено"
func (m *Message) SetRead(tx *pop.Connection) (*validate.Errors, error) {
	m.Read = true
	m.Delivered = true
	return tx.ValidateAndUpdate(m)
}

//Get връща всички съобщения от дадена кореспонденция
func (ms *Messages) Get(tx *pop.Connection, hID uuid.UUID, lim, offset int) error {
	q := "SELECT * FROM messages WHERE hire_request=?"
	params := []interface{}{hID}
	q += " ORDER BY created_at DESC"
	if offset > 0 {
		q += " OFFSET ?"
		params = append(params, offset)
	}
	if lim > 0 {
		q += " LIMIT ?"
		params = append(params, lim)
	}
	log.Println("LIMIT - ", lim)

	err := tx.RawQuery(q, params...).All(ms)

	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
