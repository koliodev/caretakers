package models

import (
	"fmt"
	"log"
	"strings"

	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// DB is a connection to your database to be used
// throughout your application.
var DB *pop.Connection

func init() {
	var err error
	env := envy.Get("GO_ENV", "development")
	DB, err = pop.Connect(env)
	if err != nil {
		log.Fatal(err)
	}
	pop.Debug = env == "development"
}

//Custom validators
type StringIsAlpha struct {
	Name    string
	Field   string
	Message string
}

// IsValid adds an error if the field is not less than the compared value.
func (v StringIsAlpha) IsValid(errors *validate.Errors) {
	const alpha = "abcdefghijklmnopqrstuvwxyzабвгдежзийклмнопрстуфхчшщъьюяц-"
	for _, char := range v.Field {
		if !strings.Contains(alpha, strings.ToLower(string(char))) {
			if len(v.Message) > 0 {
				errors.Add(validators.GenerateKey(v.Name), v.Message)
				return
			}

			errors.Add(validators.GenerateKey(v.Name), fmt.Sprintf("%s can only contain letters", v.Name))
		}
	}
	return
}
