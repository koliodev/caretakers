package models

import (
	"fmt"
	"time"

	"github.com/gofrs/uuid"
)

func (ms *ModelSuite) Test_HireRequest_Create() {
	ms.LoadFixture("a lot of users")
	ms.LoadFixture("a lot of individuals")
	ms.LoadFixture("a lot of caretakers")

	//Creates string with fixed length
	str := func(l int, append string) (s string) {
		for i := 0; i < l; i++ {
			s = s + append
		}
		return
	}

	var u User
	var i Individual
	var c Caretaker

	ms.NoError(ms.DB.Where("email=?", "fixture1@user.email").First(&u))
	ms.NoError(ms.DB.Where("name=?", "fixtureИван").First(&i))
	ms.NoError(ms.DB.Where("bio=?", "Биографията на гледач-потребител 2").First(&c))

	h := HireRequest{
		UserID:       u.ID,
		IndividualID: i.ID,
		CaretakerID:  c.ID,
		Status:       "pending",
		Message:      str(510, "n"),
		Weekly:       true,
		DateTimes: map[interface{}]WorkingTime{
			1: {12.3, 23.45},
			3: {12.3, 23.45},
		},
	}

	ms.DBDelta(1, "hire_requests", func() {
		verrs, err := h.Create(ms.DB)
		ms.NoError(err)
		ms.False(verrs.HasAny(), fmt.Sprintf("shoud not hove verrs but got %v", verrs.Errors))
	})
	h.ID = uuid.Nil

	//Nil UUIDs
	h.UserID = uuid.Nil
	verrs, err := h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.UserID = u.ID

	h.IndividualID = uuid.Nil
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.IndividualID = i.ID

	h.CaretakerID = uuid.Nil
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.CaretakerID = c.ID

	//Invalid status
	h.Status = ""
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	h.Status = "ivan"
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.Status = "declined"

	//Invalid message
	h.Message = ""
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	h.Message = "1"
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	h.Message = str(540, "n")
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.Message = "Моля ви да се грижите за моето бебче"

	//Валидация на часовете
	h.Weekly = true

	//празни часове
	h.DateTimes = nil
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//невалиден week day
	h.DateTimes = map[interface{}]WorkingTime{
		1: {12.45, 23.0},
		9: {12.45, 23.0},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//невалиден час (отрицателен)
	h.DateTimes = map[interface{}]WorkingTime{
		1: {-0.01, 23.0},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//невалиден час (над 24)
	h.DateTimes = map[interface{}]WorkingTime{
		1: {12.00, 24.01},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//невалиден час (краят е преди началото)
	h.DateTimes = map[interface{}]WorkingTime{
		1: {12.00, 10.34},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	h.Weekly = false
	h.DateTimes = nil
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//невалидни часове
	h.DateTimes = map[interface{}]WorkingTime{
		time.Now(): {13.00, 10.34},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Смесени ключове във map-a
	h.DateTimes = map[interface{}]WorkingTime{
		3: {10.00, 10.34},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	//Смесени ключове във map-a
	h.DateTimes = map[interface{}]WorkingTime{
		"ivan": {10.00, 10.34},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	h.Weekly = true
	//Смесени ключове във map-a
	h.DateTimes = map[interface{}]WorkingTime{
		time.Now(): {10.00, 10.34},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.Weekly = false

	h.DateTimes = map[interface{}]WorkingTime{
		time.Now():                {Start: 2.12, End: 10.37},
		time.Now().Add(time.Hour): {Start: 5.12, End: 10.37},
	}
	verrs, err = h.Create(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("Should not have verrs but have %v", verrs.Errors))

	//Проверява, дали цената е изчислена правилно
	ms.Equal(float32(166.59), h.Price)
}

func (ms *ModelSuite) Test_HireRequest_Update() {
	ms.LoadFixture("hire request")

	u := User{}
	h := HireRequest{}
	ms.NoError(
		ms.DB.Where("email=?", "hireRequest@user.email").First(&u))

	ms.NoError(
		ms.DB.Where("user_id=?", u.ID).First(&h))

	//Променя статуса
	h.Status = "declined"
	verrs, err := h.Update(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	//пробва да промени някое от ID параметрите
	oldH := HireRequest{}
	ms.NoError(
		ms.DB.Where("user_id=?", u.ID).First(&oldH))

	//UserID
	h.UserID = uuid.FromStringOrNil("a129d2bc-9dc0-4d8e-ad11-49cd6e9ffb70")
	verrs, err = h.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.UserID = oldH.UserID

	//CaretakerID
	h.CaretakerID = uuid.FromStringOrNil("a129d2bc-9dc0-4d8e-ad11-49cd6e9ffb70")
	verrs, err = h.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.CaretakerID = oldH.CaretakerID

	//IndividualID
	h.IndividualID = uuid.FromStringOrNil("a129d2bc-9dc0-4d8e-ad11-49cd6e9ffb70")
	verrs, err = h.Update(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	h.IndividualID = oldH.IndividualID
}

func (ms *ModelSuite) Test_HireRequests_Get() {
	ms.LoadFixture("a lot of hire requests")

	var u User
	var c User // caretaker
	var hrs HireRequests

	ms.NoError(ms.DB.Where("email=?", "hireRequest2@user.email").First(&u))
	ms.NoError(ms.DB.Where("email=?", "fixtureHireRequestCaretkers2@user.email").First(&c))

	//За потребител
	ms.NoError(hrs.Get(ms.DB, u.ID, "all"))
	ms.Equal(2, len(hrs))
	hrs = HireRequests{}

	//Задаване на state на заявката
	ms.NoError(hrs.Get(ms.DB, u.ID, "accepted"))
	ms.Equal(1, len(hrs), fmt.Sprintf("shoud get 1 hire request but got %d -- %v", len(hrs), hrs))
	hrs = HireRequests{}

	//За гледач
	ms.NoError(hrs.Get(ms.DB, c.CaretakerID.UUID, "all"))
	ms.Equal(1, len(hrs))
	hrs = HireRequests{}

}

func (ms *ModelSuite) Test_HireRequest_AfterFind() {
	ms.LoadFixture("hire request")
	h := HireRequest{}

	ms.NoError(ms.DB.Where("message=?", "Моля ви да се грижите за моето куче hire request").First(&h))

	ms.Equal("hireRequest@user.email", h.User.Email.String)
	ms.Equal("fixtureHireRequestCaretkers@user.email", h.Caretaker.Email.String)
	ms.Equal(float32(87.39), float32(h.Caretaker.Caretaker.HourRate))

}

func (ms *ModelSuite) Test_HireRequest_ChangeStatus() {
	ms.LoadFixture("hire request")

	u := User{}
	h := HireRequest{}
	ms.NoError(
		ms.DB.Where("email=?", "hireRequest@user.email").First(&u))

	ms.NoError(
		ms.DB.Where("user_id=?", u.ID).First(&h))

	//### Невалиден статус
	verrs, err := h.ChangeStatus(ms.DB, "ivan")
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(fmt.Sprint(verrs.Errors), "Невалиден статус")

	//### ОК
	verrs, err = h.ChangeStatus(ms.DB, "accepted")
	ms.NoError(err)
	ms.False(verrs.HasAny(), fmt.Sprintf("should not have verrs but got %v", verrs.Errors))
	//проверка, дали наистина е променило базата
	newH := HireRequest{}
	ms.DB.Find(&newH, h.ID)
	ms.Equal("accepted", newH.Status)

	//### Променяне от accepted на declined (не е от pending)
	verrs, err = h.ChangeStatus(ms.DB, "declined")
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Contains(fmt.Sprint(verrs.Errors), "Статусът не може да бъде променен от accepted")

}
