package actions

/**----------------------------------------------------------------------------

				Auth.go

Контролер за нещата свързани с Authentication-a (login-a)

Action-и :
	Auth() 		  ->  basic auth - с email и парола
	OAuth() 	  ->  вход/регистрация с oauth provider (google/facebook)
	ChatterAuth() ->  endpoint за pusher chat kit
    PusherAuth()  ->  endpoint за pusher

Middleware-и :
	AuthMiddleware() -> Ако няма валиден JWT в request-a връща 401 "Unauthorized"

------------------------------------------------------------------------------**/

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/gofrs/uuid"
	"github.com/markbates/goth"
	"github.com/markbates/goth/providers/facebook"
	"github.com/markbates/goth/providers/google"

	"github.com/gobuffalo/pop"
	"golang.org/x/crypto/bcrypt"

	"github.com/gobuffalo/envy"
	"github.com/markbates/goth/gothic"

	"github.com/dgrijalva/jwt-go"
	"github.com/gobuffalo/buffalo"
	"github.com/kolioDev/care/models"
	"github.com/pkg/errors"
)

// Create the JWT key used to create the signature
var jwtKey = []byte(envy.Get("JWT_TOKEN", "super_secret_jwt_token"))

//Claims Create a struct that will be encoded to a JWT.
// We add jwt.StandardClaims as an embedded type, to provide fields like expiry time
type Claims struct {
	UserID uuid.UUID
	jwt.StandardClaims
}

func init() {
	gothic.Store = App().SessionStore

	//user provider
	googleUP := google.New(os.Getenv("GOOGLE_KEY"), os.Getenv("GOOGLE_SECRET"), fmt.Sprintf("%s%s", App().Host, "/oauth/google_user/callback"))
	//caretaker provider
	googleCP := google.New(os.Getenv("GOOGLE_CARETAKER_KEY"), os.Getenv("GOOGLE_CARETAKER_SECRET"), fmt.Sprintf("%s%s", App().Host, "/oauth/google_caretaker/callback"))
	//user provider
	fbUP := facebook.New(os.Getenv("FACEBOOK_KEY"), os.Getenv("FACEBOOK_SECRET"), fmt.Sprintf("%s%s", App().Host, "/oauth/facebook_user/callback"))
	//caretaker provider
	fbCP := facebook.New(os.Getenv("FACEBOOK_KEY"), os.Getenv("FACEBOOK_SECRET"), fmt.Sprintf("%s%s", App().Host, "/oauth/facebook_caretaker/callback"))

	//Променя имената на провайдарите
	googleUP.SetName("google_user")
	googleCP.SetName("google_caretaker")
	fbUP.SetName("facebook_user")
	fbCP.SetName("facebook_caretaker")

	goth.UseProviders(googleCP, googleUP, fbCP, fbUP)

}

// Auth  пробва да логне потребител с email и парола
func Auth(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)

	//Взима парола и email (и провайдъра) от request-a
	u := &models.User{}
	if err := c.Bind(u); err != nil {
		return errors.WithStack(err)
	}

	q := tx.Where("email = ?", strings.ToLower(u.Email.String))

	var oauth = false
	//Ако потребителя влиза с oauth акаунт
	if u.Provider.Valid && u.ProviderID.Valid {
		oauth = true
		q = tx.Where("provider=?", u.Provider).Where("provider_id=?", u.ProviderID)
	}

	//Намира потреибтел
	err := q.First(u)

	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			//няма потребител с такъв  email
			return c.Render(403, r.JSON("Invalid credentials"))
		}
		return errors.WithStack(err)
	}

	//сравянва паролата и hash-а в базата
	err = bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(u.Password))
	if !oauth && err != nil {
		return c.Render(403, r.JSON("Invalid credentials"))
	}

	if !u.Verified {
		return c.Render(401, r.JSON("User not verified"))
	}

	//генерира JWT
	token, expiresAt, err := auth(*u)
	if err != nil {
		return c.Render(500, r.JSON("Internal server error"))
	}

	//Изпраща известие от типа "имате 5 непрочетени съобшения"
	if err := unreadNotify(tx, u); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(map[string]interface{}{
		"token":      token,
		"expires_at": expiresAt,
	}))
}

// Auth  пробва да логне / регистрира  потребител с google или facebook
func OAuth(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)

	gu, err := gothic.CompleteUserAuth(c.Response(), c.Request())
	if err != nil {
		return c.Error(401, err)
	}

	if gu.Email == "" {
		return c.Redirect(303, os.Getenv("FRONTEND_URL"))
	}

	authPageUrl := os.Getenv("FRONTEND_SIGNUP_URL_CARETAKER")
	if strings.Contains(c.Param("provider"), "user") {
		authPageUrl = os.Getenv("FRONTEND_SIGNUP_URL_USER")
	}

	//Проверка, дали съществува такъв потребител.
	// Ако съществуава да го логне, ако не да препрати към съответната страница за регистрация от фронтен

	u := models.User{}
	q := tx.Where("provider = ? and provider_id = ? or email = ?", gu.Provider, gu.UserID, gu.Email)

	//Aко съществува такъв потребител
	exists, err := q.Exists(&u)
	if err != nil {
		return errors.WithStack(err)
	}
	if exists {
		if err = q.First(&u); err != nil {
			return errors.WithStack(err)
		}

		//Дали съществува потребител с този email, ако съществува, да го link-не
		u.Provider = nulls.NewString(gu.Provider)
		u.ProviderID = nulls.NewString(gu.UserID)
		if err := tx.Save(&u); err != nil {
			return errors.WithStack(err)
		}
		authPageUrl = os.Getenv("FRONTEND_LOGIN_URL")
	}

	fendSignupURL := os.Getenv("FRONTEND_URL") + authPageUrl
	fendSignupURL = strings.Replace(fendSignupURL, ":email", gu.Email, 1)
	fendSignupURL = strings.Replace(fendSignupURL, ":provider", gu.Provider, 1)
	fendSignupURL = strings.Replace(fendSignupURL, ":provider_id", gu.UserID, 1)

	//Изпраща известие от типа "имате 5 непрочетени съобшения"
	if err := unreadNotify(tx, &u); err != nil {
		return errors.WithStack(err)
	}

	return c.Redirect(303, fendSignupURL)

}

//PusherAuth връща JWT за pusher chattkit auth-a
func ChatterAuth(c buffalo.Context) error {
	chatter := NewChatter()
	userID := c.Param("user_id")

	authRes, err := chatter.Authenticate(userID)

	if err != nil {
		return errors.WithStack(err)
	} else {
		return c.Render(http.StatusOK, r.JSON(authRes.TokenResponse()))
	}

}

//PusherAuth връща JWT за pusher channels auth-a
func PusherAuth(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)

	//Проверява, дали името на channel-a е същото като id-то на потребителя
	chName := c.Request().PostForm.Get("channel_name")
	if chName != "private-"+u.ID.String() {
		return c.Render(403, r.String("unacceptable channel name"))
	}

	p := NewPusher()
	params, _ := ioutil.ReadAll(c.Request().Body)
	response, err := p.client.AuthenticatePrivateChannel(params)

	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.String(string(response)))

}

//auth - creating auth JWT
func auth(user models.User) (string, int64, error) {

	//Определям, кога ще изтече token-a
	// 2 дни валидност
	expirationTime := time.Now().Add((2 * 24) * time.Hour)

	//Прави JWT claim, който включва дата на изтичането и UserID
	claims := &Claims{
		UserID: user.ID,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		return "", 0, errors.New("Cannot generate JWT")
	}
	return tokenString, expirationTime.Unix(), nil
}

//AuthMiddleware - изисква логнат потребител (наличие на валиден JWT )
func AuthMiddleware(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {

		parsedC, _ := postParse(c)
		tknStr := parsedC.Request().PostForm.Get("_token")

		if tknStr == "" {
			tknStr = c.Param("_token")
		}

		claims := &Claims{}

		tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
			return jwtKey, nil
		})
		if err != nil || !tkn.Valid {
			return c.Render(401, r.JSON("Unauthorized 0"))
		}

		var u models.User
		if err := models.DB.Find(&u, claims.UserID); err != nil {
			return c.Render(401, r.JSON("Unauthorized 1"))
		}

		//Ако потребителя не е с потвърден E-mail адрес
		if !u.Verified {
			return c.Render(401, r.JSON("Unauthorized 2"))
		}

		//Makes sure no password is send here and there
		u.Password = ""
		c.Set("current_user", &u)

		return next(c)
	}
}

//праща известие от тип messages.unread (ако има нужда)
func unreadNotify(tx *pop.Connection, u *models.User) error {
	unreadMsgCount, _ := tx.Where("receiver=?", u.ID).
		Where("read=?", false).
		Count(&models.Message{})
	//Ако има повече от 1 непрочетени
	if unreadMsgCount > 1 {
		err := notify(tx, "messages.unread", *u, uuid.Nil, uint(unreadMsgCount))
		if err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}
