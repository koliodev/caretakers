package actions

/**----------------------------------------------------------------------------

				Auth.go

Контролер за нещата свързани с Известията

Action-и :
	GetNotifications() 	 ->  връща известията на даден user (paginate-ва ги )
	SetRead() 	         ->  задава "проветено на" на дадено известие
	DeleteNotification() ->  изтрива известие с дадено id

------------------------------------------------------------------------------**/

import (
	"fmt"
	"log"
	"strconv"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/nulls"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/kolioDev/care/models"
	"github.com/pkg/errors"
	"github.com/pusher/pusher-http-go"
)

type Pusher struct {
	client *pusher.Client
}

func NewPusher() Pusher {

	client := &pusher.Client{
		AppID:   envy.Get("PUSHER_APPID", "1234"),
		Key:     envy.Get("PUSHER_KEY", "ket"),
		Secret:  envy.Get("PUSHER_SECRET", "pa$$"),
		Cluster: envy.Get("PUSHER_CLUSTER", "eu"),
		Secure:  false,
	}

	return Pusher{client}
}

// NotificationsGetNotifications default implementation.
func NotificationsGetNotifications(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := c.Value("tx").(*pop.Connection)
	ns := models.Notifications{}

	off, _ := strconv.Atoi(c.Param("skip")) //limit
	lim, _ := strconv.Atoi(c.Param("get"))  //offset

	err := ns.GetForUser(tx, *u, uint(lim), uint(off))
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(ns))
}

// NotificationsSetRead задава статус прочетено на някое известие
func NotificationsSetRead(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	n, err := getNotification(c)
	if err != nil {
		if err.Error() == "does not own notification" {
			return c.Render(403, r.String("does not own notification"))
		}
		return errors.WithStack(err)
	}

	_, err = n.SetRead(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.String("ok"))
}

//NotificationsDeleteNotification изтрива известие от базата
func NotificationsDeleteNotification(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	n, err := getNotification(c)
	if err != nil {
		if err.Error() == "does not own notification" {
			return c.Render(403, r.String("does not own notification"))
		}
		return errors.WithStack(err)
	}

	err = n.Remove(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.String("ok"))
}

func notify(tx *pop.Connection, event string, user models.User, metaID uuid.UUID, messageParams ...interface{}) error {
	log.Println("Пращам известие")
	n := models.Notification{}
	n.Type = event
	n.UserID = user.ID
	if metaID != uuid.Nil {
		n.MetaID = nulls.NewUUID(metaID)
	}
	err := n.ComposeMessage(messageParams...)
	if err != nil {
		return errors.WithStack(err)
	}

	verrs, err := n.Create(tx)
	if err != nil {
		return errors.New(fmt.Sprintf("cannot create notification has err- %v", err.Error()))
	}
	if verrs.HasAny() {
		return errors.New(fmt.Sprintf("cannot create notification has verrs- %v", verrs.Errors))
	}

	//pusher broadcas
	if envy.Get("GO_ENV", "development") != "test" && envy.Get("GO_DISABLE_PUSHER", "false") != "true" {
		p := NewPusher()
		err := p.client.Trigger(fmt.Sprintf("private-%s", user.ID.String()), "notification-sent", n)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

func getNotification(c buffalo.Context) (models.Notification, error) {
	u := c.Value("current_user").(*models.User)
	tx := c.Value("tx").(*pop.Connection)
	id := c.Param("notification_id")
	n := models.Notification{}
	if err := tx.Find(&n, id); err != nil {
		return n, errors.WithStack(err)
	}

	//ако потребителя не притежава известието
	if u.ID != n.UserID {
		return n, errors.New("does not own notification")
	}
	return n, nil
}
