package actions

import (
	"encoding/json"
	"fmt"

	"github.com/gobuffalo/packr/v2"

	paramlogger "github.com/gobuffalo/mw-paramlogger"
	"github.com/markbates/goth/gothic"

	//"github.com/markbates/goth/gothic"
	"net/url"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	forcessl "github.com/gobuffalo/mw-forcessl"
	"github.com/pkg/errors"
	"github.com/unrolled/secure"

	"github.com/gobuffalo/buffalo-pop/pop/popmw"

	"github.com/kolioDev/care/models"
	"github.com/rs/cors"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App

//Настройва App().Host
func init() {
	App().Host = envy.Get("APP_URL", App().Host)
	fmt.Printf("GARFILD initialized, App.Host= %v \n", App().Host)
}

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
//
// Routing, middleware, groups, etc... are declared TOP -> DOWN.
// This means if you add a middleware to `app` *after* declaring a
// group, that group will NOT have that new middleware. The same
// is true of resource declarations as well.
//
// It also means that routes are checked in the order they are declared.
// `ServeFiles` is a CATCH-ALL route, so it should always be
// placed last in the route declarations, as it will prevent routes
// declared after it to never be called.
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env: ENV,
			//SessionStore: sessions.Null{},
			PreWares: []buffalo.PreWare{
				cors.Default().Handler,
			},
			SessionName: "_care_session",
		})

		// Automatically redirect to SSL
		app.Use(forceSSL())

		// Log request parameters (filters apply).
		app.Use(paramlogger.ParameterLogger)

		// Set the request content type to JSON
		//app.Use(contenttype.Set("application/json"))

		// Wraps each request in a transaction.
		//  c.Value("tx").(*pop.Connection)
		// Remove to disable this.
		app.Use(popmw.Transaction(models.DB))

		app.GET("/{json:(?:json)}", HomeHandler)
		app.GET("/", HomeHandler)

		app.Middleware.Skip(AuthMiddleware, Auth, UsersRegister, HomeHandler, UsersVerifyEmail)

		//Auth
		app.POST("/auth/", Auth)
		app.GET("/verify/email/{token}", UsersVerifyEmail)

		//UserRegister - регистрира  птребител  ( обикновен потребител или caretaker)
		app.PUT("/{type:(?:caretaker|user)}/register", UsersRegister)
		app.POST("/{type:(?:caretaker|user)}/register", UsersRegister)

		auth := app.Group("/oauth/")
		auth.GET("/{provider}", buffalo.WrapHandlerFunc(gothic.BeginAuthHandler)) //provider-a e {userType}_{provider}
		auth.GET("/{provider}/callback", OAuth)

		app.Use(AuthMiddleware)

		app.POST("/auth/pusher", PusherAuth)
		app.POST("/auth/chatter", ChatterAuth)

		//Get user
		app.GET("/user", UsersGetAuthUser)
		//update user
		app.POST("/user/update", UsersUpdateUser)
		app.POST("/caretaker/update", UserIsCaretaker(CaretakersUpdateCaretaker))
		app.GET("/calendar", UserIsCaretaker(CaretakersGetCalendarInfo))

		//IndividualsAdd добавя нуждаещ се човек
		app.PUT("/individuals/add", UserIsNotCaretaker(IndividualsAddIndividual))
		app.POST("/individuals/add", UserIsNotCaretaker(IndividualsAddIndividual))

		app.GET("/individuals", UserIsNotCaretaker(IndividualsGetIndividuals))
		app.GET("/caretaker/find", UserHasIndividual(IndividualsFindCaretakers))
		app.GET("/individual/{id}", IndividualsGetIndividual)

		app.POST("/individual/edit/{individual_id}", UserHasIndividual(IndividualsUpdateIndividual))
		app.PUT("/individual/edit/{individual_id}", UserHasIndividual(IndividualsUpdateIndividual))

		app.POST("/individual/remove/{individual_id}", UserHasIndividual(IndividualsRemoveIndividual))
		app.DELETE("/individual/remove/{individual_id}", UserHasIndividual(IndividualsRemoveIndividual))

		app.POST("/hire/", UserHasIndividual(HireRequestsSend))

		app.GET("/hire/requests/{status:(?:all|pending|accepted|declined)}", UsersGetHireRequests)
		app.GET("/hire/request/{id}", HireRequestGetRequest)
		app.POST("/hire/request/{id}/change/status/{status:(?:all|pending|accepted|declined)}", HireRequestsChangeStatus)

		//TODO::hire request delete (потребителя може да изтрива вече пратен request) ?

		app.GET("/caretaker/find/individuals/{proximity:[0-9]*\\.?[0-9]+}", UserIsCaretaker(CaretakersFindIndividuals))

		app.GET("/caretaker/get/{caretaker_id}", CaretakersGetCaretaker)
		app.POST("/send/message/", MessagesSendMessage)
		app.POST("/message/set/cursor", MessagesSetReadCursor)
		app.GET("/messages/{hire_request}", UserInHireRequest(MessagesGetForRoom))
		app.POST("/call/{action:(?:send|accept|decline|cancel)}/{hire_request}", UserInHireRequest(MessagesMediaCall)) //TODO::apidoc

		app.GET("/notifications/", NotificationsGetNotifications)
		app.POST("/notification/{notification_id}/read", NotificationsSetRead)
		app.POST("/notification/{notification_id}/remove", NotificationsDeleteNotification)
		app.DELETE("/notification/{notification_id}/remove", NotificationsDeleteNotification)

		// serve-ва статични файлове (които се показват в email-ите)
		app.ServeFiles("/", packr.New("app:public:assets", "../public"))
	}

	return app
}

// forceSSL will return a middleware that will redirect an incoming request
// if it is not HTTPS. "http://example.com" => "https://example.com".
// This middleware does **not** enable SSL. for your application. To do that
// we recommend using a proxy: https://gobuffalo.io/en/docs/proxy
// for more information: https://github.com/unrolled/secure/
func forceSSL() buffalo.MiddlewareFunc {
	return forcessl.Middleware(secure.Options{
		SSLRedirect:     ENV == "production",
		SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
	})
}

//Fix-ва проблема с не-получаването на post данните
//Копира ги от c.Request().Body в c.Request().PostForm
func postParse(c buffalo.Context) (buffalo.Context, error) {
	//трябва да е post или put метод
	if c.Request().Method != "POST" && c.Request().Method != "PUT" {
		return c, nil
	}

	var vs map[string]interface{}
	err := json.Unmarshal([]byte(fmt.Sprint(c.Request().Body)), &vs)
	if err != nil {
		return c, errors.WithStack(err)
	}

	vals := url.Values{}
	for k, v := range vs {
		vals.Add(k, fmt.Sprint(v))
	}
	c.Request().PostForm = vals
	return c, nil
}
