package actions

/**----------------------------------------------------------------------------

				Messages.go

Контролер за нещата свързани със съобщенията

Action-и :
	SendMessage() 	->  pusher broadcast-ва и запазва в базата съобщение
	SetReadCursor() ->  задава курсор прочетено на за съобщение с дадено pusher id
						(и всички съобщения изпратени от другия преди въпросното съобщение)
	GetForRoom() 	->  връща съобщенията за стая с определено id (paginate-ва ги )

------------------------------------------------------------------------------**/

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/kolioDev/care/models"
	"github.com/pkg/errors"
	"github.com/pusher/chatkit-server-go"
	pusherAuth "github.com/pusher/pusher-platform-go/auth"
)

type Chatter struct {
	client *chatkit.Client
}

func NewChatter() Chatter {
	client, _ := chatkit.NewClient(
		envy.Get("PUSHER_CHATKIT_INSTANCE", "v1:us1:43ebc889-dded-4a0b-b3fc-24762ebe0065"),
		envy.Get("PUSHER_CHATKIT_KEY", "f7e63760-1a6e-4304-9595-47ceaa5fc159:/LAXZaOtsSO0AF9usGsv0GoRawDN4xj5d6V3RPMMmO0="),
	)
	return Chatter{client}
}

func (chatter *Chatter) Authenticate(user string) (*pusherAuth.Response, error) {
	chatter.client.CreateUser(context.Background(), chatkit.CreateUserOptions{
		ID:   user,
		Name: user,
	})

	return chatter.client.Authenticate(chatkit.AuthenticatePayload{
		GrantType: "client_credentials",
	}, chatkit.AuthenticateOptions{
		UserID: &user,
	})
}

//MessagesSendMessage запазва съобщение в базата и го праща по  pusher chat_kit
func MessagesSendMessage(c buffalo.Context) error {
	tx := models.DB //c.Value("tx").(*pop.Connection)
	u := c.Value("current_user").(*models.User)
	h := &models.HireRequest{}

	c, err := postParse(c)
	if err != nil {
		return errors.WithStack(err)
	}

	hID := c.Request().PostForm.Get("room")
	msg := c.Request().PostForm.Get("message")
	msg = strings.ReplaceAll(msg, INITIAL_MSG_INDICATOR, "")

	if err := tx.Find(h, hID); err != nil {
		return c.Render(406, r.JSON(map[string]string{
			"hire_request": "invalid hire_request id 60",
		}))
	}

	m, verrs, err := sendMessage(tx, msg, u.ID, *h)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(406, r.JSON(verrs.Errors))
	}

	//Проверява, дали има скорошни съобщения до същиячовек, ако няма му праща известие

	receiver := h.Caretaker
	if u.CaretakerID.Valid {
		receiver = h.User
	}
	hasRecent, _ := tx.Where("receiver=?", receiver.ID).
		Where("sender=?", u.ID).
		Where("id!=?", m.ID).
		Where("created_at > ?", time.Now().Add(-1*24*time.Hour)). //преди повече от 1 ден
		Exists(&models.Message{})

	if !hasRecent {
		log.Println("notifying user")
		//Праща нотификация на получателя
		err := notify(tx, "message.received", receiver, h.RoomID,
			fmt.Sprintf("%s %s", strings.Title(u.FirstName), strings.Title(u.SecondName)))
		if err != nil {
			return errors.WithStack(err)
		}
	}

	return c.Render(200, r.String("sent"))
}

//MessagesSetReadCursor задва  ReadCursor
func MessagesSetReadCursor(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	m := models.Message{}
	ms := models.Messages{}
	u := c.Value("current_user").(*models.User)
	verrs := &validate.Errors{}

	c, err := postParse(c)
	if err != nil {
		return errors.WithStack(err)
	}

	mID := c.Request().PostForm.Get("id")
	if err := tx.Where("pusher_id=?", mID).First(&m); err != nil {
		return c.Render(404, r.String("message not found"))
	}
	tx.Find(m.HireRequest, m.HireRequestID)

	//Проверка, дали потребителя е receiver-а на съобщението
	if m.Receiver != u.ID {
		return c.Render(403, r.String("user is not receiver"))
	}

	//Намира предходните съобшения от тази кореспонденция, където authUser-a  e получател
	err = tx.Where("pusher_id<=?", mID).
		Where("read=?", false).
		Where("hire_request=?", m.HireRequestID).
		Where("receiver=?", u.ID).All(&ms)
	if err != nil {
		return errors.WithStack(err)
	}

	//Задава им статус - прочетен и доставен
	verrs, err = ms.SetRead(models.DB)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(406, r.JSON(verrs.Errors))
	}

	//Pusher broadcast
	if envy.Get("GO_ENV", "development") != "test" {
		chatter := NewChatter()
		err := chatter.client.SetReadCursor(context.Background(), u.ID.String(), m.HireRequestID.String(), m.PusherID)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	return c.Render(200, r.String("ОК"))
}

//MessagesGetForRoom Връща съобщенията от дадена коресподенция (специфисчна чат стая)
func MessagesGetForRoom(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := c.Value("tx").(*pop.Connection)
	ms := models.Messages{}
	h := models.HireRequest{}

	if err := tx.Find(&h, c.Param("hire_request")); err != nil {
		return c.Render(404, r.String("room not found"))
	}

	if u.ID != h.UserID && u.CaretakerID.UUID != h.CaretakerID {
		return c.Render(403, r.JSON("unauthorized, not in room"))
	}

	off, _ := strconv.Atoi(c.Param("skip")) //limit
	lim, _ := strconv.Atoi(c.Param("get"))  //offset

	if err := ms.Get(tx, h.ID, lim, off); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(ms))
}

//MessagesMediaCall изпраща сигнал, че няко се обажда на някой  ИЛИ сигнал, че някой приема обаждане
func MessagesMediaCall(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := c.Value("tx").(*pop.Connection)
	h := models.HireRequest{}
	action := c.Param("action")
	toU := models.User{}

	tx.Find(&h, c.Param("hire_request")) //сигурно е че няма да хвърли грешка, защото минава през middleware
	toU = h.User

	if u.CaretakerID.UUID != h.CaretakerID {
		toU = h.Caretaker
		u.Caretaker = &models.Caretaker{}
	}

	event := fmt.Sprintf("call-%s", action)
	if envy.Get("GO_ENV", "development") != "test" && envy.Get("GO_DISABLE_PUSHER", "false") != "true" {
		log.Println("Broadcasting", event, "to", fmt.Sprintf("private-%s", toU.ID.String()))
		p := NewPusher()
		err := p.client.Trigger(fmt.Sprintf("private-%s", toU.ID.String()), event, map[string]interface{}{
			"hire_request": h.ID.String(),
			"from": map[string]interface{}{
				"id":      u.ID.String(),
				"name":    strings.Title(fmt.Sprintf("%s %s", u.FirstName, u.SecondName)),
				"is_caretaker": u.CaretakerID.UUID == h.CaretakerID,
			},
		})
		if err != nil {
			return errors.WithStack(err)
		}
	}
	return c.Render(200, r.String("ok"))
}

//Запазва съобщение в базата и го broadcat-ва
func sendMessage(tx *pop.Connection, msg string, sender uuid.UUID, h models.HireRequest) (*models.Message, *validate.Errors, error) {
	m := &models.Message{}
	verrs, err := m.New(tx, h, msg, sender)
	if err != nil {
		return m, validate.NewErrors(), errors.WithStack(err)
	}

	if verrs.HasAny() {
		return m, verrs, nil
	}

	//Broadcast- ва съобщението
	if envy.Get("GO_ENV", "development") != "test" {
		chatter := NewChatter()
		pID, err := chatter.client.SendSimpleMessage(context.Background(), chatkit.SendSimpleMessageOptions{
			RoomID:   h.RoomID.String(),
			Text:     msg,
			SenderID: sender.String(),
		})
		if err != nil {
			return m, validate.NewErrors(), errors.WithStack(err)
		}
		//Задава pusher-id на съобщението
		verrs, err := m.SetPusherID(tx, pID)
		if err != nil {
			return m, validate.NewErrors(), errors.WithStack(err)
		}
		if verrs.HasAny() {
			return m, verrs, nil
		}
	}
	return m, validate.NewErrors(), nil
}
