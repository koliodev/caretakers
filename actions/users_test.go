package actions

import (
	"fmt"
	"strings"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/kolioDev/care/models"
)

func (as *ActionSuite) Test_Users_Register() {
	u := models.User{
		Password:             "qwerty",
		PasswordConfirmation: "qwerty",
		Email:                nulls.NewString("test@gmail.com"),
		FirstName:            "Latin",
		SecondName:           "Кирилски",
		Location:             models.Location{12.22, 13.33},
	}

	as.DBDelta(2, "users", func() {
		res := as.JSON("/user/register").Put(u)
		as.Equal(200, res.Code)

		// --- EMAIL -----
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Вече съществува потребител с e-mail адрес test@gmail.com")

		u.Email = nulls.NewString("invalid.com")
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Невалиден е-mail адрес")

		u.Email = nulls.NewString("i")
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "E-mail адресът трябва да е между 4 и 150 символа")

		u.Email = nulls.NewString("123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–1111")
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "E-mail адресът трябва да е между 4 и 150 символа")

		u.Email = nulls.NewString("valid@user.email")

		//----- PASSWORD -----
		u.Password = ""
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Паролата е задължителна")

		u.Password = "i"
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Паролата трябва да е между 6 и 100 символа")

		u.Password = "123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–123456789–1111"
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Паролата трябва да е между 6 и 100 символа")

		u.PasswordConfirmation = "123"
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Паролите не съвпадат")

		u.Password = "password"
		u.PasswordConfirmation = "password"

		//----- Name ---
		u.FirstName = ""
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		u.FirstName = "Българско"

		//---- Second name
		u.SecondName = ""
		res = as.JSON("/user/register").Put(u)
		as.Equal(406, res.Code)
		u.SecondName = "Latin"

		//--- Всичко 6
		res = as.JSON("/user/register").Put(u)
		as.Equal(200, res.Code)
	})

}

func (as *ActionSuite) Test_Users_Caretaker_Register() {
	u := models.User{
		Password:             "qwerty",
		PasswordConfirmation: "qwerty",
		Email:                nulls.NewString("test11@gmail.com"),
		FirstName:            "Latin",
		SecondName:           "Кирилски",
		Location:             models.Location{12.22, 13.33},
		Caretaker: &models.Caretaker{
			Bio:        "Алеко Иваницов Константинов, познат под псевдонима Щастливеца, е български писател, адвокат, общественик и основател на организираното туристическо движение в България.",
			Experience: "Работа с деца, коне и кучета",
			Edu:        "",
			Skills:     []string{"плетене", "заваряне", "струговане"},
			WorksWith:  []string{"деца", "кучета"},
			HourRate:   10.50,
			Birth:      time.Date(2001, 1, 19, 0, 0, 0, 0, time.UTC),
			Picture:    "base64",
		},
	}

	as.DBDelta(2, "users", func() {
		as.DBDelta(1, "caretakers", func() {
			res := as.JSON("/caretaker/register").Put(u)
			as.Equal(200, res.Code)
		})

		res := as.JSON("/caretaker/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Вече съществува потребител с e-mail адрес test11@gmail.com")

		u.Caretaker.HourRate = 9999
		res = as.JSON("/caretaker/register").Put(u)
		as.Equal(406, res.Code)
		as.Contains(res.Body.String(), "Вече съществува потребител с e-mail адрес test11@gmail.com")
		as.Contains(res.Body.String(), "Невалидна надница")

		u.Caretaker.HourRate = 34.44
		u.Email = nulls.NewString("test111@gmail.com")

		//--- Всичко 6
		as.DBDelta(1, "caretakers", func() {
			res := as.JSON("/caretaker/register").Put(u)
			as.Equal(200, res.Code)
		})
	})

}

func (as *ActionSuite) Test_Users_GetHireRequests() {
	as.LoadFixture("a lot of hire requests")
	token, u := loginUser(as)

	oldU := models.User{}
	as.NoError(as.DB.Where("email=?", "hireRequest2@user.email").First(&oldU))

	err := as.DB.RawQuery("UPDATE hire_requests SET user_id = ? WHERE user_id =? ", u.ID, oldU.ID).Exec()
	as.NoError(err)

	//all
	res := as.JSON("/hire/requests/all?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 1")
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 2")

	//pending
	res = as.JSON("/hire/requests/pending?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "null")

	//accepted
	res = as.JSON("/hire/requests/accepted?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 1")

	//declined
	res = as.JSON("/hire/requests/declined?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 2")

	//invalid status
	res = as.JSON("/hire/requests/ivan?_token=" + token).Get()
	as.Equal(404, res.Code, "Expects 404 but got %d - %s", res.Code, res.Body.String())

}

func (as *ActionSuite) Test_Users_GetHireRequestsCaretaker() {
	as.LoadFixture("a lot of hire requests")
	token, u := loginUser(as)

	oldU := models.User{}
	as.NoError(as.DB.Where("email=?", "fixtureHireRequestCaretkers0@user.email").First(&oldU))

	err := as.DB.RawQuery("UPDATE hire_requests SET user_id = ? WHERE user_id =? ", u.ID, oldU.ID).Exec()
	as.NoError(err)

	u.CaretakerID = oldU.CaretakerID
	as.NoError(as.DB.Save(u))

	//all
	res := as.JSON("/hire/requests/all?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 0")

	//pending
	res = as.JSON("/hire/requests/pending?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 0")

	//accepted
	res = as.JSON("/hire/requests/accepted?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "null")

	//declined
	res = as.JSON("/hire/requests/declined?_token=" + token).Get()
	as.Equal(200, res.Code, "Expects 200 but got %d - %s", res.Code, res.Body.String())
	as.Contains(res.Body.String(), "null")

	//invalid status
	res = as.JSON("/hire/requests/ivan?_token=" + token).Get()
	as.Equal(404, res.Code, "Expects 404 but got %d - %s", res.Code, res.Body.String())
}

func (as *ActionSuite) Test_Users_UpdateUser() {
	token, u := loginUser(as)
	url := fmt.Sprintf("/user/update?_token=%s", token)
	u.Password = ""
	u.PasswordConfirmation = ""

	//Валидено населено мясро
	u.City = nulls.NewString("Dobrich 9300")
	res := as.JSON(url).Post(u)
	as.Equal(200, res.Code, "should return 200 but got %d -- %s", res.Code, res.Body.String())
	//проверява, дали е hit-нало базата
	newU := models.User{}
	as.NoError(as.DB.Find(&newU, u.ID))
	as.Equal("Dobrich 9300", newU.City.String)
	//дали останалите полета не са счупени
	as.Equal(strings.ToLower(u.SecondName), newU.SecondName)
	as.Equal(u.Location, newU.Location)
	as.Equal(u.ProviderID, newU.ProviderID)
	as.Equal(u.PasswordHash, newU.PasswordHash)

	//Невалидно поле
	u.City = nulls.NewString("1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–1234567890–")
	res = as.JSON(url).Post(u)
	as.Equal(406, res.Code)
	as.Contains(res.Body.String(), "Името на населеното място")
	u.City = nulls.NewString("Dobrich 9300")

	//Пробва да смени паролата -> добавя oldPassword field към request-a
	u.Password = "go6o123456"
	u.PasswordConfirmation = "go6o123456"
	type expandedUser struct {
		models.User
		OldPass string `json:"old_password"`
	}
	//Грешна стара парола
	expU := expandedUser{*u, "login_user_pass_wrong"}
	res = as.JSON(url).Post(expU)
	as.Equal(403, res.Code, "should return 403 but got %d -- %s", res.Code, res.Body.String())
	expU.OldPass = "login_user_pass" //правилната парола
	res = as.JSON(url).Post(expU)
	as.Equal(200, res.Code)
	//логва се с новата парола
	res = as.JSON("/auth").Post(u)
	as.Equal(200, res.Code, "should return 200 but got %d -- %s", res.Code, res.Body.String())

	//Пробва да смени паролите, но password_confirmation-а не съвпада
	expU.PasswordConfirmation = "страцимир"
	expU.OldPass = "go6o123456"
	res = as.JSON(url).Post(expU)
	as.Equal(406, res.Code)
	as.Contains(res.Body.String(), "Паролите не съвпадат")

}

func (as *ActionSuite) Test_Users_UsersVerifyEmail() {
	as.LoadFixture("a lot of email verifications")
	u := models.User{}
	e := models.EmailVerification{}

	as.NoError(as.DB.Where("verified=?", false).First(&u))
	as.NoError(as.DB.Where("user_id=?", u.ID).Last(&e))

	res := as.JSON(fmt.Sprintf("/verify/email/%s", "invalid_token")).Get()
	as.Equal(404, res.Code)

	as.DBDelta(-1, "email_verifications", func() {
		res = as.JSON(fmt.Sprintf("/verify/email/%s", e.Token)).Get()
		as.Equal(303, res.Code)
		as.Contains(res.Header().Get("Location"), "/login")
		as.Equal("http://127.0.0.1:8080/login", res.Header().Get("Location"))
	})

	as.NoError(as.DB.Find(&u, u.ID))
	as.Equal(true, u.Verified)
}
