package actions

import (
	"encoding/json"
	"fmt"

	"github.com/kolioDev/care/models"
)

func (as *ActionSuite) Test_Notifications_GetNotifications() {
	as.LoadFixture("lots of notifications")
	token, u := loginUser(as)
	notiU := models.User{}
	as.NoError(as.DB.Where("email=?", "notification@user1.email").First(&notiU))

	err := as.DB.RawQuery("UPDATE notifications SET user_id=? WHERE user_id=?", u.ID, notiU.ID).Exec()
	as.NoError(err)

	//Без skip/get параметри
	res := as.JSON("/notifications/?_token=" + token).Get()
	as.Equal(200, res.Code)
	ns := models.Notifications{}
	as.NoError(json.Unmarshal([]byte(res.Body.String()), &ns))
	as.Equal(6, len(ns))

	//С skip/get параметри
	res = as.JSON("/notifications?skip=1;get=3;_token=" + token).Get()
	as.Equal(200, res.Code)
	ns = models.Notifications{}
	as.NoError(json.Unmarshal([]byte(res.Body.String()), &ns))
	as.Equal(3, len(ns))

}

func (as *ActionSuite) Test_Notifications_DeleteNotification() {
	as.LoadFixture("lots of notifications")
	token, u := loginUser(as)
	notiU := models.User{}
	n := models.Notification{}

	as.NoError(as.DB.Where("email=?", "notification@user1.email").First(&notiU))
	as.NoError(as.DB.Where("user_id=?", notiU.ID).First(&n))

	//Login user-a  не притежава известието
	res := as.JSON(fmt.Sprintf("/notification/%s/remove?_token=%s", n.ID, token)).Delete()
	as.Equal(403, res.Code)

	//Всичко 6
	err := as.DB.RawQuery("UPDATE notifications SET user_id=? WHERE user_id=?", u.ID, notiU.ID).Exec()
	as.NoError(err)
	res = as.JSON(fmt.Sprintf("/notification/%s/remove?_token=%s", n.ID, token)).Delete()
	as.Equal(200, res.Code)

}
func (as *ActionSuite) Test_Notifications_SetRead() {
	as.LoadFixture("lots of notifications")
	token, u := loginUser(as)
	notiU := models.User{}
	n := models.Notification{}

	as.NoError(as.DB.Where("email=?", "notification@user1.email").First(&notiU))
	as.NoError(as.DB.Where("user_id=?", notiU.ID).First(&n))

	//Login user-a  не притежава известието
	res := as.JSON(fmt.Sprintf("/notification/%s/read?_token=%s", n.ID, token)).Post(nil)
	as.Equal(403, res.Code)

	//Всичко 6
	err := as.DB.RawQuery("UPDATE notifications SET user_id=? WHERE user_id=?", u.ID, notiU.ID).Exec()
	as.NoError(err)
	res = as.JSON(fmt.Sprintf("/notification/%s/read?_token=%s", n.ID, token)).Post(nil)
	as.Equal(200, res.Code)
}
