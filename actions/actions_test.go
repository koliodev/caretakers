package actions

import (
	"encoding/json"
	"testing"

	"github.com/kolioDev/care/models"

	"github.com/gobuffalo/packr/v2"
	"github.com/gobuffalo/suite"
)

type ActionSuite struct {
	*suite.Action
}

func Test_ActionSuite(t *testing.T) {
	action, err := suite.NewActionWithFixtures(App(), packr.New("Test_ActionSuite", "../fixtures"))
	if err != nil {
		t.Fatal(err)
	}

	as := &ActionSuite{
		Action: action,
	}
	suite.Run(t, as)
}

//Log-ва потребителя
func loginUser(as *ActionSuite) (string, *models.User) {
	//Log-ва потребителя
	as.LoadFixture("login user")
	u := &models.User{}
	as.NoError(
		as.DB.Where("email=?", "login@user.email").First(u),
	)
	u.Password = "login_user_pass"
	res := as.JSON("/auth").Post(u)

	as.Equal(200, res.Code, "should return 200 but got %d -- %s", res.Code, res.Body.String())

	var resBody = &struct {
		Token     string `json:"token"`
		ExpiresAt int    `json:"expires_at"`
	}{}

	as.NoError(json.Unmarshal(res.Body.Bytes(), resBody))

	return resBody.Token, u
}
