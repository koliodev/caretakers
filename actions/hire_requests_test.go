package actions

import (
	"fmt"
	"time"

	"github.com/gobuffalo/nulls"
	"github.com/kolioDev/care/models"
)

func (as *ActionSuite) Test_HireRequests_ChangeStatus() {
	as.LoadFixture("a lot of hire requests")

	token, u := loginUser(as)

	//### Дали user-a може да променя (НЕ МОЖЕ)

	oldU := models.User{}
	as.NoError(as.DB.Where("email=?", "fixtureHireRequestCaretkers0@user.email").First(&oldU))

	err := as.DB.RawQuery("UPDATE hire_requests SET user_id = ? WHERE caretaker_id =? ", u.ID, oldU.CaretakerID).Exec()
	as.NoError(err)

	h := models.HireRequest{}
	as.NoError(as.DB.Where("user_id=?", u.ID).First(&h))

	res := as.JSON(fmt.Sprintf("/hire/request/%s/change/status/accepted", h.ID)).Post(map[string]string{
		"_token": token,
	})
	as.Equal(403, res.Code, fmt.Sprintf("Expects 403 got %d, %s", res.Code, res.Body.String()))

	//Дали caretaker-а може да променя (МОЖЕ)
	u.CaretakerID = oldU.CaretakerID
	as.NoError(as.DB.Save(u))

	as.DBDelta(1, "notifications", func() { //проверка, дали е пратило известие
		res = as.JSON(fmt.Sprintf("/hire/request/%s/change/status/accepted", h.ID)).Post(map[string]string{
			"_token": token,
		})

		as.Equal(200, res.Code, fmt.Sprintf("Expects 200 got %d, %s", res.Code, res.Body.String()))
		//проверка, дали наистина е променило базата
		newH := models.HireRequest{}
		as.DB.Find(&newH, h.ID)
		as.Equal("accepted", newH.Status)

		n := models.Notification{}
		as.NoError(as.DB.Last(&n))
		as.Equal("Login User прие поръчката за лице FixtureHR0", n.Message)
	})

	//Aко има проблем със статуса (трябва да върне 403)
	u.CaretakerID = oldU.CaretakerID
	as.NoError(as.DB.Save(u))
	res = as.JSON(fmt.Sprintf("/hire/request/%s/change/status/declined", h.ID)).Post(map[string]string{
		"_token": token,
	})
	as.Equal(403, res.Code, fmt.Sprintf("Expects 403 got %d, %s", res.Code, res.Body.String()))

}

func (as *ActionSuite) Test_HireRequest_GetRequest() {
	as.LoadFixture("a lot of hire requests")
	token, u := loginUser(as)
	h := models.HireRequest{}
	as.NoError(
		as.DB.Where("message=?", "Моля ви да се грижите за моето куче 0").First(&h))

	url := fmt.Sprintf("/hire/request/%v?_token=%s", h.ID, token)

	//Потребителя нама нищо общо с request-a
	res := as.JSON(url).Get()
	as.Equal(404, res.Code)

	//Потребитял е user-a в request-a
	err := as.DB.RawQuery("UPDATE hire_requests SET user_id = ? WHERE id =? ", u.ID, h.ID).Exec()
	as.NoError(err)
	res = as.JSON(url).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 0")
	as.Contains(res.Body.String(), "pending")

	//Потребитял е caretaker-a в request-a

	c := &models.Caretaker{
		Bio:        "Алеко Иваницов Константинов, познат под псевдонима Щастливеца, е български писател, адвокат, общественик и основател на организираното туристическо движение в България.",
		Experience: "Работа с деца, коне и кучета",
		Edu:        "",
		Skills:     []string{"плетене", "заваряне", "струговане"},
		WorksWith:  []string{"деца", "кучета"},
		HourRate:   10.50,
		Birth:      time.Date(2001, 1, 19, 0, 0, 0, 0, time.UTC),
		Picture:    "base64",
	}
	c.Create(as.DB)
	u.CaretakerID = nulls.NewUUID(c.ID)
	as.NoError(as.DB.Save(u))

	h.CaretakerID = c.ID
	as.NoError(as.DB.Update(&h))

	res = as.JSON(url).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Моля ви да се грижите за моето куче 0")
	as.Contains(res.Body.String(), "pending")

	//грешно id
	url = fmt.Sprintf("/hire/request/%v?_token=%s", "98967d04-ebfa-45cf-9485-2298ef92d0e0", token)
	res = as.JSON(url).Get()
	as.Equal(404, res.Code)

	//невалидно id
	url = fmt.Sprintf("/hire/request/%v?_token=%s", "Тишо-ми-дължи-5-вафли-Боровец-сини", token)
	res = as.JSON(url).Get()
	as.Equal(404, res.Code)
}

func (as *ActionSuite) Test_HireRequests_Send() {
	as.LoadFixture("a lot of caretakers")
	as.LoadFixture("a lot of individuals")

	token, u := loginUser(as)
	i := models.Individual{}
	c := models.Caretaker{}
	cu := models.User{}

	as.NoError(as.DB.Where("city=?", "Dobrich").First(&i))
	as.NoError(as.DB.Where("tip_rate=?", 79.97).First(&c))
	as.NoError(as.DB.Where("caretaker=?", c.ID).First(&cu))

	i.UserID = u.ID
	as.NoError(as.DB.Save(&i))

	as.DBDelta(1, "hire_requests", func() { //проверка, дали е запазило в базата
		//проверка, дали е пратило известия на изпращачия и на получателя
		as.DBDelta(2, "notifications", func() {

			res := as.JSON("/hire?_token=" + token).Post(map[string]string{
				"individual_id": i.ID.String(),
				"caretaker_id":  c.ID.String(),
				"message":       "Гледай ми кучето",
				"weekly":        "true",
				"times":         "{\"1\":{\"start\":12.3,\"end\":23.45},\"3\":{\"start\":12.3,\"end\":23.45}}",
			})

			as.Equal(200, res.Code, fmt.Sprintf("expects 200 but got %d (%s)", res.Code, res.Body.String()))
		})

		n1 := models.Notification{}
		n2 := models.Notification{}
		as.DB.Where("user_id=?", u.ID).First(&n1)
		as.DB.Where("user_id=?", cu.ID).First(&n2)

		as.Equal("Успешно изпратена поръчка към Caretaker User за лице FixtureГошо", n1.Message)
		as.Equal("Login User ви изпрати поръчка за лице FixtureГошо", n2.Message)

	})
	//Невалидно съобщение
	res := as.JSON("/hire?_token=" + token).Post(map[string]string{
		"individual_id": i.ID.String(),
		"caretaker_id":  c.ID.String(),
		"message":       "",
		"weekly":        "true",
		"times":         "{\"1\":{\"start\":12.3,\"end\":23.45},\"3\":{\"start\":12.3,\"end\":23.45}}",
	})

	as.Equal(406, res.Code, fmt.Sprintf("expects 406 but got %d (%s)", res.Code, res.Body.String()))

	//Невалидни id-та
	res = as.JSON("/hire?_token=" + token).Post(map[string]string{
		"individual_id": "ivan",
		"caretaker_id":  "go6o",
		"message":       "Гледай ми кучето",
		"weekly":        "true",
		"times":         "{\"1\":{\"start\":12.3,\"end\":23.45},\"3\":{\"start\":12.3,\"end\":23.45}}",
	})

	as.Equal(403, res.Code, fmt.Sprintf("expects 403 but got %d (%s)", res.Code, res.Body.String()))

	//Несъществуващ caretaker
	res = as.JSON("/hire?_token=" + token).Post(map[string]string{
		"individual_id": i.ID.String(),
		"caretaker_id":  "2f17ce69-7ee9-44f3-b989-d0a852bb904f",
		"message":       "Гледай ми кучето",
		"weekly":        "true",
		"times":         "{\"1\":{\"start\":12.3,\"end\":23.45},\"3\":{\"start\":12.3,\"end\":23.45}}",
	})

	as.Equal(403, res.Code)

	//Несъществвуващ  individual
	res = as.JSON("/hire?_token=" + token).Post(map[string]string{
		"individual_id": "2f17ce69-7ee9-44f3-b989-d0a852bb904f",
		"caretaker_id":  c.ID.String(),
		"message":       "Гледай ми кучето",
		"weekly":        "true",
		"times":         "{\"1\":{\"start\":12.3,\"end\":23.45},\"3\":{\"start\":12.3,\"end\":23.45}}",
	})

	as.Equal(403, res.Code)

	//Невалидни дати / часове
	res = as.JSON("/hire?_token=" + token).Post(map[string]string{
		"individual_id": i.ID.String(),
		"caretaker_id":  c.ID.String(),
		"message":       "Гледай ми кучето",
		"weekly":        "true",
		"times":         "{\"iv4o\":{\"start\":12.3,\"end\":23.45},\"3\":{\"start\":12.3,\"end\":23.45}}",
	})

	as.Equal(403, res.Code)

	//Dublicate
	as.DBDelta(0, "hire_requests", func() {
		res := as.JSON("/hire?_token=" + token).Post(map[string]string{
			"individual_id": i.ID.String(),
			"caretaker_id":  c.ID.String(),
			"message":       "Гледай ми кучето",
			"weekly":        "true",
			"times":         "{\"1\":{\"start\":12.3,\"end\":23.45},\"3\":{\"start\":12.3,\"end\":23.45}}",
		})

		as.Equal(406, res.Code)
	})

}
