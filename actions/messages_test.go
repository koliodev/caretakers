package actions

import (
	"fmt"

	"github.com/kolioDev/care/models"
)

func (as *ActionSuite) Test_Messages_SendMessage() {
	as.LoadFixture("a lot of hire requests")
	token, u := loginUser(as)
	h := models.HireRequest{}
	as.NoError(
		as.DB.Where("message=?", "Моля ви да се грижите за моето куче 0").First(&h))

	err := as.DB.RawQuery("UPDATE hire_requests SET user_id = ? WHERE id =? ", u.ID, h.ID).Exec()
	as.NoError(err)

	//Проверява, дали е hit-нало базата със съобшенията
	as.DBDelta(1, "messages", func() {
		//Проверява, дали е hit-нало базата със известията
		as.DBDelta(1, "notifications", func() {
			res := as.JSON("/send/message/").Post(map[string]interface{}{
				"room":    h.RoomID.String(),
				"message": "Ko стаа луд ?",
				"_token":  token,
			})
			as.Equal(200, res.Code, fmt.Sprintf("Expects 200 but got %d with body %s", res.Code, res.Body.String()))
		})

		n := models.Notification{}
		as.NoError(as.DB.First(&n))
		as.Equal("Login User ви изпрати съобщение", n.Message)
	})

	//Проверява, дали ако прати второ съобщение, ще прати второ известие
	as.DBDelta(0, "notifications", func() {
		res := as.JSON("/send/message/").Post(map[string]interface{}{
			"room":    h.RoomID.String(),
			"message": "Ko стаа луд ?",
			"_token":  token,
		})
		as.Equal(200, res.Code, fmt.Sprintf("Expects 200 but got %d with body %s", res.Code, res.Body.String()))
	})

	//Изпращача е caretaker
	as.DBDelta(1, "messages", func() {
		//Проверява, дали е hit-нало базата със известията
		as.DBDelta(0, "notifications", func() {
			res := as.JSON("/send/message/").Post(map[string]interface{}{
				"room":    h.RoomID.String(),
				"message": "Ko стаа луд ?",
				"_token":  token,
			})
			as.Equal(200, res.Code, fmt.Sprintf("Expects 200 but got %d with body %s", res.Code, res.Body.String()))
		})
	})

	//невалиден hire request
	res := as.JSON("/send/message/").Post(map[string]interface{}{
		"hire_request": "f271829c-403f-43e9-84ac-a8ec2cba36e2",
		"message":      "Ko стаа луд ?",
		"_token":       token,
	})
	as.Equal(406, res.Code, fmt.Sprintf("Expects 406 but got %d with body %s", res.Code, res.Body.String()))

	//липсващо съобщение
	res = as.JSON("/send/message/").Post(map[string]interface{}{
		"hire_request": h.ID.String(),
		"message":      "",
		"_token":       token,
	})

	//твърде дълго съобщение

	mess := ""

	for i := 1; i <= 1025; i++ {
		mess += "O"
	}

	res = as.JSON("/send/message/").Post(map[string]interface{}{
		"hire_request": h.ID.String(),
		"message":      mess,
		"_token":       token,
	})

	as.Equal(406, res.Code)
}

func (as *ActionSuite) Test_Messages_SetReadCursor() {
	url := "/message/set/cursor"
	as.LoadFixture("a lot of messages")
	token, u := loginUser(as)

	//Задава на всички съобщения pusher_id
	ms := models.Messages{}
	as.DB.All(&ms)
	for k, m := range ms {
		verrs, err := m.SetPusherID(as.DB, uint(k*10+1))
		as.NoError(err)
		as.False(verrs.HasAny())
	}

	//Потребитялят не е receiver на съобшенията
	res := as.JSON(url).Post(map[string]string{
		"id":     "31",
		"_token": token,
	})

	as.Equal(403, res.Code)

	//Прави потребителя receiver на всички съобщения
	for _, m := range ms {
		err := as.DB.RawQuery("UPDATE hire_requests SET user_id=? WHERE id=?", u.ID, m.HireRequestID).Exec()
		as.NoError(err)
		err = as.DB.RawQuery("UPDATE messages SET receiver=? WHERE id=?", u.ID, m.ID).Exec()
		as.NoError(err)
	}
	//Потребитялят не е receiver на съобшенията
	res = as.JSON(url).Post(map[string]string{
		"id":     "11",
		"_token": token,
	})

	as.Equal(200, res.Code, fmt.Sprintf("Wants 200 got %d (%s)", res.Code, res.Body.String()))

	res = as.JSON(url).Post(map[string]string{
		"id":     "20",
		"_token": token,
	})
	as.Equal(404, res.Code)

}

func (as *ActionSuite) Test_Messages_GetForRoom() {
	as.LoadFixture("a lot of messages")
	token, u := loginUser(as)
	h := models.HireRequest{}
	as.NoError(as.DB.Where("message=?", "Моля ви да се грижите за моето куче hire request messages1").First(&h))

	//Aко потребителя не е свързан с room-a (request-a)
	res := as.JSON(fmt.Sprintf("/messages/%s?_token=%s", h.ID, token)).Get()
	as.Equal(403, res.Code, fmt.Sprintf("wants 403 got %d (%s)", res.Code, res.Body.String()))

	//Aко потребителя E свързан с room-a (request-a)
	as.NoError(as.DB.RawQuery("UPDATE hire_requests SET user_id=? WHERE id=?", u.ID, h.ID).Exec())
	res = as.JSON(fmt.Sprintf("/messages/%s?_token=%s", h.ID, token)).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "message4")
	as.Contains(res.Body.String(), "message3")
	as.Contains(res.Body.String(), "message2")
	as.Contains(res.Body.String(), "message1")

	//offset и limit
	as.NoError(as.DB.RawQuery("UPDATE hire_requests SET user_id=? WHERE id=?", u.ID, h.ID).Exec())
	res = as.JSON(fmt.Sprintf("/messages/%s?_token=%s;skip=%d;get=%d", h.ID, token, 1, 3)).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "message3")
	as.Contains(res.Body.String(), "message2")

}

func (as *ActionSuite) Test_Messages_MediaCall() {
	as.LoadFixture("a lot of hire requests")
	token, u := loginUser(as)
	h := models.HireRequest{}

	as.NoError(as.DB.First(&h))

	url := fmt.Sprintf("/call/send/%s", h.ID)
	d := map[string]string{
		"_token": token,
	}

	res := as.JSON(url).Post(d)
	as.Equal(403, res.Code)

	err := as.DB.RawQuery("UPDATE hire_requests SET user_id = ? WHERE caretaker_id =? ", u.ID, h.CaretakerID).Exec()
	as.NoError(err)

	res = as.JSON(url).Post(d)
	as.Equal(200, res.Code)
}
