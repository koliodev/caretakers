package actions

/**----------------------------------------------------------------------------

				HireRequests.go

Контролер за нещата свързани с  Заявките

Action-и :
	GetRequest()   ->  Връща денните за заявка с дадено ID
	Send() 		   ->  Изпраща заявка от логнатия user до някой caretaker
	ChangeStatus() ->  Приема/Отхвърля заявка

------------------------------------------------------------------------------**/

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/kolioDev/care/mailers"
	"strings"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/kolioDev/care/models"
	"github.com/pkg/errors"
	"github.com/pusher/chatkit-server-go"

	"log"
	"time"
)

const INITIAL_MSG_INDICATOR = "#####!#####INITIAL_MESSAGE"

//UsersSendHireRequest изпраща request към гледач
func HireRequestsSend(c buffalo.Context) error {
	tx := models.DB //c.Value("tx").(*pop.Connection)

	c, err := postParse(c)
	if err != nil {
		return errors.WithStack(err)
	}

	u := c.Value("current_user").(*models.User)

	if u == nil || u.ID == uuid.Nil {
		return c.Render(401, r.JSON("Unauthorized"))
	}

	//Намира individual и caretaker
	i := &models.Individual{}
	ct := &models.Caretaker{}

	iIDErr := tx.Find(i, c.Request().PostForm.Get("individual_id"))
	cIDErr := tx.Find(ct, c.Request().PostForm.Get("caretaker_id"))
	msg := c.Request().PostForm.Get("message")

	if c.Request().PostForm.Get("weekly") == "" || c.Request().PostForm.Get("times") == "" {
		return c.Render(403, r.JSON("Invalid dates time settings 0"))
	}

	weekly := c.Request().PostForm.Get("weekly") == "true"
	var dt = map[interface{}]models.WorkingTime{}
	if weekly {
		dtw := map[int]models.WorkingTime{}
		err = json.Unmarshal([]byte(c.Request().PostForm.Get("times")), &dtw)
		for k, v := range dtw {
			dt[k] = v
		}
	} else {
		dtw := map[time.Time]models.WorkingTime{}
		err = json.Unmarshal([]byte(c.Request().PostForm.Get("times")), &dtw)
		for k, v := range dtw {
			dt[k] = v
		}
	}

	if err != nil {
		return c.Render(403, r.JSON("Invalid dates time settings 1"))
	}

	//Aко не съществуват такива individual или caretaker
	if iIDErr != nil || cIDErr != nil {
		return c.Render(403, r.JSON("invalid uuid"))
	}

	//Прави request
	h, verr, err := u.RequestHire(tx, msg, weekly, dt, *ct, *i)
	if err != nil {
		return errors.WithStack(err)
	}
	if verr.HasAny() {
		return c.Render(406, r.JSON(verr.Errors))
	}

	//Създава pusher chat_kit стая
	if envy.Get("GO_ENV", "development") != "test" && envy.Get("GO_DISABLE_PUSHER", "false") != "true" {
		if h.RoomID == h.ID { //ако е първата такава стая
			roomID := h.RoomID.String()

			uE := h.User.Email.String
			if len(uE) > 10 {
				uE = uE[0:10]
			}
			cE := h.Caretaker.Email.String
			if len(cE) > 10 {
				cE = cE[0:10]
			}

			name := fmt.Sprintf("%s-&-%s", uE, cE)
			if len(name) > 60 {
				name = name[:60]
			}
			chatter := NewChatter()
			_, err = chatter.client.CreateRoom(context.Background(), chatkit.CreateRoomOptions{
				ID:        &roomID,
				Name:      name,
				Private:   true,
				UserIDs:   []string{h.UserID.String(), h.Caretaker.ID.String()},
				CreatorID: u.ID.String(),
			})

			if err != nil {
				return errors.WithStack(err)
			}
		}

		//Изпращане на начално съобщение
		_, verrs, err := sendMessage(tx, msg+INITIAL_MSG_INDICATOR+h.ID.String(), u.ID, *h)
		if err != nil {
			return errors.WithStack(err)
		}
		if verrs.HasAny() {
			return c.Render(406, r.JSON(verrs.Errors))
		}

	}

	//праща известие тип  request.received И request.sent
	cu := models.User{} //caretaker user
	if err := tx.Where("caretaker=?", ct.ID).First(&cu); err != nil {
		return errors.WithStack(err)
	}
	//request.sent
	err = notify(tx, "request.sent", *u, h.ID,
		fmt.Sprintf("%s %s", strings.Title(cu.FirstName), strings.Title(cu.SecondName)),
		strings.Title(i.Name),
	)
	if err != nil {
		log.Println("NOTIFICATION SENT ERROR", err.Error())
	}
	//request.received
	err = notify(tx, "request.received", cu, h.ID,
		fmt.Sprintf("%s %s", strings.Title(u.FirstName), strings.Title(u.SecondName)),
		strings.Title(i.Name),
	)
	if err != nil {
		log.Println("NOTIFICATION SENT ERROR", err.Error())
	}

	h.User = *u
	h.Individual = *i
	go mailers.SendHireRequestReceived(cu, *h)

	return c.Render(200, r.JSON("OK"))
}

// HireRequestsChangeStatus променя статуса на някой request
func HireRequestsChangeStatus(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := models.DB // c.Value("tx").(*pop.Connection)
	h := models.HireRequest{}

	hID := c.Param("id")
	status := c.Param("status")

	if err := tx.Find(&h, hID); err != nil {
		return errors.WithStack(err)
	}

	//проверка, дали логнатия потребител е caretaker-а на този request
	if u.CaretakerID.UUID != h.CaretakerID {
		return c.Render(403, r.JSON("user is not request's caretaker"))
	}

	verrs, err := h.ChangeStatus(tx, status)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		return c.Render(403, r.JSON(verrs.Errors))
	}

	//праща известие тип  request.accepted ИЛИ request.declined
	err = notify(tx, "request."+status, h.User, h.ID, fmt.Sprintf("%s %s", strings.Title(u.FirstName), strings.Title(u.SecondName)),
		strings.Title(h.Individual.Name))
	if err != nil {
		log.Println("NOTIFICATION SENT ERROR", err.Error())
	}

	go mailers.SendHireRequestChangedStatus(h.User, h)

	return c.Render(200, r.JSON("OK"))
}

//HireRequestGetRequest връща данните за request с дадено id
func HireRequestGetRequest(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := c.Value("tx").(*pop.Connection)
	h := models.HireRequest{}

	q := &pop.Query{}

	if u.CaretakerID.Valid { //Ако потребителя  е caretaker
		q = tx.Where("caretaker_id=?", u.CaretakerID)
	} else { //Ако потребителя  е  user
		q = tx.Where("user_id=?", u.ID)
	}

	if err := q.Find(&h, c.Param("id")); err != nil {
		return c.Render(404, r.String("Not found"))
	}

	return c.Render(200, r.JSON(h))
}

//UserHasIndividual MIDLEWARE проверява, дали потребител e caretaker или user в даден hire_request
func UserInHireRequest(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		respMsg :="Unauthorized -  not in hire request"

		c, err := postParse(c)
		if err != nil {
			return errors.WithStack(err)
		}
		hID := c.Request().PostForm.Get("hire_request")

		if hID == "" {
			hID = c.Param("hire_request")
		}

		if hID == "" {
			return c.Render(403, r.JSON(respMsg+"0"))
		}

		u := c.Value("current_user").(*models.User)
		h := models.HireRequest{}

		if err := models.DB.Find(&h, hID); err != nil {
			return c.Render(403, r.JSON(respMsg+"1"))
		}

		if h.UserID != u.ID  && h.CaretakerID != u.CaretakerID.UUID{
			return c.Render(403, r.JSON(respMsg+"2"))
		}

		return next(c)
	}
}