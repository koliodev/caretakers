package actions

/**----------------------------------------------------------------------------

				Caretakers.go

Контролер за нещата свързани с гледачите

Action-и :
	GetCaretaker() 	  ->  връща информация за caretaker (с определено id)
	UpdateCaretaker() ->  промяна на някои от данните на caretaker с дадено id
	FindIndividuals() ->  връща подходящите лица за log-натия caretaker
    GetCalendarInfo() ->  връща информация, кога с какво е зает гледача (като поръчки)

------------------------------------------------------------------------------**/

import (
	"fmt"
	"strconv"
	"time"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/kolioDev/care/models"
	"github.com/pkg/errors"
)

// CaretakersGetCaretaker връща информация за caretaker на база id
func CaretakersGetCaretaker(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	ctaker := &models.User{}
	if err := tx.Find(ctaker, c.Param("caretaker_id")); err != nil {
		if err.Error() == "sql: no rows in result set" {
			return c.Render(404, r.String("Not found"))
		}
		return errors.WithStack(err)
	}
	if !ctaker.CaretakerID.Valid {
		return c.Render(404, r.String("Not found"))
	}
	return c.Render(200, r.JSON(ctaker))
}

//CaretakersUpdateCaretaker променя данните в caretaker частта нa някой потребител
func CaretakersUpdateCaretaker(c buffalo.Context) error {
	tx := models.DB // c.Value("tx").(*pop.Connection)
	u := c.Value("current_user").(*models.User)
	if !u.CaretakerID.Valid {
		return c.Render(403, r.String("user is not caretaker"))
	}

	if err := c.Bind(u.Caretaker); err != nil {
		return errors.WithStack(err)
	}

	verrs, err := u.Caretaker.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(406, r.JSON(verrs.Errors))
	}

	return c.Render(200, r.String("updated"))
}

// CaretakersFindIndividuals връща подходящите лица за даден caretaker
func CaretakersFindIndividuals(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := c.Value("tx").(*pop.Connection)

	dist, err := strconv.ParseFloat(c.Param("proximity"), 32)
	if err != nil {
		return errors.WithStack(err)
	}

	inds, err := u.Caretaker.FindIndividuals(tx, float32(dist))
	if err != nil {
		return errors.WithStack(err)
	}
	return c.Render(200, r.JSON(inds))
}

//CaretakersGetCalendarInfo връща данните нуждни за страничката на календара
func CaretakersGetCalendarInfo(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := models.DB //c.Value("tx").(*pop.Connection)

	dts, err := u.Caretaker.GetCalendarInfo(tx)
	if err != nil {
		return errors.WithStack(err)
	}

	//Прави dts в нещо, което може да мине през json parse
	dtsNormal := map[string][]models.WorkingTimeWithHR{}
	for k, d := range dts {
		t, isDt := k.(time.Time)
		if isDt {
			dtsNormal[t.String()] = d
			continue
		}
		dtsNormal[fmt.Sprint(k)] = d
	}

	return c.Render(200, r.JSON(dtsNormal))
}
