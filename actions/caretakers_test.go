package actions

import (
	"fmt"

	"github.com/gobuffalo/nulls"
	"github.com/kolioDev/care/models"
)

func (as *ActionSuite) Test_Caretakers_GetCaretaker() {
	as.LoadFixture("a lot of caretakers")

	bio := "Биографията на гледач-потребител 1"

	c := &models.Caretaker{}
	as.NoError(as.DB.Where("bio=?", bio).First(c)) //Тоя е от Добрич

	cu := &models.User{}
	as.NoError(as.DB.Where("caretaker=?", c.ID).First(cu))

	token, u := loginUser(as)

	res := as.JSON(fmt.Sprintf("/caretaker/get/%v?_token=%s", cu.ID, token)).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), bio)

	//Потребителя не е caretaker
	res = as.JSON(fmt.Sprintf("/caretaker/get/%v?_token=%s", u.ID, token)).Get()
	as.Equal(404, res.Code)

	//Не съществува такъв user
	res = as.JSON(fmt.Sprintf("/caretaker/get/%v?_token=%s", c.ID, token)).Get()
	as.Equal(404, res.Code)
}

func (as *ActionSuite) Test_Caretakers_UpdateCaretaker() {
	as.LoadFixture("a lot of caretakers")
	token, u := loginUser(as)
	c := models.Caretaker{}
	as.NoError(as.DB.First(&c))
	url := fmt.Sprintf("/caretaker/update?_token=%s", token)

	//Aко log-натия потребител не е caretaker
	res := as.JSON(url).Post(c)
	as.Equal(403, res.Code)

	//Ако всичко си е ок
	u.CaretakerID = nulls.NewUUID(c.ID)
	as.NoError(as.DB.Save(u))
	res = as.JSON(url).Post(c)
	as.Equal(200, res.Code)

	//Aко има валидационни грешки
	c.Bio = "панко"
	res = as.JSON(url).Post(c)
	as.Equal(406, res.Code)
	as.Contains(res.Body.String(), "Биографията трябва да е между 10 и 1000 символа")

	//Aко няма грешки, проверява дали е hit-нало базата
	c.Bio = "123 Панко го боли"
	res = as.JSON(url).Post(c)
	as.Equal(200, res.Code)
	newC := &models.Caretaker{}
	as.NoError(as.DB.Find(newC, c.ID))
	as.Equal(c.Bio, newC.Bio)
}

func (as *ActionSuite) Test_Caretakers_FindIndividuals() {
	as.LoadFixture("a lot of caretakers")
	as.LoadFixture("a lot of individuals")

	c := &models.Caretaker{}
	as.NoError(as.DB.Where("bio=?", "Биографията на гледач-потребител 1").First(c)) //Тоя е от Добрич

	token, u := loginUser(as)

	//потребителя не е caretaker
	res := as.JSON("/caretaker/find/individuals/10?_token=" + token).Get()
	as.Equal(403, res.Code, fmt.Sprintf("should return 403 but got %d with body %s", res.Code, res.Body.String()))

	u.Caretaker = c
	u.CaretakerID = nulls.NewUUID(c.ID)
	as.NoError(as.DB.Save(u))

	res = as.JSON("/caretaker/find/individuals/10?_token=" + token).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Dobrich")
	as.Contains(res.Body.String(), "Donchevo")

	res = as.JSON("/caretaker/find/individuals/2.3?_token=" + token).Get()
	as.Equal(200, res.Code)
	as.NotContains(res.Body.String(), "Donchevo")

	res = as.JSON("/caretaker/find/individuals/ivan?_token=" + token).Get()
	as.Equal(404, res.Code)

}

func (as *ActionSuite) Test_Caretakers_GetCalendarInfo() {
	as.LoadFixture("a lot of hire requests")
	token, u := loginUser(as)
	c := models.Caretaker{}
	h := models.HireRequest{}
	as.NoError(as.DB.First(&c))
	as.NoError(as.DB.First(&h))
	as.NoError(as.DB.RawQuery("UPDATE hire_requests SET caretaker_id=? , status=?", c.ID, "accepted").Exec())

	url := fmt.Sprintf("/calendar?_token=%s", token)

	//Aко потребителя НЕ Е caretaker
	res := as.JSON(url).Get()
	as.Equal(403, res.Code)

	//Aко потребителя Е caretaker
	as.NoError(as.DB.RawQuery("UPDATE users SET caretaker=? WHERE  id=?", c.ID, u.ID).Exec())
	res = as.JSON(url).Get()
	as.Equal(200, res.Code)

	//Дали response-а e какъвто трябва
	as.Contains(res.Body.String(), "fixtureHR0")
	as.Contains(res.Body.String(), "\"start\":4.54,\"end\":6.67")
	as.Contains(res.Body.String(), h.ID.String())
}
