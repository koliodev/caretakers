package actions

/**----------------------------------------------------------------------------

				Individuals.go

Контролер за нещата свързани с Лицата

Action-и :
    GetIndividual()				-> Връща данните за лице с дадено id
    GetIndividuals()  			-> Връща всички лица, който log-натия user е създал
	AddIndividual() 			-> Добавя лице (само ако log-натия user не е caretaker)
	UpdateIndividual() 	  		-> Редактира лице
	RemoveIndividual()  		-> Изтрива лице от базата
	IndividualsFindCaretakers() -> Намира подходящии гледачи за дадено лице

Middleware-и :
	UserHasIndividual() -> Ако логнатия потребител НЕ е създал лицето (чието id е в request-a)
                           Връща  403 "does not own individual"

------------------------------------------------------------------------------**/

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gofrs/uuid"
	"github.com/kolioDev/care/models"
	"github.com/pkg/errors"
)

// IndividualsAddIndividual създава ново лице
func IndividualsAddIndividual(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)

	i := &models.Individual{}
	if err := c.Bind(i); err != nil {
		return errors.WithStack(err)
	}

	u := c.Value("current_user").(*models.User)

	verrs, err := u.AddIndividual(tx, i)

	if err != nil {
		return c.Render(500, r.JSON("Internal server error"))
	}
	if verrs.HasAny() {
		return c.Render(406, r.JSON(verrs.Errors))
	}

	return c.Render(200, r.JSON(i))
}

//IndividualsUpdate променя лице
func IndividualsUpdateIndividual(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	u := c.Value("current_user").(*models.User)
	i := &models.Individual{}

	if err := c.Bind(i); err != nil {
		return errors.WithStack(err)
	}

	i.ID = uuid.FromStringOrNil(c.Param("individual_id"))
	i.UserID = u.ID

	verrs, err := i.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(406, r.JSON(verrs.Errors))
	}

	return c.Render(200, r.JSON(i))
}

//IndividualsRemove изтрива лице
func IndividualsRemoveIndividual(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	i := &models.Individual{}
	i.ID = uuid.FromStringOrNil(c.Param("individual_id"))

	if err := i.Delete(tx); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.String("OK"))
}

//IndividualsGetIndividuals  връща всички лица добавени от потребителя
func IndividualsGetIndividuals(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	tx := c.Value("tx").(*pop.Connection)
	is := models.Individuals{}

	if err := tx.Where("user_id=?", u.ID).All(&is); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(is))
}

//IndividualsFindCaretakers намира подходящите гледачи за даден потребител
func IndividualsFindCaretakers(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)

	//Proximity
	dist, err := strconv.ParseFloat(c.Param("proximity"), 32)
	if err != nil {
		return c.Render(403, r.JSON("Invalid proximity"))
	}

	//Individual ID
	individualID := c.Param("individual_id")
	i := models.Individual{}
	if err := tx.Find(&i, individualID); err != nil {
		return c.Render(403, r.JSON("Invalid individual id"))
	}

	//Skills
	skills := strings.Split(c.Param("skills"), ",")
	var skillsI []interface{}
	if len(skills) > 0 && c.Param("skills") != "" {
		for _, s := range skills {
			skillsI = append(skillsI, s)
		}
	} else {
		skillsI = nil
	}

	us, err := i.FindSuitableCaretakers(models.DB, float32(dist), skillsI)

	//TODO::delete
	log.Println(fmt.Sprintf("I.ID-%s;   dist-%f;    skillsI-%s,   users-%v", i.ID.String(), dist, skillsI, us))

	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(us))
}

//IndividualsGetIndividual връща данните за лице с дадено id
func IndividualsGetIndividual(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	id := c.Param("id")
	i := models.Individual{}

	if err := tx.Find(&i, id); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(200, r.JSON(i))
}

//UserHasIndividual MIDLEWARE проверява, дали потребител има права да прави нещо свързано с  лице
func UserHasIndividual(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {

		c, err := postParse(c)
		if err != nil {
			return errors.WithStack(err)
		}
		iID := c.Request().PostForm.Get("individual_id")

		if iID == "" {
			iID = c.Param("individual_id")
		}

		if iID == "" {
			return c.Render(403, r.JSON("Unauthorized -  does not own individual"))
		}

		u := c.Value("current_user").(*models.User)
		i := models.Individual{}

		if err := models.DB.Find(&i, iID); err != nil {
			return c.Render(403, r.JSON("Unauthorized -  does not own individual"))
		}

		if i.UserID != u.ID {
			return c.Render(403, r.JSON("Unauthorized -  does not own individual"))
		}

		return next(c)
	}
}
