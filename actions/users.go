package actions

/**----------------------------------------------------------------------------

				Auth.go

Контролер за нещата свързани с Authentication-a (login-a)

Action-и :
	Register() 		  -> Добавя user в базата (и връща JWT при успех)
	UpdateUser() 	  -> Редактира user в базата
	GetHireRequests() -> Връща всички заявки //TODO::да го преместя в hire_requests
    GetAuthUser()  	  -> Връща log-натия user

Middleware-и :
	IsCaretaker()    -> Ако потребителя НЕ е гледач връша  403 "Unauthorized - user must be caretaker"
	IsNotCaretaker() -> Ако потребителя Е гледач връша  403 "Unauthorized - user must not be caretaker"
------------------------------------------------------------------------------**/

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/pusher/chatkit-server-go"
	"log"
	"strings"

	"github.com/kolioDev/care/mailers"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"github.com/kolioDev/care/models"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// UsersRegister registers a new user.
func UsersRegister(c buffalo.Context) error {

	//Видът потребител
	t := c.Param("type")

	tx := models.DB //c.Value("tx").(*pop.Connection)

	u := &models.User{}
	if err := c.Bind(u); err != nil {
		return errors.WithStack(err)
	}

	verrs, err := u.Create(tx, t == "caretaker")
	if err != nil {
		log.Println("user create error ", err.Error())
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(406, r.JSON(verrs.Errors))
	}

	//Изпраща email с токен за верификация
	e := models.EmailVerification{}
	if err := tx.Where("user_id=?", u.ID).Last(&e); err != nil {
		return errors.WithStack(err)
	}
	go mailers.SendEmailVerifications(*u, e)

	return c.Render(200, r.String("waiting for email verification"))
}

// UsersUpdateUser променя данните на някой потреибтел (име мейл парола населено място)
func UsersUpdateUser(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	u := c.Value("current_user").(*models.User)

	upU := &models.User{} //update details for the user
	if err := c.Bind(upU); err != nil {
		return errors.WithStack(err)
	}
	upU.ID = u.ID

	//Ако пича е решил да си смеяна паролата, трябва да ми докаже, че знае предната
	if upU.Password != "" && u.PasswordHash != "" {
		c, _ := postParse(c)
		p := c.Request().PostForm.Get("old_password")

		//сравянва паролата и hash-а в базата
		err := bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(p))
		if err != nil {
			return c.Render(403, r.JSON("Invalid old password"))
		}
	}

	verrs, err := upU.Update(tx)
	if err != nil {
		return errors.WithStack(err)
	}
	if verrs.HasAny() {
		return c.Render(406, r.JSON(verrs.Errors))
	}

	return c.Render(200, r.String("updated successfully"))
}

//UserGetAuthUser връща детайлите на логнатия потребител
func UsersGetAuthUser(c buffalo.Context) error {
	u := c.Value("current_user").(*models.User)
	return c.Render(200, r.JSON(u))
}

//UsersGetHireRequests връща всичи request-и за потребител
func UsersGetHireRequests(c buffalo.Context) error {
	tx := models.DB //c.Value("tx").(*pop.Connection)
	u := c.Value("current_user").(*models.User)

	var hrs models.HireRequests
	status := c.Param("status")

	//Намества userID-то в зависимост, дали потребителя е caretaker
	uID := u.ID
	if u.CaretakerID.Valid {
		uID = u.CaretakerID.UUID
	}

	if err := hrs.Get(tx, uID, status); err != nil {
		if errors.Cause(err) != sql.ErrNoRows {
			return errors.WithStack(err)
		}
	}

	return c.Render(200, r.JSON(hrs))
}

//UsersVerifyEmail вика се, когато потребителя си отвори пощата и цъкна на "потвърди E-mail"
func UsersVerifyEmail(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	token := c.Param("token")
	e := &models.EmailVerification{}
	u := &models.User{}

	err := tx.Where("token=?", token).Last(e)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return c.Render(404, r.JSON("Not found"))
		}
		return errors.WithStack(err)
	}

	if err := tx.Find(u, e.UserID); err != nil {
		return errors.WithStack(err)
	}

	if err := u.Verify(tx); err != nil {
		return errors.WithStack(err)
	}


	//Създава потребител в pusher chatter
	if envy.Get("GO_ENV", "development") != "test" && envy.Get("GO_DISABLE_PUSHER", "false") != "true" {
		chatter := NewChatter()
		err = chatter.client.CreateUser(context.Background(), chatkit.CreateUserOptions{
			ID:   u.ID.String(),
			Name: fmt.Sprintf("%s %s", u.FirstName, u.SecondName),
		})
		if err != nil {
			return errors.WithStack(err)
		}
	}

	//Праща welcome email
	go mailers.SendWelcomeEmails(*u)

	url := envy.Get("FRONTEND_URL", "127.0.0.1:8080")
	url = url + envy.Get("FRONTEND_LOGIN_URL", "/login")
	url = strings.ReplaceAll(url, "/:provider_id", "")
	url = strings.ReplaceAll(url, "/:provider", "")



	return c.Redirect(303, url)
}

//UserIsCaretaker MIDLEWARE проверява, дали потребителя е caretaker
func UserIsCaretaker(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		u := c.Value("current_user").(*models.User)
		if !u.CaretakerID.Valid {
			return c.Render(403, r.JSON("Unauthorized - user must be caretaker"))
		}
		return next(c)
	}
}

//UserIsCaretaker MIDLEWARE проверява, дали потребителя е нормален потребител
func UserIsNotCaretaker(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		u := c.Value("current_user").(*models.User)
		if u.CaretakerID.Valid {
			return c.Render(403, r.JSON("Unauthorized - user must not be caretaker"))
		}
		return next(c)
	}
}
