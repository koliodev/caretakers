package actions

/**----------------------------------------------------------------------------

				Home.go

Контролер, който обслужва началната страница ( "/"  )

Action-и :
	HomeHandler() 		  ->  Връща списък със всички endpoint-ове (route-oве)
 							  във html или json формат

------------------------------------------------------------------------------**/

import (
	"fmt"
	"io/ioutil"
	"sort"

	"github.com/gobuffalo/buffalo"
	"gopkg.in/yaml.v2"
)

type apiDocs = map[string]struct {
	Method      string                       `json:"method" yaml:"method"`
	Description string                       `json:"description" yaml:"description"`
	Protected   bool                         `json:"protected" yaml:"protected"`
	Requires    map[string]string            `json:"requires" yaml:"requires"`
	Returns     map[string]map[string]string `json:"returns" yaml:"returns"`
}

// HomeHandler is a default handler to serve up
// a home page.
func HomeHandler(c buffalo.Context) error {
	data, err := ioutil.ReadFile("apidoc.yaml")
	if err != nil {
		return c.Render(500, r.String("Internal server error 1"))
	}

	var docs apiDocs

	err = yaml.Unmarshal([]byte(data), &docs)

	if err != nil {
		return c.Render(500, r.String("Internal server error "+err.Error()))
	}

	if c.Param("json") == "json" {
		return c.Render(200, r.JSON(docs))
	}

	html := "<h1>Api endpoints</h1><h2>visit /json for json format api route</h2>"

	//Сортира route-овете по азбучен ред
	keys := make([]string, 0, len(docs))
	for k := range docs {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		r := docs[k]

		requires := ""
		for k, r := range r.Requires {
			requires += fmt.Sprintf("%s - %s <br>", k, r)
		}

		returns := ""
		for k, r := range r.Returns {
			ret := ""
			for k, r := range r {
				ret += fmt.Sprintf("%s - %s <br>", k, r)
			}
			returns += fmt.Sprintf("<b>%s</b> : %s <br>", k, ret)
		}

		html += fmt.Sprintf(`
<br>
<h4>%s</h4>
<p><b>Method:</b> %s</p>
<p><b>Description:</b> %s</p>
<p><b>Requires authentication:</b> %v</p>
<p><b>Requires:</b> %s</p>
<p><b>Returns:</b> %s</p> <br>
		`, k, r.Method, r.Description, r.Protected, requires, returns)
	}

	c.Response().Write([]byte(html))

	return nil

}
