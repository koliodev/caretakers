package actions

import (
	"encoding/json"

	"github.com/dgrijalva/jwt-go"
	"github.com/gobuffalo/nulls"
	"github.com/kolioDev/care/models"
)

func (as *ActionSuite) Test_Auth_Auth() {
	as.LoadFixture("login user")
	as.LoadFixture("a lot of messages")

	u := &models.User{}
	as.NoError(
		as.DB.Where("email=?", "login@user.email").First(u),
	)

	//## Всичко 6
	u.Password = "login_user_pass"

	res := as.JSON("/auth").Post(u)
	as.Equal(200, res.Code, "should return 200 but got %d -- %s", res.Code, res.Body.String())

	var resBody = &struct {
		Token     string `json:"token"`
		ExpiresAt int    `json:"expires_at"`
	}{}

	as.NoError(json.Unmarshal(res.Body.Bytes(), resBody))

	//Проверява, дали token-а e ok
	tknStr := resBody.Token
	claims := &Claims{}
	tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	as.NoError(err)
	as.True(tkn.Valid)

	//Проверява дали е пратило известие от типа "имате 420 непрочетени съобшения"
	as.NoError(as.DB.RawQuery("UPDATE messages SET receiver=?", u.ID).Exec())
	as.DBDelta(1, "notifications", func() {
		res := as.JSON("/auth").Post(u)
		as.Equal(200, res.Code, "should return 200 but got %d -- %s", res.Code, res.Body.String())
		n := models.Notification{}
		as.DB.Last(&n)
		as.Equal("Имате 5 непрочетени съобщения", n.Message)
	})

	u.Email = nulls.NewString("not_valid")
	res = as.JSON("/auth").Post(u)
	as.Equal(403, res.Code)
	u.Email = nulls.NewString("login@user.email")

	//Дали authMiddleWare-a спира request-и ако потребителя НЕ е verified
	res = as.JSON("/user?_token=" + tknStr).Get()
	as.Equal(true, u.Verified)
	as.Equal(200, res.Code, "should return 200 but got %d -- %s", res.Code, res.Body.String())
	u.Verified = false
	as.NoError(as.DB.Save(u))
	res = as.JSON("/user?_token=" + tknStr).Get()
	as.Equal(401, res.Code)

	//Дали може да се log-не, ако не е verified
	res = as.JSON("/auth").Post(u)
	as.Equal(401, res.Code, "should return 401 but got %d -- %s", res.Code, res.Body.String())

}
