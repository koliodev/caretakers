package actions

import (
	"fmt"

	"github.com/gobuffalo/nulls"
	"github.com/gofrs/uuid"
	"github.com/kolioDev/care/models"
)

func (as *ActionSuite) Test_Individuals_AddIndividual() {
	as.LoadFixture("a lot of individuals")
	var i models.Individual

	as.NoError(as.DB.Where("name=?", "fixtureИван").First(&i))
	i.ID = uuid.Nil

	token, u := loginUser(as)

	as.DBDelta(1, "individuals", func() {
		res := as.JSON(fmt.Sprintf("/individuals/add/?_token=%s", token)).Put(i)
		as.Equal(200, res.Code, fmt.Sprintf("Expecting 200 but got %d (%s)", res.Code, res.Body.String()))
		as.Contains(res.Body.String(), "\"name\":\"fixtureИван\"")
	})

	//Валидационни грешки
	i.Name = ""
	as.DBDelta(0, "individuals", func() {
		res := as.JSON(fmt.Sprintf("/individuals/add/?_token=%s", token)).Put(i)
		as.Equal(406, res.Code, fmt.Sprintf("Expecting 406 but got %d (%s)", res.Code, res.Body.String()))
		as.Contains(res.Body.String(), "Името е задължително")
	})

	//Log-натия потребител е caretaker
	i.Name = "Иван"
	as.DBDelta(0, "individuals", func() {
		as.LoadFixture("a lot of caretakers")
		c := models.Caretaker{}
		as.NoError(as.DB.First(&c))
		u.CaretakerID = nulls.NewUUID(c.ID)
		as.DB.Save(u)
		res := as.JSON(fmt.Sprintf("/individuals/add/?_token=%s", token)).Put(i)
		as.Equal(403, res.Code, fmt.Sprintf("Expecting 401 but got %d (%s)", res.Code, res.Body.String()))
	})

}

func (as *ActionSuite) Test_Individuals_UpdateIndividual() {
	as.LoadFixture("a lot of individuals")
	var i models.Individual

	as.NoError(as.DB.Where("name=?", "fixtureИван").First(&i))
	token, u := loginUser(as)

	i.UserID = u.ID
	as.NoError(as.DB.Save(&i))

	url := fmt.Sprintf("/individual/edit/%s/?_token=%s", i.ID.String(), token)
	i.Name = "Гошиту"
	as.DBDelta(0, "individuals", func() {
		res := as.JSON(url).Put(i)
		as.Equal(200, res.Code, fmt.Sprintf("Expecting 200 but got %d (%s)", res.Code, res.Body.String()))
		as.Contains(res.Body.String(), "\"name\":\"Гошиту\"")
	})

	//Валидационни грешки
	i.Name = ""
	as.DBDelta(0, "individuals", func() {
		res := as.JSON(url).Put(i)
		as.Equal(406, res.Code, fmt.Sprintf("Expecting 406 but got %d (%s)", res.Code, res.Body.String()))
		as.Contains(res.Body.String(), "Името е задължително")
	})
	i.Name = "Иван"

	//Log-натия потребител не притежава лицето
	i.UserID = uuid.FromStringOrNil("f684bef4-5b78-44e3-b54f-4c2739b60caa")
	as.NoError(as.DB.Save(&i))
	as.DBDelta(0, "individuals", func() {
		res := as.JSON(url).Put(i)
		as.Equal(403, res.Code, fmt.Sprintf("Expecting 401 but got %d (%s)", res.Code, res.Body.String()))
	})

}

func (as *ActionSuite) Test_Individuals_RemoveIndividual() {
	as.LoadFixture("a lot of individuals")
	var i models.Individual

	as.NoError(as.DB.Where("name=?", "fixtureИван").First(&i))
	token, u := loginUser(as)

	i.UserID = u.ID
	as.NoError(as.DB.Save(&i))

	url := fmt.Sprintf("/individual/remove/%s/?_token=%s", i.ID.String(), token)

	as.DBDelta(-1, "individuals", func() {
		res := as.JSON(url).Delete()
		as.Equal(200, res.Code, fmt.Sprintf("Expecting 200 but got %d (%s)", res.Code, res.Body.String()))
	})

	//Log-натия потребител не притежава лицето
	i.UserID = uuid.FromStringOrNil("f684bef4-5b78-44e3-b54f-4c2739b60caa")
	as.NoError(as.DB.Save(&i))
	as.DBDelta(0, "individuals", func() {
		res := as.JSON(url).Delete()
		as.Equal(403, res.Code, fmt.Sprintf("Expecting 401 but got %d (%s)", res.Code, res.Body.String()))
	})

}

func (as *ActionSuite) Test_Individuals_FindCaretakers() {
	token, u := loginUser(as)
	as.LoadFixture("a lot of caretakers")
	as.LoadFixture("a lot of individuals")

	//Взима individual от базата и го прави собственост на потребитея
	i := models.Individual{}
	as.NoError(as.DB.Where("city=?", "Dobrich").First(&i))
	i.UserID = u.ID
	as.NoError(as.DB.Save(&i))

	//Всичко ОК
	res := as.JSON(fmt.Sprintf("/caretaker/find?proximity=%d;individual_id=%v;_token=%s", 100, i.ID, token)).Get()
	as.Equal(200, res.Code, fmt.Sprintf("should return 200 but returns %d (%v)", res.Code, res.Body.String()))
	as.Contains(res.Body.String(), "Dobrich")
	as.Contains(res.Body.String(), "Varna")
	as.Contains(res.Body.String(), "Silistra")

	//Всичко ОК + филтър за уменията
	res = as.JSON(fmt.Sprintf("/caretaker/find?proximity=%d;individual_id=%v;skills=умение2,умение3;_token=%s", 100, i.ID, token)).Get()
	as.Equal(200, res.Code, fmt.Sprintf("should return 200 but returns %d (%v)", res.Code, res.Body.String()))
	as.Contains(res.Body.String(), "Dobrich")
	as.NotContains(res.Body.String(), "Varna")
	as.NotContains(res.Body.String(), "Silistra")

	//Невалидно individual ID
	res = as.JSON(fmt.Sprintf("/caretaker/find?proximity=%d;individual_id=%v;_token=%s", 100, "123", token)).Get()
	as.Equal(403, res.Code, fmt.Sprintf("should return 403 but returns %d (%v)", res.Code, res.Body.String()))

	//Потребителя не притежава лицето
	res = as.JSON(fmt.Sprintf("/caretaker/find?proximity=%d;individual_id=%v;_token=%s", 100, "c6072389-1ded-4c1a-a703-cb6b9a3692b6", token)).Get()
	as.Equal(403, res.Code, fmt.Sprintf("should return 403 but returns %d (%v)", res.Code, res.Body.String()))

	//Невалидно разстояние
	res = as.JSON(fmt.Sprintf("/caretaker/find?proximity=%s;individual_id=%v;_token=%s", "го6ко", i.ID, token)).Get()
	as.Equal(403, res.Code, fmt.Sprintf("should return 403 but returns %d (%v)", res.Code, res.Body.String()))

}

func (as *ActionSuite) Test_Individuals_GetIndividuals() {
	token, u := loginUser(as)
	as.LoadFixture("a lot of individuals")

	i := models.Individual{}
	as.NoError(as.DB.Where("city=?", "Donchevo").First(&i))
	i.ID = uuid.Nil

	verrs, err := u.AddIndividual(as.DB, &i)
	as.NoError(err)
	as.False(verrs.HasAny())

	res := as.JSON("/individuals?_token=" + token).Get()
	as.Equal(res.Code, 200)
	as.Contains(res.Body.String(), "Donchevo")
}

func (as *ActionSuite) Test_Individuals_GetIndividual() {
	token, u := loginUser(as)
	as.LoadFixture("a lot of individuals")

	i := models.Individual{}
	as.NoError(as.DB.Where("city=?", "Donchevo").First(&i))
	i.ID = uuid.Nil

	verrs, err := u.AddIndividual(as.DB, &i)
	as.NoError(err)
	as.False(verrs.HasAny())

	res := as.JSON(fmt.Sprintf("/individual/%s?_token=%s", i.ID, token)).Get()
	as.Equal(res.Code, 200)
	as.Contains(res.Body.String(), "Donchevo")
}
