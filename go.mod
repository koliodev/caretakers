module github.com/kolioDev/care

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gobuffalo/buffalo v0.14.11
	github.com/gobuffalo/buffalo-pop v1.23.1
	github.com/gobuffalo/envy v1.7.1
	github.com/gobuffalo/flect v0.1.6
	github.com/gobuffalo/mw-forcessl v0.0.0-20180802152810-73921ae7a130
	github.com/gobuffalo/mw-paramlogger v0.0.0-20190129202837-395da1998525
	github.com/gobuffalo/nulls v0.1.0
	github.com/gobuffalo/packr/v2 v2.7.1
	github.com/gobuffalo/pop v4.12.2+incompatible
	github.com/gobuffalo/suite v2.8.2+incompatible
	github.com/gobuffalo/uuid v2.0.5+incompatible
	github.com/gobuffalo/validate v2.0.3+incompatible
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/imdario/mergo v0.3.8
	github.com/markbates/goth v1.61.0
	github.com/markbates/grift v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/pusher/chatkit-server-go v0.0.0-20191211163135-a0642258d53e
	github.com/pusher/jwt-go v3.0.1+incompatible // indirect
	github.com/pusher/pusher-http-go v4.0.0+incompatible
	github.com/pusher/pusher-platform-go v0.0.0-20180920124841-9972b66271c7
	github.com/rs/cors v1.7.0
	github.com/satori/go.uuid v1.2.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stanislas-m/mocksmtp v1.1.0
	github.com/stretchr/testify v1.4.0
	github.com/unrolled/secure v0.0.0-20190103195806-76e6d4e9b90c
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
	golang.org/x/tools v0.0.0-20191015150414-f936694f27bf
	gopkg.in/yaml.v2 v2.2.8
)
